<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// $router->get('/test', function () {
//     return view('test');
// });
$router->post('/4B450080E7F4EAC23A8C1C7082BB1A0090A0DF4EF990C96FE36697D63E6030CE', 'ActionReceiverController@create');

$router->post('/3fkrva9coq0w5obdyg22rt9ebtja39b7jnaqvj75', 'ActionReceiverController@customerDataReceiver');

$router->post('/9545F63DB763888E8F012A9291399082/{form}', 'LeadsReceiverController@leadsReceiver');

$router->post('/417113d9a9887a84647039bc51ee420435a3bfeccd7cb78e7e9d7efbacb42dc6', 'StatusUpdaterController@statusReceiver');

$router->post('/7a84647039b/orogames/save', 'OrangeGamesController@save');
$router->post('/7a84647039b/orogames/play', 'OrangeGamesController@play');
$router->post('/7a84647039b/orogames/notify', 'OrangeGamesController@notify');


// require 'importing.php';
