<?php

/*
|--------------------------------------------------------------------------
| Importing routes Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// $router->get('/import/customers/{index}/{position}/{batch}', function ($index, $position, $batch) {
//     $directory = "/home/forge/cbox.dash.rocks/docs/";
//     // $directory = "C://work/dashrocks-form-data/docs/";
//     $files = [
//         "pushids_decrypted_1.csv",
//         "pushids_decrypted_2.csv",
//         "pushids_decrypted_3.csv",
//         "pushids_decrypted_4.csv",
//     ];

//     foreach ($files as $key => $file) {
//         if ($key < $index) {
//             continue;
//         }
//         $row = 1;
//         if (($handle = fopen($directory . $file, "r")) !== false) {
//             fseek($handle, $position);
//             while (($data = fgetcsv($handle, 1000, ",")) !== false) {
//                 $customerData = [
//                     'sso_id' => $data[0],
//                     'token'  => $data[1],
//                 ];

//                 $row++;
//                 if ($row % $batch == 0) {
//                     $position = ftell($handle);
//                     fclose($handle);
//                     return redirect("/import/customers/$key/$position/$batch");
//                 }

//                 $customer = App\Models\Customer::where('sso_id', $data[0])
//                     ->where('token', $data[1])
//                     ->first();
//                 if ($customer) {
//                     continue;
//                 }

//                 $customer = App\Models\Customer::where('sso_id', $data[0])->first();
//                 if (!$customer) {
//                     $customer = App\Models\Customer::where('token', $data[1])->first();
//                 }

//                 if ($customer) {
//                     $customer = App\Models\Customer::updateOrCreate(
//                         ['id' => $customer->id],
//                         $customerData
//                     );
//                 } else {
//                     $customer = App\Models\Customer::create($customerData);
//                 }
//             }
//             fclose($handle);
//         }
//     }
// });


// $router->get('/build/customers/{offset}/{limit}', function ($offset, $limit) {
//     $actions = App\Models\Action::offset($offset)
//         ->limit($limit)
//         ->get();

//     foreach ($actions as $action) {
//         $customerService = new App\Services\CustomerService($action);
//         $customerService->store();
//     }

//     echo "DONE $offset";
//     $offset += $limit;
//     return redirect("/build/customers/$offset/$limit");
// });

/*
|--------------------------------------------------------------------------
*/

// $router->get('/build/populate/forms/{offset}/{limit}', function ($offset, $limit) {
//     $forms = app('db')->select("SELECT GROUP_CONCAT(DISTINCT(form)) as forms, actions.phone FROM actions
//                     INNER JOIN customers ON customers.phone = actions
//                     WHERE phone IN (SELECT phone FROM customers WHERE forms IS NULL)
//                     GROUP BY actions.phone LIMIT $offset, $limit");

//     foreach ($forms as $data) {
//         App\Models\Customer::where('phone', $data->phone)
//             ->update(['forms' => $data->forms]);
//     }

//     echo "DONE $offset";
//     $offset += $limit;
//     return redirect("/build/populate/forms/$offset/$limit");
//     die('DONE');
// });

/*
|--------------------------------------------------------------------------
*/

// $router->get('/import/{offset}/{limit}', function ($offset, $limit) {
//     $file = "/home/forge/cbox.dash.rocks/docs/SSO_SOHO20200707.csv";
//     // $file = "C://tmp/SSO_SOHO20200707.csv";

//     // $a = [];
//     $csv = array_map('str_getcsv', file($file));
//     foreach ($csv as &$line) {
//         $line = array_map("utf8_encode", $line); //added
//     }

//     array_walk($csv, function (&$a) use ($csv) {
//         $a = array_combine($csv[0], $a);
//     });
//     array_shift($csv); # remove column header

//     foreach (array_slice($csv, $offset, $limit) as $key => $value) {
//         $postData = [
//             strtolower("MA_PRIMARY_MSISDN") => $value['MA_PRIMARY_MSISDN'],
//             strtolower("ACCOUNT_ID") => $value['ACCOUNT_ID'],
//             strtolower("SEGMENT_BUSINESS_CODE") => $value['SEGMENT_BUSINESS_CODE'],
//         ];

//         $action = \App\Models\Action::create([
//             'gdpr' => 0,
//             'sso_id' => $value["SSO_ID"],
//             'form' => 'soho-import',
//             'page' => "import-csv",
//             'medium' => 'import',
//             'agent' => 'import',
//             'phone' => formatPhone($value["MSISDN"]),
//             'post_data' => json_encode($postData),
//             'created_at' => DateTime::createFromFormat('d-M-y', strtolower($value["MA_DATE_CREATED"]))->format('Y-m-d'). ' 00:00:00',
//         ]);

//         $customerService = new App\Services\CustomerService($action);
//         $customerService->store();
//     }

//     $offset += $limit;
//     return redirect("/import/$offset/$limit");
// });

/*
|--------------------------------------------------------------------------
*/

// $router->get('/import/{offset}/{limit}', function ($offset, $limit) {
//     $file = "/home/forge/cbox.dash.rocks//docs/baza-cbox1806.csv";
//     // $file = "C://temp/Baza CBox 18.06.csv";

//     // $a = [];
//     $csv = array_map('str_getcsv', file($file));
//     foreach ($csv as &$line) {
//         $line = array_map("utf8_encode", $line); //added
//     }

//     array_walk($csv, function (&$a) use ($csv) {
//         $a = array_combine($csv[0], $a);
//     });
//     array_shift($csv); # remove column header

//     foreach (array_slice($csv, $offset, $limit) as $key => $value) {
//         $postData = [
//             "nps" => $value['nps'],
//             "call-back" => $value['call-back'],
//             "location" => str_replace(['?'], ['s'], $value['Location']),
//         ];
//         $action = \App\Models\Action::create([
//             'gdpr' => $value['call-back'] == "da" ? 1 : 0,
//             'form' => 'call-back-import',
//             'page' => "import-csv",
//             'medium' => $value['System'],
//             'agent' => 'import',
//             'ip' => $value['IP'],
//             'phone' => "0" . $value["Phone-number"],
//             'email' => strpos($value["email"], '@') ? $value["email"] : '',
//             'post_data' => json_encode($postData),
//             'created_at' => date("Y-m-d H:i:s", strtotime($value["Date"].":00"))
//         ]);

//         $customerService = new App\Services\CustomerService($action);
//         $customerService->store();
//     }

//     $offset += $limit;
//     return redirect("/import/$offset/$limit");
// });

/*
|--------------------------------------------------------------------------
*/
// $router->get('/importBigQuery', function () {
//     $file = "c:/tmp/importShops-20200527-215150.csv";
//     $csv = [];
//     $row = 1;
//     if (($handle = fopen($file, "r")) !== false) {
//         while (($data = fgetcsv($handle, 1000, ",")) !== false) {
//             $csv[] = $data;
//         }
//         fclose($handle);
//     }
//     foreach ($csv as $key => &$line) {
//         // $line = array_map("utf8_encode", $line); //added
//         if (count($line) != 7) {
//             unset($csv[$key]);
//             print_r($line);
//         }
//     }
//     array_walk($csv, function (&$a) use ($csv) {
//         $a = array_combine($csv[0], $a);
//     });
//     array_shift($csv); # remove column header

//     foreach ($csv as $key => $value) {
//         App\Action::create([
//             'logged_in' => 0,
//             'form' => 'shops',
//             'page' => "import-csv",
//             'medium' => 'import',
//             'agent' => 'import',
//             'name' => $value["name"],
//             'phone' => $value["phone"],
//             'gdpr' => $value["gdpr"],
//             'message' => $value["message"],
//             'subject' => $value["subject"],
//             'post_data' => json_encode(['location' => $value["location"]]),
//             'created_at' => date("Y-m-d H:i:s", strtotime($value["created_at"]))
//         ]);
//     }
//     die('DONE');
// });


$router->get('/import/leads/{offset}/{limit}/{filename}', function ($offset, $limit, $filename) {
    $delimiter = ",";
    $file = "/home/forge/cbox.dash.rocks/docs/" . $filename;
    // $file = "C:/work/dashrocks-form-data/docs/" . $filename;

    foreach (file($file) as $key => $line) {
        $csv[] = str_getcsv($line, $delimiter);
        // $csv[] = array_map("utf8_encode", $columns);
    }

    array_walk($csv, function (&$a) use ($csv) {
        $a = array_combine($csv[0], $a);
    });
    array_shift($csv); # remove column header
    foreach (array_slice($csv, $offset, $limit) as $key => $value) {
        $action = \App\Models\Action::create([
            'form' => 'leads-facebook-love',
            'page' => "lead - id: not-set",
            'medium' => 'web',
            'agent' => 'Zapier',
            'name' => $value['Name'],
            'email' => strpos($value["Email"], '@') ? $value["Email"] : '',
            'phone' => formatPhone($value["Phone"]),
            // 'created_at' => date("Y-m-d H:i:s", strtotime($value["Created"]))
            'created_at' => date("Y-m-d H:i:s")
        ]);
        $customerService = new App\Services\CustomerService($action);
        $customerService->store();
        // save to CRM
        $crm = new \App\Services\OrangeCrm($action);
        $crm->upload();
    }

    $offset += $limit;
    return redirect("/import/leads/$offset/$limit/$filename");
});

$router->get('/leads/resend/{from}/{to}/{lead}', function ($from, $to, $lead) {
    $actions = \App\Models\Action::where('id', '>=', $from)
        ->where('id', '<=', $to)
        ->where('form', $lead)
        ->get();

    foreach ($actions as $action) {
        // save to CRM
        $crm = new \App\Services\OrangeCrm($action);
        $crm->upload();
        echo ". ";
    }

    echo "Done";
    return;
});
/*
|--------------------------------------------------------------------------
|--------------------------------------------------------------------------
 */
function formatPhone($phone = null)
{
    if (is_null($phone) || strlen($phone) == 0) {
        return null;
    }
    $phone = str_replace(
        ["+40", " ", "(", ")", "-", "+"],
        ["0", "", "", "", "", ""],
        $phone
    );

    if (strpos($phone, "0040") === 0) {
        $phone = substr_replace($phone, "0", 0, 4);
    }

    if (strpos($phone, "4007") === 0) {
        $phone = substr_replace($phone, "07", 0, 4);
    }

    if (strpos($phone, "407") === 0) {
        $phone = substr_replace($phone, "07", 0, 3);
    }

    if (strpos($phone, "007") === 0) {
        $phone = substr_replace($phone, "07", 0, 3);
    }

    if (strpos($phone, "00") === 0) {
        $phone = substr_replace($phone, "00", 0, 2);
    }

    if ($phone[0] != "0") {
        $phone = "0" . $phone;
    }

    return $phone;
}
