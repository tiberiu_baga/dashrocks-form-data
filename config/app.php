<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
     */

    'name' => env('APP_NAME', 'Lumen'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
     */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
     */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
     */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
     */

    'timezone' => 'Europe/Bucharest',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
     */

    'locale' => env('APP_LOCALE', 'en'),

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
     */

    'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
     */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    "crm_user" => env('CRM_USER'),
    "crm_password" => env('CRM_PASSWORD'),
    "crm_url" => env('CRM_URL'),

    "cbox_key" => env('CBOX_UPDATER_KEY'),

    "tkr" => [
        'url' => env('CRM_TKR_URL'),
        'key' => env('CRM_TKR_KEY'),
    ],

    "datoCms" => [
        'url' => env('DATOCMS_URL'),
        'key' => env('DATOCMS_KEY'),
        'hipto_key' => env('DATOCMS_HIPTO_KEY'),
    ],
    "datoCmsStatus" => [
        'url' => env('DATOCMS_STATUS_URL'),
        'key' => env('DATOCMS_STATUS_KEY'),
    ],

    "usabilla" => [
        'key' => env('USABILLA_KEY'),
        'secret' => env('USABILLA_SECRET'),
        'forms' => [
            '618ba388b36bdf554c4c5b12',
            '6169877c8fe6223c1048c610'
        ],
    ],

    "oro_games_key" => env('CBOX_OROGAMES_KEY'),

    "telesales" => [
        'url' => env('CRM_TEL_URL'),
        'user' => env('CRM_TEL_USER'),
        'pwd' => env('CRM_TEL_PWD'),
    ],

    "repo" => [
        'url' => env('CRM_REPO_URL')
    ],
];
