<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'actito' => [
        'key' => env('ACTITO_KEY'),
        'entity' => env('ACTITO_ENTITY'),
        'abandonScenarioId' => env('ACTITO_BASKET_ABANDONER_PROFILE_ID'),
        'summerLotteryCodeCampaignId' => env('ACTITO_SUMMERLOTTERY_CODE_CAMPAIGN_ID'),
        'summerLotteryPrizeCampaignCarrefourId' => env('ACTITO_SUMMERLOTTERY_PRIZE_CAMPAIGN_CARREFOUR_ID'),
        'summerLotteryPrizeCampaignDecathlonId' => env('ACTITO_SUMMERLOTTERY_PRIZE_CAMPAIGN_DECATHLON_ID'),
        'summerLotteryPrizeCampaignShopId' => env('ACTITO_SUMMERLOTTERY_PRIZE_CAMPAIGN_SHOP_ID'),
        'xmasLotteryCodeCampaignId' => env('ACTITO_XMASLOTTERY_CODE_CAMPAIGN_ID'),
        'xmasLotteryCodeReminderCampaignId' => env('ACTITO_XMASLOTTERY_CODE_REMINDER_CAMPAIGN_ID'),
        'xmasLotteryPrizeCampaignNorielId' => env('ACTITO_XMASLOTTERY_PRIZE_CAMPAIGN_NORIEL_ID'),
        'xmasLotteryPrizeCampaignCarrefourId' => env('ACTITO_XMASLOTTERY_PRIZE_CAMPAIGN_CARREFOUR_ID'),
        'xmasLotteryPrizeCampaignDecathlonId' => env('ACTITO_XMASLOTTERY_PRIZE_CAMPAIGN_DECATHLON_ID'),
        'xmasLotteryPrizeCampaignIKEAId' => env('ACTITO_XMASLOTTERY_PRIZE_CAMPAIGN_IKEA_ID'),
        'xmasLotteryPrizeCampaignShopId' => env('ACTITO_XMASLOTTERY_PRIZE_CAMPAIGN_SHOP_ID'),
    ],

    'basketRecovery' => [
        'source' => env('ABANDON_COS_URL'),
        'isTesting' => env('ABANDON_COS_TESTING'),
        'testEmails' => env('ABANDON_COS_TEST_EMAILS'),
    ],

];
