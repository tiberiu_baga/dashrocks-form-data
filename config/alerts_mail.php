<?php

/*
|--------------------------------------------------------------------------
| Alerts mail config
|--------------------------------------------------------------------------

|
 */
return [
    'to_mail' => env('MAIL_TO_ADDRESS_ALERTS'),
    'subject' => 'Inactive form alert for ',

    'yoxo_to' => 'yoxoalerts@dashrocks.com',
    'yoxo_subject' => 'Yoxo Alerts for day: ',
];
