<?php

use Illuminate\Database\Query\Builder;

$baseSyncConfigs = [
    '01_cbox_actions' => [
        'from_table' => 'actions',
        'incremental_sync' => true,
        'exclude_columns' => ['ip']
    ],
    '01_cbox_customers' => [
        'from_table' => 'customers',
        'incremental_sync' => false,
        'exclude_columns' => ['ip', 'ip_location_id']
    ],
    '01_cbox_leads' => [
        'from_table' => 'actions',
        'incremental_sync' => false,
        'from_select_closure' => function (Builder $query) {
            return $query->whereNotNull('crm_type');
        },
        'exclude_columns' => ['ip']
    ],
    '01_cbox_forms' => [
        'from_table' => 'actions',
        'incremental_sync' => true,
        'from_select_closure' => function (Builder $query) {
            return $query->whereNotIn('form', [
                'disponibilitate',
                'contact',
                'newsletter-page',
                'newsletter_modal',
                'newsletter',
                'usabilla_feedback',
                'shops',
                'antreprenori',
                'homepage-abonament-nou',
                'abonamente-oferta-portare-client-nou',
                'client-nou'
            ]);
        },
        'exclude_columns' => ['ip']
    ],
    '01_cbox_orange_website_products' => [
        'from_table' => 'orange_website_products',
        'incremental_sync' => true
    ],
    // '01_innertrends_yoxo_goals' => [
    //     'from_table' => 'goals',
    //     'incremental_sync' => false,
    //     'from_connection' => 'yoxo_channel'
    // ],
    // '01_innertrends_yoxo_onboarded' => [
    //     'from_table' => 'onboarded',
    //     'incremental_sync' => false,
    //     'from_connection' => 'yoxo_channel'
    // ],
    // '01_innertrends_yoxo_signupintent' => [
    //     'from_table' => 'signupintent',
    //     'incremental_sync' => false,
    //     'from_connection' => 'yoxo_channel'
    // ],
    // '01_innertrends_yoxo_visitors' => [
    //     'from_table' => 'visitors',
    //     'incremental_sync' => false,
    //     'from_connection' => 'yoxo_channel'
    // ],
];

$syncConfigs = [];
foreach ($baseSyncConfigs as $syncKey => $syncConfig) {
    $syncConfigs[$syncKey] = $syncConfig;
    $syncConfigs[$syncKey . '_oro_digital'] = array_merge($syncConfig, [
        'to_table' => $syncKey,
        'project_id' => 'oro-digital-analytics-001-dev',
        'dataset_id' => 'orange_raw',
        'big_query_key_file_path' => base_path('oro-digital-analytics-001-dev-2a9d15afc163.json'),
        'dataset_location' => 'europe-west3',
        'bucket_name' => 'oro-digital-imports'
    ]);
}

return $syncConfigs;
