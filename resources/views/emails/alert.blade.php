<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>
<body style="color: #000000; font-family: 'Open Sans', sans-serif;">

<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
                <tr>
                    <td align="center" valign="top">
                        <h3 style="height: 40px; line-height: 40px; background-color: #f56857; color: #ffffff;">There are inactive forms for yesterday</h3>
                        <table class="emailExceptionTable" style="text-align: left;" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td><strong>The following forms are inactive:</strong></td>
                            </tr>
                            @foreach($content as $key => $value)
                                <tr>
                                    <td></td>
                                    <td>{{ $value['name'] }} - {{ $value['page'] }}</td>
                                </tr>
                            @endforeach
                        </table>
                        <hr style="color: #f6f6f6;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
