<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>
<body style="color: #000000; font-family: 'Open Sans', sans-serif;">

<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
        <td valign="top">
            <table border="0" cellpadding="20" cellspacing="0" id="emailContainer">
                <tr>
                    <td align="center" valign="top">
                        <h3 style="height: 40px; line-height: 40px; background-color: #f5b757; color: #ffffff;">New leads for date: {{ date('Y-m-d H:i') }}</h3>
                        <table class="emailExceptionTable" style="text-align: left;" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td><strong>New leads from:  {{ $page }}</strong></td>
                            </tr>
                            @foreach($content as $value)
                                @if($value->form == 'wholesale-page')
                                    @php
                                    $json = json_decode($value->post_data);
                                    if($json && isset($json->company_name)) {
                                        $company = $json->company_name;
                                    } else {
                                        $company = '';
                                    }
                                    @endphp
                                    <tr>
                                        <th>Name:</th> <td>{{ $value->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone:</th> <td>{{ $value->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email:</th> <td>{{ $value->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Company:</th> <td>{{ $company }}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <th>Phone:</th> <td>{{ $value->phone }}</td>
                                    </tr>
                                @endif
                                <hr style="color: #f6f6f6;">
                            @endforeach
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
