<!DOCTYPE html>
<html>
<head>
    <title>Hello this is an example</title>
</head>
<body>
    Data form saver example
</body>
</html>

<script type="text/javascript" src="/form_data_saver.js"></script>
<script type="text/javascript">
    var fcmUserToken = '67tg28o7gr9fhf[0890wehq276fnp9g7q2hvqpyugusdejofiuiwdfhjeuifsstesttibi';
    var mySSOid = { loggedIn: true, ssoId: 123456 };
    var formData = {
      'first_name' : 'Tibi',
      'last_name' : 'Jones',
      'name' : 'Jones one',
      'email' : 'Jones@asdasdasd.com',
      'phone' : '0040 (741) 122-909',
      'address' : 'Blaa street 43C Brasov Romania',
      'state' : 'Covasna1',
      'city' : 'Brasov',
      'subject' : 'help',
      'message' : 'Hi man I need some help with that stuff',
      'gdpr' : 'Nu',
      'company' : 'Google',
      'type' : 'Business',
      'loyalty_eur' : '50.50',
      'offer_eligibility' : 'offer eligibility',
      'last_loyalty_date' : '2021-12-31',
      'contract_expiration_date' : '2021-12-11',
      'package_code' : 'Orange1',
    };
    var formName = "contactASD";

    // How to call the data saver
    __drFormSaver(formData, formName);
</script>
