console.log("cbox script loaded v4.3"), function () {

    function e(e) {
        for (var t = e + "=", d = document.cookie.split(";"), i = 0; i < d.length; i++) {
            for (var n = d[i]; " " == n.charAt(0);) n = n.substring(1, n.length);
            if (0 == n.indexOf(t)) return n.substring(t.length, n.length)
        }
        return null
    }

    window.location.href.indexOf("orange.ro") > -1 && document.querySelectorAll('[id^="campaignCodeLeads"], [id^="sourceLeads"], [id^="mediumLeads"]').forEach((function (t, d, i) {
        var n = function () {
            var t = {utm_campaign: "", utm_source: "", utm_medium: ""}, d = e("utmz");
            if (d) {
                var i = decodeURI(d).split("|");
                void 0 !== i[0] && (t.utm_campaign = i[0]), void 0 !== i[1] && (t.utm_source = i[1]), void 0 !== i[2] && (t.utm_medium = i[2])
            }
            return t
        }();
        document.getElementById(t.id) && (-1 !== t.id.toString().indexOf("campaignCodeLeads") && (document.getElementById(t.id).value = n.utm_campaign), -1 !== t.id.toString().indexOf("sourceLeads") && (document.getElementById(t.id).value = n.utm_source), -1 !== t.id.toString().indexOf("mediumLeads") && (document.getElementById(t.id).value = n.utm_medium))
    }));

    function showXmasLottery() {
        var $ = window.jQuery;

        if (window.isXmasLotteryIncluded) return;

        window.isXmasLotteryIncluded = true;

        var $container;

        var retryTimes = 20;

        insertSection();
        handleCopyCodeToClipboard();
        handleShowMoreBtnClick();
        handleSaveCode();
        handleSubmit();
        removeErrorOnFocus();
        removeErrorOnChange();

        function insertSection() {
            var $target = $('#cartcont > .container > .row').first();
            $container = getContainerMarkup();

            var $style = getStyle();
            $style.insertAfter($target);
            $container.insertAfter($target);
        }

        function checkForMarkup() {
            var hasXmasLottery = $('#cartcont .omni-row').length > 0;
            var hasCartContainer = $('#cartcont').length > 0;
            if (!hasXmasLottery && hasCartContainer && isXmasLotteryIncluded) {
                insertSection();
                handleCopyCodeToClipboard();
                handleShowMoreBtnClick();
                handleSubmit();
                removeErrorOnFocus();
                removeErrorOnChange();

                $container.find('.omni-row__form-group--coupon input').val(window.xmasLotteryCode);
                $container.find('#omni-email').val(window.xmasEmail);
                $container.show();
                scrollToMarkup();
            }
        }

        function handleCopyCodeToClipboard() {
            $container.find('.omni-box__copy').on('click', function () {
                dataLayer.push({
                    "event": "oro.action_click",
                    "event_category": "Eshop XMas Lottery 22",
                    "event_action": "Thank You Page",
                    "event_label": "Copy Code",
                    "event_value": "0"
                });
                var $self = $(this);
                var $message = $self.closest('.omni-row').find('.omni-row__message--coupon');
                var $input = $self.siblings('input');
                var inputValue = $input.val();

                navigator.clipboard.writeText(inputValue).then(function () {
                    $message.text('Cod copiat cu succes!');
                    $message.removeClass('omni-hidden');
                    setTimeout(function () {
                        $message.text('');
                        $message.addClass('omni-hidden');
                    }, 3000);
                });
            });
        }

        function handleShowMoreBtnClick() {
            $container.find('.omni-row__showMoreBtn').on('click', function () {
                var $self = $(this);
                var $showMore = $self.closest('.omni-row').find('.omni-form-group__terms');

                $self.addClass('d-none');
                $showMore.removeClass('d-none');
            });
        }

        function handleSaveCode() {
            var orderId = $('input[name="orderId"]').eq(0).val();
            var payload = {
                id: orderId
            };

            try {
                var email = $('#_invoice_proform_WAR_webshopportlets_email').val();
                $container.find('#omni-email').val(email);
                window.xmasEmail = email;

                payload['email'] = email;
            } catch (ex) {

            }

            sendRequest('/save', payload);
        }

        function scrollToMarkup() {
            var offsetTop = $container[0].getBoundingClientRect().top;
            var yOffset = -150;
            var y = offsetTop + window.pageYOffset + yOffset;
            window.scrollTo({top: y, behavior: 'smooth'});
        }

        function handleSubmit() {
            var $codeInput = $container.find('.omni-row__form-group--coupon input');
            var $emailInput = $container.find('.omni-row__form-group--email input');

            $container.find('.omni-row__submitBtn').on('click', function () {
                var isValidForm = validateForm();

                if (!isValidForm) {
                    return;
                }

                dataLayer.push({
                    "event": "oro.action_click",
                    "event_category": "Eshop XMas Lottery 22",
                    "event_action": "Thank You Page",
                    "event_label": "Send Email",
                    "event_value": "0"
                });

                var payload = {
                    code: $codeInput.val(),
                    email: $emailInput.val(),
                };

                sendRequest('/notify', payload);
            });

            function validateForm() {
                validateEmail();
                validateCheckboxes();

                var isValid = $container.find('.omni-row__form-group--error').length === 0;

                return isValid;

                function validateEmail() {
                    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    var $emailInput = $container.find('.omni-row__form-group--email input');
                    var email = $emailInput.val();
                    var $formGroup = $emailInput.closest('.omni-row__form-group');

                    if (!emailRegex.test(email)) {
                        $formGroup.addClass('omni-row__form-group--error');
                        showErrorMessage($emailInput, 'Adresa de e-mail este incorectă.');
                    } else {
                        var $error = $formGroup.find('.omni-form-group__error');

                        $formGroup.removeClass('omni-row__form-group--error');
                        $error.text('');
                    }
                }

                function validateCheckboxes() {
                    var $checkboxes = $container.find('.omni-row__form-group--checkbox input');
                    var hasCheckedTerms = true;

                    $checkboxes.each(function () {
                        var $self = $(this);

                        if (!$self.is(':checked')) {
                            $self.closest('.omni-row__form-group').addClass('omni-row__form-group--error');
                            hasCheckedTerms = false;
                        }

                        if (hasCheckedTerms) {
                            $container.find('.omni-row__main-error').addClass('d-none');
                        } else {
                            showMainError('Pentru a putea participa, trebuie să fii de acord cu modul de utilizare a datelor personale și cu termenii și condițiile campaniei.');
                        }
                    });
                }
            }
        }

        function sendRequest(endpoint, payload) {
            var apiBaseUrl = 'https://cbox.mydigitalorange.ro/7a84647039b/orogames';
            var apiKey = ']x<YkW2Eqxj6LkimasW^8xKFR<pK-W';
            payload.key = apiKey;

            $.ajax({
                type: 'POST',
                url: apiBaseUrl + endpoint,
                data: JSON.stringify(payload),
                contentType: 'application/json',
                success: function (data) {
                    if (endpoint === '/save') {

                        if (data.error) {
                            $container.hide();
                            return;
                        }

                        $container.show();
                        scrollToMarkup();

                        window.xmasLotteryInterval = setInterval(checkForMarkup, 200);

                        dataLayer.push({
                            "event": "oro.action_click",
                            "event_category": "Eshop XMas Lottery 22",
                            "event_action": "Thank You Page",
                            "event_label": "Eligible Order",
                            "event_value": 123,
                            "event_noninteraction": true
                        });

                        window.xmasLotteryCode = data.code;

                        $container.find('.omni-row__form-group--coupon input').val(data.code);
                        document.cookie = "oro_xmas_lottery=" + data.code + "; expires=Mon, 1 May 2023 12:00:00 UTC; path=/; SameSite=Lax;"
                        return;
                    }

                    if (endpoint === '/notify') {
                        if (data.error) {
                            showMainError('Din păcate, datele introduse nu sunt corecte. Te rugăm să le verifici din nou.');
                            return;
                        }

                        if (data.success) {
                            showMainSuccess('Voucherul tău a fost trimis cu succes pe adresa ta de email.');
                        }
                    }
                },
                error: function () {
                    if (endpoint === '/save') {
                        if (retryTimes > 0) {
                            retryTimes--;
                            setTimeout(function () {
                                sendRequest(endpoint, payload);
                            }, 200);
                        }

                        return;
                    }

                    if (endpoint === '/notify') {
                        showMainError('A avut loc o eroare. Te rugăm sa încerci din nou');
                    }
                },
            });
        }

        function showErrorMessage($input, message) {
            var $formGroup = $input.closest('.omni-row__form-group');
            var $error = $formGroup.find('.omni-form-group__error');

            $formGroup.addClass('omni-row__form-group--error');
            $error.text(message);
        }

        function showMainError(message) {
            var $mainError = $container.find('.omni-row__main-error');

            $mainError.removeClass('d-none');
            $mainError.text(message);
        }

        function showMainSuccess(message) {
            var $mainSuccess = $container.find('.omni-row__main-success');
            var $mainError = $container.find('.omni-row__main-error');

            $mainError.addClass('d-none');
            $mainSuccess.removeClass('d-none');
            $mainSuccess.text(message);
            $container.find('.omni-row__submitBtn').prop('disabled', true);
        }

        function removeErrorOnFocus() {
            var $inputs = $container.find('input[type="text"]');

            $inputs.on('focus', function () {
                var $formGroup = $(this).closest('.omni-row__form-group');
                var $error = $formGroup.find('.omni-form-group__error');

                $formGroup.removeClass('omni-row__form-group--error');
                $error.text('');
            });
        }

        function removeErrorOnChange() {
            var $checkboxes = $container.find('input[type="checkbox"]');

            $checkboxes.on('change', function () {
                var $formGroup = $(this).closest('.omni-row__form-group');

                $formGroup.removeClass('omni-row__form-group--error');
            });
        }

        function getContainerMarkup() {
            return $('' +
                '<div class="omni-ty-surprise" style="display:none;">' +
                '   <div className="container">' +
                '		<div class="row omni-row">' +
                '			<div class="col-xs-12">' +
                '				<div class="row btmspace">' +
                '					<div class="col-xs-12">' +
                '						<div class="box-hmax">' +
                '							<div class="row btm0px">' +
                '								<div class="col-xs-12 col-md-offset-1 col-md-10">' +
                '									<div class="omni-cols">' +
                '										<div class="omni-col">' +
                '											<div class="omni-row__title text-orange color-orange font-weight-bold">Ce zici de o surpriză?</div>' +
                '											<div class="omni-row__paragraph font-weight-bold">Mulțumim pentru comandă!<br>Iată codul tău unic</div>' +
                '											<div class="omni-row__form-group omni-row__form-group--input omni-row__form-group--coupon">' +
                '												<label for="omni-coupon" class="omni-form-group__label color-orange font-weight-bold  text-orange">Cod unic</label>' +
                '												<div class="omni-form-group__box">' +
                '													<input type="text" id="omni-coupon" class="font-weight-bold" readonly value="XXXXXX">' +
                '													<img src="https://www.orange.ro/docs/campanii/xmas-lottery-2022/imagini/xmas-lottery-copy-icon.png" class="omni-box__copy" alt="Copy Icon">' +
                '												</div>' +
                '											</div>' +
                '											<div class="omni-row__message omni-row__message--coupon font-weight-bold omni-hidden"></div>' +
                '											<div class="omni-row__paragraph font-weight-bold">Folosește codul primit pentru a câștiga pe loc unul dintre voucherele Decathlon, Carrefour, Noriel, IKEA sau Orange.</div>' +
                '											<div class="omni-row__paragraph font-weight-bold">Salvează codul tău unic, fă o captură de ecran cu el sau trimite-l pe adresa ta de e-mail.</div>' +
                '											<div class="omni-row__subtitle color-orange font-weight-bold">Trimite codul tău unic pe e-mail</div>' +
                '											<div class="omni-row__form-group omni-row__form-group--input omni-row__form-group--email">' +
                '												<div class="omni-form-group__error text-danger"></div>' +
                '												<label for="omni-email" class="omni-form-group__label color-orange font-weight-bold  text-orange">E-mail</label>' +
                '												<div class="omni-form-group__box">' +
                '													<input type="text" id="omni-email" class="font-weight-bold">' +
                '												</div>' +
                '											</div>' +
                '											<div class="omni-row__form-group omni-row__form-group--checkbox">' +
                '												<input type="checkbox" class="omni-form-group__checkbox d-none" id="termsConsent">' +
                '												<label for="termsConsent">Sunt de acord cu Termenii și Condițiile campaniei <a href="https://www.orange.ro/docs/campanii/xmas-lottery-2022/221122_Regulament-Xmas-Lottery-Nov-dec-22.pdf" target="_blank" class="omni-row__link">„Iarna se-mpart surprizele online”</a></label>' +
                '											</div>' +
                '											<div class="omni-row__form-group omni-row__form-group--checkbox">' +
                '												<input type="checkbox" class="omni-form-group__checkbox d-none" id="ageConsent">' +
                '												<label for="ageConsent">Declar că am peste 16 ani și sunt de acord ca' +
                '												<span class="omni-form-group__terms d-none">Orange Romania să mă contacteze și să utilizeze datele mele personale, inclusiv zona în care utilizez serviciile Orange, pentru a-mi oferi cele mai bune oferte și servicii personalizate în funcție de nevoile mele, din portofoliul Orange Romania și Orange Romania Communications.</span>' +
                '												</label>' +
                '											</div>' +
                '											<div class="omni-row__showMoreBtn color-orange  text-orange font-weight-bold">Citește mai mult</div>' +
                '											<button class="omni-row__submitBtn btn btn-important">Trimite</button>' +
                '											<div class="omni-row__main-error font-weight-bold d-none"></div>' +
                '											<div class="omni-row__main-success font-weight-bold d-none"></div>' +
                '										</div>' +
                '										<div class="omni-col">' +
                '											<img src="https://www.orange.ro/docs/campanii/xmas-lottery-2022/imagini/xmas-lottery-elf.png" class="omni-col__image" alt="Elf">' +
                '										</div>' +
                '									</div>' +
                '								</div>' +
                '							</div>' +
                '						</div>' +
                '					</div>' +
                '				</div>' +
                '			</div>' +
                '		</div>' +
                '	</div>' +
                '</div>'
            )
        }

        function getStyle() {
            return $("<style>" + ".omni-ty-surprise {\n" +
                "\tmargin-left: 0px;\n" +
                "\tpadding: 60px 0;\n" +
                "\tbackground: #000;\n" +
                "}\n" +
                "\n" +
                ".omni-ty-surprise .col-xs-12 {\n" +
                "\twidth: 100%;\n" +
                "}\n" +
                "\n" +
                ".omni-ty-surprise .row:not(.omni-row) {\n" +
                // "\tmargin: 0;\n" +
                "}\n" +
                "\n" +
                ".omni-ty-surprise .color-orange {\n" +
                "\tcolor: #ff7900;\n" +
                "}\n" +
                "\n" +
                ".omni-row {\n" +
                "\tbackground: #000;\n" +
                "\tpadding: 10px 0;\n" +
                "\tmargin-left: 0px;\n" +
                "\tmargin-right: 0px;\n" +
                "}\n" +
                "\n" +
                ".omni-row * {\n" +
                "\tfont-family: 'helveticaneue', 'Arial', sans-serif;\n" +
                "}\n" +
                "\n" +
                "/* Start Columns */\n" +
                ".omni-row .omni-cols {\n" +
                "\tdisplay: flex;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-cols .omni-col:first-child {\n" +
                "\tmax-width: 449px;\n" +
                "\twidth: 100%;\n" +
                "\tmargin-right: 40px;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-cols .omni-col:last-child {\n" +
                "\tdisplay: flex;\n" +
                "\talign-items: flex-start;\n" +
                "\tpadding: 40px 70px 0;\n" +
                "}\n" +
                "\n" +
                "/* End Columns */\n" +
                "\n" +
                "/* Start Text Elements */\n" +
                ".omni-row .omni-row__title {\n" +
                "\tfont-size: 28px;\n" +
                "\tline-height: 1;\n" +
                "\tletter-spacing: -1.04px;\n" +
                "\tmargin-bottom: 24px;\n" +
                "\twidth: fit-content;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__paragraph {\n" +
                "\tcolor: #fff;\n" +
                "\tmargin-bottom: 16px;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__link {\n" +
                "\tcolor: #fff;\n" +
                "\tborder-bottom: 1px solid #757575;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__subtitle {\n" +
                "\tfont-size: 24px;\n" +
                "\tline-height: 1;\n" +
                "\tletter-spacing: -0.88px;\n" +
                "\tmargin-top: 40px;\n" +
                "\tmargin-bottom: 24px;\n" +
                "}\n" +
                "\n" +
                "/* End Text Elements */\n" +
                "\n" +
                "/* Start Form Group */\n" +
                ".omni-row .omni-row__form-group {\n" +
                "\tposition: relative;\n" +
                "\tdisplay: flex;\n" +
                "\talign-items: center;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--email {\n" +
                "\tmargin-bottom: 24px;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-form-group__label {\n" +
                "\tcursor: pointer;\n" +
                "\tmargin: 0 16px 0 0;\n" +
                "    white-space: nowrap;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--checkbox {\n" +
                "\tmargin-bottom: 8px;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-form-group__box {\n" +
                "\tposition: relative;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--email.omni-row__form-group--error {\n" +
                "\tpadding-top: 24px;\n" +
                "}\n" +
                "\n" +
                "/* End Form Group */\n" +
                "\n" +
                "/* Start Input */\n" +
                ".omni-row .omni-row__form-group input {\n" +
                "\tappearance: none;\n" +
                "\tborder: 2px solid #fff;\n" +
                "\toutline: 0;\n" +
                "\tmargin: 0;\n" +
                "\tpadding: 4px 8px;\n" +
                "\twidth: 100%;\n" +
                "\tbackground: #000;\n" +
                "\tcolor: #fff;\n" +
                "\ttransition: 0.15s border-color ease-in-out;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--input .omni-form-group__box {\n" +
                "\tmax-width: 140px;\n" +
                "\twidth: 100%;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--input.omni-row__form-group--email .omni-form-group__box {\n" +
                "\tmax-width: 200px;\n" +
                "}\n" +
                "\n" +
                ".omni-row input:focus,\n" +
                ".omni-row .omni-row__form-group--coupon input {\n" +
                "\tborder: 2px solid #ff7900;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--input.omni-row__form-group--error input {\n" +
                "\tborder: 2px solid #cd3c14;\n" +
                "}\n" +
                "\n" +
                "/* End Input */\n" +
                "\n" +
                "/* Start Label */\n" +
                ".omni-row .omni-row__form-group--checkbox label {\n" +
                "\tcolor: #fff;\n" +
                "\tmargin: 0;\n" +
                "\t-webkit-touch-callout: none;\n" +
                "\tpadding-left: 24px;\n" +
                "\tposition: relative;\n" +
                "\t-webkit-user-select: none;\n" +
                "\t-khtml-user-select: none;\n" +
                "\t-moz-user-select: none;\n" +
                "\t-ms-user-select: none;\n" +
                "\tuser-select: none;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--checkbox label::before,\n" +
                ".omni-row .omni-row__form-group--checkbox label::after {\n" +
                "\tcontent: '';\n" +
                "\tposition: absolute;\n" +
                "\ttop: 2px;\n" +
                "\tleft: 0;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--checkbox label::before {\n" +
                "\tborder: 2px solid #fff;\n" +
                "\theight: 16px;\n" +
                "\twidth: 16px;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--checkbox.omni-row__form-group--error label::before {\n" +
                "\tborder: 2px solid #cd3c14;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--checkbox input+label::after {\n" +
                "\tbackground: 50% / cover url('https://www.orange.ro/docs/campanii/xmas-lottery-2022/imagini/xmas-lottery-check-icon.png') no-repeat;\n" +
                "\tdisplay: none;\n" +
                "\theight: 16px;\n" +
                "\twidth: 16px;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__form-group--checkbox input:checked+label::after {\n" +
                "\tdisplay: block;\n" +
                "}\n" +
                "\n" +
                "/* End Label */\n" +
                "\n" +
                "/* Start Error */\n" +
                ".omni-row .omni-row__form-group .omni-form-group__error {\n" +
                "\tdisplay: block;\n" +
                "\tfont-size: 12px;\n" +
                "\tposition: absolute;\n" +
                "\ttop: 0;\n" +
                "}\n" +
                "\n" +
                "/* End Error */\n" +
                "\n" +
                "/* Start Main Error */\n" +
                ".omni-row .omni-row__main-error {\n" +
                "\tcolor: #fff;\n" +
                "\tbackground-color: #cd3c14;\n" +
                "\tpadding: 4px 8px;\n" +
                "\tmargin-top: 24px;\n" +
                "}\n" +
                "\n" +
                "/* End Main Error */\n" +
                "\n" +
                "/* Start Main Success */\n" +
                ".omni-row .omni-row__main-success {\n" +
                "\tcolor: #000;\n" +
                "\tbackground-color: #fff;\n" +
                "\tpadding: 4px 8px;\n" +
                "\tmargin-top: 24px;\n" +
                "}\n" +
                "\n" +
                "/* End Main Success */\n" +
                "\n" +
                "/* Start Message */\n" +
                ".omni-row .omni-row__message {\n" +
                "\tcolor: #fff;\n" +
                "\tmargin-top: 16px;\n" +
                "\tdisplay: block;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__message:not(.omni-hidden) {\n" +
                "\tmargin-top: 4px;\n" +
                "\tmargin-bottom: 16px;\n" +
                "}\n" +
                "\n" +
                "/* End Message */\n" +
                "\n" +
                "/* Start Copy Icon */\n" +
                ".omni-row .omni-box__copy {\n" +
                "\tcursor: pointer;\n" +
                "\tposition: absolute;\n" +
                "\twidth: 16px;\n" +
                "\theight: auto;\n" +
                "\ttop: 50%;\n" +
                "\tright: 8px;\n" +
                "\ttransform: translateY(-50%);\n" +
                "}\n" +
                "\n" +
                "/* End Copy Icon */\n" +
                "\n" +
                "/* Start Show More Button */\n" +
                ".omni-row .omni-row__showMoreBtn {\n" +
                "\tcursor: pointer;\n" +
                "\twidth: fit-content;\n" +
                "\tpadding-left: 24px;\n" +
                "}\n" +
                "\n" +
                "/* End Show More Button */\n" +
                "\n" +
                "/* Start Submit Button */\n" +
                ".omni-row .omni-row__submitBtn {\n" +
                "\tmargin-top: 24px;\n" +
                "\tcolor: #fff;\n" +
                "\tbackground: #ff7900;\n" +
                "}\n" +
                "\n" +
                ".omni-row .omni-row__submitBtn:focus,\n" +
                ".omni-row .omni-row__submitBtn:active,\n" +
                ".omni-row .omni-row__submitBtn:active:focus {\n" +
                "\toutline: 0;\n" +
                "}\n" +
                "\n" +
                "/* .omni-row .omni-row__submitBtn:hover {\n" +
                "\tborder: 2px solid #ff7900;\n" +
                "} */\n" +
                "\n" +
                "/* End Submit Button */\n" +
                "\n" +
                ".omni-row .omni-col__image {\n" +
                "\tmax-width: 300px;\n" +
                "\twidth: 100%;\n" +
                "}\n" +
                "\n" +
                ".omni-hidden {\n" +
                "\tvisibility: hidden !important;\n" +
                "}\n" +
                "\n" +
                "@media (max-width:1200px) {\n" +
                "\t.omni-ty-surprise {\n" +
                "\t\tpadding: 60px 14px;\n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "@media (max-width:1024px) {\n" +
                "\t.omni-row .omni-cols .omni-col:last-child {\n" +
                "\t\tpadding: 40px 10px;\n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "@media (max-width: 767px) {\n" +
                "\t.omni-row .omni-cols {\n" +
                "\t\tdisplay: block;\n" +
                "\t}\n" +
                "}" + "</style>");
        }
    }

    window.showXmasLottery = showXmasLottery;
    // var testers = [
    //     'aurelian.popescu@orange.com',
    //     'cosmin.velea@omniconvert.com',
    //     'anca.gogea@omniconvert.com',
    //     'raluca.vidrascu@orange.com',
    //     'laurentiu.mitrea@orange.com',
    //     'dragos.dumitrascu@orange.com',
    //     'andreea.dulama@orange.com'
    // ];

    jQuery(document).ready(function() {
        // var email = $('#_invoice_proform_WAR_webshopportlets_email').val();
        // document.location.href.indexOf('magazin-online/adaugat-la-cos/finalizare-comanda/comanda-ta') > -1 && testers.includes(email.toLowerCase()) && showXmasLottery();
        document.location.href.indexOf('magazin-online/adaugat-la-cos/finalizare-comanda/comanda-ta') > -1 && showXmasLottery();
    })
}();
