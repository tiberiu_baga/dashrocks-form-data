<script>
  (function () {

    respondToVisibility = function(element, callback) {
        var options = {
            root: document.documentElement
        }

        var observer = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
              callback(entry.intersectionRatio > 0);
            });
        }, options);

        observer.observe(element);
    }

    respondToVisibility(document.getElementById("successBody"), function(visible) {
        if(visible) {
            dataLayer.push({
                'event':'gaEventTriggered',
                'eventCategory': 'Lead Online Voce-TLS Page',
                'eventAction':'Lead Success',
                'eventLabel':'Click'});
        }
    });

    respondToVisibility(document.getElementById("errorBody"), function(visible) {
        if(visible) {
            dataLayer.push({
                'event':'gaEventTriggered',
                'eventCategory': 'Lead Online Voce-TLS Page',
                'eventAction':'Lead Failed',
                'eventLabel':'Click'});
        }
    });

})();
</script>
