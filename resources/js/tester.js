function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
var __regexTestEmail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
function __wrapElem(element) {
    var org_html = element.innerHTML;
    new_html = "<div class='submit_button_wrap'>" + org_html + "</div>";
    element.innerHTML = new_html;
}
function __drFormSaver(formData, formName) {
    console.log("__drFormSaver - " + formName);
    console.table(formData);
}
var form_mic365 = document.getElementById("telesales-form");
    if(form_mic365) {
        __wrapElem( document.getElementById("sendForm").parentElement );
        var submit_mic365 = document.getElementsByClassName("submit_button_wrap")[0];
        submit_mic365 && submit_mic365.addEventListener('click', function(e) {
            if( isNumeric(document.getElementById("phone_number").value)
                && document.getElementById("numeClient").value !== ""
                && document.getElementById("company_name").value !== ""
                && document.getElementById("emailAdress").value !== "" && __regexTestEmail.test(document.getElementById("emailAdress").value)
                && document.getElementById("gdpr").checked) {
                __drFormSaver({
                    'name' : document.getElementById("numeClient").value,
                    'phone' : document.getElementById("phone_number").value,
                    'email' : document.getElementById("emailAdress").value,
                    'gdpr' : 1,
                    'company_name' : document.getElementById("company_name").value
                }, "microsoft-365");
            }
        });
    }
