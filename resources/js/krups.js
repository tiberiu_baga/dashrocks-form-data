<script>
(function(){
    setTimeout(function() {
    function __wrapElem(element) {
        var org_html = element.outerHTML;
        new_html = "<div class='submit_button_wrap'>" + org_html + "</div>";
        element.outerHTML = new_html;
    }
    var __regexTestEmail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    var form_newsl = document.querySelector(".newsletter-form");
    if(form_newsl) {
        // __wrapElem( form_newsl.querySelector("button[type=submit]") );
        var submit_newsl = form_newsl.querySelector("button[type=submit]");
        submit_newsl && submit_newsl.addEventListener('click', function(e) {
            if(form_newsl.querySelector("input[name=newsletter]").value !== ""
                && __regexTestEmail.test( form_newsl.querySelector("input[name=newsletter]").value )
                && document.getElementById("gdpr").checked) {
                    dataLayer.push({
                        'event':"abonare-newsletter",
                        'form':"newsletterForm"
                    });
            }
        }, true);
    }

    var form_newsl2 = document.querySelector(".hedrick-fe-content");
    if(form_newsl2) {
        var submit_newsl2 = form_newsl2.querySelector(".hedrick-ButtonElement--content");
        submit_newsl2 && submit_newsl2.addEventListener('click', function(e) {
            if(document.getElementById("hedrick-field-email").value !== ""
                && __regexTestEmail.test( document.getElementById("hedrick-field-email").value )
                && document.getElementById("hedrick-field-name").value !== ""
                && form_newsl2.querySelector("[name=FieldsElement--privacyText-checkbox]").checked) {
                    dataLayer.push({
                        'event':"abonare-newsletter",
                        'form':"hedrick"
                    });
            }
        }, true);
    }

    setTimeout(function() {
        var form_newsl3 = document.querySelector(".arlington-fe-content");
        if(form_newsl3) {
            var submit_newsl3 = form_newsl3.querySelector(".arlington-ButtonElement--content");
            submit_newsl3 && submit_newsl3.addEventListener('click', function(e) {
                if(document.getElementById("arlington-field-email").value !== ""
                    && __regexTestEmail.test( document.getElementById("arlington-field-email").value )
                    && document.getElementById("arlington-field-name").value !== ""
                ) {
                        dataLayer.push({
                            'event':"abonare-newsletter",
                            'form':"popup-arlington"
                        });
                }
            }, true);
        }
    }, 5000);

    }, 1000);
})();
</script>
