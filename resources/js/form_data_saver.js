function __drFormSaver(formData, formName) {
    var __reqDr = new XMLHttpRequest();
    var __issetFunc = function(object) {
        if (typeof object !== 'undefined') {
            return object;
        }
        return '';
    }
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    var mySSOid = window.mySSOid || {'loggedIn': false, 'ssoId': null};
    if (__issetFunc(formData.first_name) && __issetFunc(formData.last_name) ) {
        var name = formData.first_name + " " + formData.last_name;
    } else if (__issetFunc(formData.firstName) && __issetFunc(formData.lastName) ) {
        var name = formData.firstName + " " + formData.lastName;
    } else {
        var name = __issetFunc(formData.name);
    }

    __reqDr.open("POST", "https://cbox.dash.rocks/4B450080E7F4EAC23A8C1C7082BB1A0090A0DF4EF990C96FE36697D63E6030CE");
    __reqDr.setRequestHeader("cache-control", "no-cache");
    __reqDr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    __reqDr.send(JSON.stringify({
            "robit": formName.concat('2afbd389089a5176e12ede2422e3540a'),
            "logged_id": mySSOid.loggedIn,
            "form": formName,
            "page": window.location.href,
            "agent": userAgent,

            "name": name,
            "email": __issetFunc(formData.email),
            "phone": __issetFunc(formData.phone),
            "state": __issetFunc(formData.state),
            "city": __issetFunc(formData.city),
            "address": __issetFunc(formData.address),
            "token": __issetFunc(fcmUserToken),
            "sso_id": mySSOid.ssoId,
            "subject": __issetFunc(formData.subject),
            "message": __issetFunc(formData.message),
            "gdpr": __issetFunc(formData.gdpr),
            "post_data": formData,
            "type" : __issetFunc(formData.type),
            "loyalty_eur" : __issetFunc(formData.loyalty_eur),
            "offer_eligibility" : __issetFunc(formData.offer_eligibility),
            "last_loyalty_date" : __issetFunc(formData.last_loyalty_date),
            "contract_expiration_date" : __issetFunc(formData.contract_expiration_date),
            "package_code" : __issetFunc(formData.package_code),
    }));
}
