<script type="text/javascript">
;
(function () {
    'use strict';

    if(hasFormCookie() || (typeof mySSOid !== 'undefined' && mySSOid.loggedIn)) {
         return;
    }
    var $;
    var $modal;
    var $body;
    var surveyId = {leadOnly: 49271, leadMessage: 49272};
    var shouldSend = true;
    var savedLeadData;

    var css = ''+
        '    .o-closed_acq .modal-backdrop.omni-backdrop {'+
        '        opacity: 0;'+
        '        visibility: hidden;'+
        '        transition: opacity 0.5s ease;'+
        '    }'+
        '    '+
        '    .o-closed_acq #omni-appointment.omni-acq {'+
        '        display: none;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq {'+
        '        position: fixed;'+
        '        margin: 0;'+
        '        bottom: 0;'+
        '        right: 55px;'+
        '        max-width: 390px;'+
        '        max-height: calc(100% - 10px);'+
        '        overflow-y: auto;'+
        '        -webkit-transform: translate(0);'+
        '        transform: translate(0);'+
        '        transition: transform 0.8s ease-in-out;'+
        '        border-radius: 4px 4px 0;'+
        '        z-index: 10001;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq select::-ms-expand {'+
        '        display: none;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq select::-ms-value {'+
        '        background: none;'+
        '        color: #000;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .modal-content {'+
        '        padding: 5px;'+
        '        border-radius: 0;'+
        '        background-clip: border-box;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .modal-head {'+
        '        position: relative;'+
        '        display: table;'+
        '        width: calc(100% + 12px);'+
        '        margin: -6px -6px 0 -6px;'+
        '        background: #f8d147;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .modal-head-text {'+
        '        position: relative;'+
        '        display: table-cell;'+
        '        padding: 0 30px 0 20px;'+
        '        font-size: 15px;'+
        '        font-family: sans-serif;'+
        '        line-height: 1.2;'+
        '        font-weight: 400;'+
        '        height: 66px;'+
        '        vertical-align: middle;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .modal-head-text svg {'+
        '        vertical-align: middle;'+
        '        margin-right: 15px;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .modal-body {'+
        '        padding: 10px 15px 10px;'+
        '    }'+
        '    '+
        '    /* form close */'+
        '    #omni-appointment.omni-acq .omni-form-close {'+
        '        position: absolute;'+
        '        width: 27px;'+
        '        height: 15px;'+
        '        top: 17px;'+
        '        right: 0;'+
        '        border: 0;'+
        '        background: url("//cdn-x.omniconvert.com/public/uploads/2019-09/8004aa6acfea23b26bf6bf06a6300a99.svg") no-repeat center left;'+
        '        background-size: contain;'+
        '        cursor: pointer;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-title {'+
        '        color: #fd6620;'+
        '        margin: 5px 0 20px;'+
        '        font-size: 25px;'+
        '        font-weight: 600;'+
        '        line-height: 1;'+
        '    }'+
        '    '+
        '    /* form title */'+
        '    #omni-appointment.omni-acq .omni-form-body {'+
        '        margin-top: 10px;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field {'+
        '        position: relative;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field:not(:first-of-type) {'+
        '        margin-top: 10px;'+
        '    }'+
        '    '+
        '    /* field label */'+
        '    #omni-appointment.omni-acq .omni-form-field label {'+
        '        font-family: Arial;'+
        '        margin: 0 0 5px;'+
        '        color: #171717;'+
        '        font-size: 12px;'+
        '        font-weight: 600;'+
        '        line-height: 1;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field sup {'+
        '        left: 2px;'+
        '        top: 1px;'+
        '        color: #ff7900;'+
        '        font-weight: bold;'+
        '        font-size: 15px;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field label,'+
        '    #omni-appointment.omni-acq .omni-form-field input.omni-input-f,'+
        '    #omni-appointment.omni-acq .omni-form-field select,'+
        '    #omni-appointment.omni-acq .omni-form-field textarea {'+
        '        display: block;'+
        '    }'+
        '    '+
        '    /* inputs */'+
        '    #omni-appointment.omni-acq .omni-form-field select,'+
        '    #omni-appointment.omni-acq .omni-form-field input.omni-input-f,'+
        '    #omni-appointment.omni-acq .omni-form-field textarea {'+
        '        height: 35px;'+
        '        line-height: 35px;'+
        '        width: 100%;'+
        '        padding: 0 8px;'+
        '        border-radius: 4px;'+
        '        background: #fff;'+
        '        border: 1px solid #c5c5c5;'+
        '        transition: all 0.2s ease;'+
        '        outline: none;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field select optgroup {'+
        '        font-weight: bold;'+
        '        color: #000;'+
        '    }'+
        '    #omni-appointment.omni-acq .omni-form-field select option.main-option {'+
        '        font-weight: bold;'+
        '        color: #000;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field.f-location::after,'+
        '    #omni-appointment.omni-acq .omni-form-field[f_type="select"]::after {'+
        '        content: \'\';'+
        '        position: absolute;'+
        '        right: 10px;'+
        '        bottom: 13px;'+
        '        width: 0;'+
        '        height: 0;'+
        '        border-left: 5px solid #fff;'+
        '        border-right: 5px solid #fff;'+
        '        border-top: 6px solid #000;'+
        '        pointer-events: none;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field[f_type="select"][disabled]::after {'+
        '        border-left: 5px solid #efefef;'+
        '        border-right: 5px solid #efefef;'+
        '        border-top: 6px solid #b7b7b7;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field select {'+
        '        -webkit-appearance: none;'+
        '        color: black;'+
        '    }'+
        '    '+
        '    /* textarea */'+
        '    #omni-appointment.omni-acq .omni-form-field textarea {'+
        '        padding: 8px;'+
        '        height: 170px;'+
        '        max-height: 200px;'+
        '        min-height: 50px;'+
        '        resize: vertical;'+
        '        line-height: 1.1;'+
        '    }'+
        '    '+
        '    /* disabled */'+
        '    #omni-appointment.omni-acq .omni-form-field[disabled] label,'+
        '    #omni-appointment.omni-acq .omni-form-field[disabled] sup,'+
        '    #omni-appointment.omni-acq .omni-form-field[disabled] [name] {'+
        '        color: #cccccc;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field input[disabled],'+
        '    #omni-appointment.omni-acq .omni-form-field select[disabled],'+
        '    #omni-appointment.omni-acq .omni-form-field textarea[disabled] {'+
        '        background: #EFEFEF;'+
        '    }'+
        '    '+
        '    /* submit button */'+
        '    #omni-appointment.omni-acq .omni-form-action {'+
        '        margin-top: 20px;'+
        '        text-align: center;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-action button {'+
        '        width: 100%;'+
        '        color: #fff;'+
        '        outline: none;'+
        '        width: auto;'+
        '        padding-left: 20px;'+
        '        background-color: #ff7801;'+
        '        padding-right: 20px;'+
        '    }'+
        '    '+
        '    /* disabled button */'+
        '    #omni-appointment.omni-acq .omni-form-action button[disabled] {'+
        '        background: #cccccc;'+
        '        border-color: #cccccc;'+
        '        opacity: 1;'+
        '        pointer-events: initial;'+
        '    }'+
        '    '+
        '    /* consent */'+
        '    #omni-appointment.omni-acq .omni-fields .f-consent {'+
        '        line-height: 12px;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .f-consent input#i_acq {'+
        '        display: none !important;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .f-consent input[type="checkbox"]+label::before {'+
        '        content: "";'+
        '        left: 0;'+
        '        position: relative;'+
        '        top: 3px;'+
        '        display: inline-block;'+
        '        background: url(https://www.orange.ro/newsletter/imagini/unchecked.png) no-repeat;'+
        '        background-size: contain;'+
        '        width: 17px;'+
        '        height: 17px;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .f-consent input[type="checkbox"]:checked+label::before {'+
        '        content: "";'+
        '        left: 0;'+
        '        position: relative;'+
        '        top: 3px;'+
        '        display: inline-block;'+
        '        width: 17px;'+
        '        height: 17px;'+
        '        background: #ff7801 url(https://www.orange.ro/newsletter/imagini/checked.png);'+
        '        background-size: contain;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .f-consent .omni_checkbox {'+
        '        cursor: pointer;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .f-consent input#i_acq,'+
        '    #omni-appointment.omni-acq .f-consent label[for] {'+
        '        color: #000000;'+
        '        margin: 0;'+
        '        font-size: 12px;'+
        '        font-family: Arial;'+
        '        display: inline;'+
        '        vertical-align: text-bottom;'+
        '        font-weight: 400;'+
        '        line-height: 1.4;'+
        '        cursor: pointer;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni_seeMore {'+
        '        background: #ff5800;'+
        '        padding: 0 5px;'+
        '        font-size: 14px;'+
        '        color: #fff;'+
        '        vertical-align: bottom;'+
        '        line-height: 14px;'+
        '        cursor: pointer;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni_seeMore::before {'+
        '        content: "+";'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-320br {'+
        '        display: none;'+
        '    }'+
        '    '+
        '    /* validations style */'+
        '    #omni-appointment.omni-acq .omni-error-acq select,'+
        '    #omni-appointment.omni-acq .omni-error-acq input,'+
        '    #omni-appointment.omni-acq .omni-error-acq textarea {'+
        '        border-color: #ff7272 !important;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-error-acq label {'+
        '        color: #ff4e4e;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-error-acq:not(.f-consent) label::after {'+
        '        font-size: 11px;'+
        '        display: inline;'+
        '        font-family: Arial;'+
        '        color: #ff4e4e;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .f-phone.omni-error-acq:not(.f-consent) label::after {'+
        '        content: " - Număr incomplet";'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .f-phone.omni-error-acq.o-length-error:not(.f-consent) label::after {'+
        '        content: " - Număr maxim caractere depaşit";'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .f-consent.omni-error-acq::after {'+
        '        content: \'*Pentru a continua trebuie să accepţi Termenii şi Condiţiile\';'+
        '        position: relative;'+
        '        display: block;'+
        '        margin-top: 8px;'+
        '        font-size: 12px;'+
        '        color: #ff0000;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni_fieldsInfo {'+
        '        position: relative;'+
        '        color: #000;'+
        '        padding-top: 10px;'+
        '        font-size: 12px;'+
        '        margin-bottom: 0;'+
        '        margin-top: 10px;'+
        '        padding-top: 8px;'+
        '        border-top: 1px solid #ccc;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni_fieldsInfo sup {'+
        '        top: 1px;'+
        '        margin-right: 2px;'+
        '        color: #ff7900;'+
        '        font-weight: bold;'+
        '        font-size: 15px;'+
        '    }'+
        '    '+
        '    /* end */'+
        '    #omni-appointment.omni-acq .omni-form-acq {'+
        '        position: relative;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-end-acq {'+
        '        position: absolute;'+
        '        left: 0;'+
        '        top: -10px;'+
        '        bottom: 0;'+
        '        width: 100%;'+
        '        font-size: 34px;'+
        '        text-align: center;'+
        '        background: #ffffff;'+
        '        opacity: 0.9;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-end-acq.omni-end-ty::after {'+
        '        content: "Îți mulțumim! Solicitarea a fost trimisă.";'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-end-acq.omni-end-error::after {'+
        '        content: "Legătura cu serverul nu s-a putut stabili. Te rugam să încerci mai târziu.";'+
        '        padding: 20px 5px;'+
        '        font-size: 26px;'+
        '        line-height: 1.2;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-end-acq::after {'+
        '        position: absolute;'+
        '        width: 100%;'+
        '        left: 0;'+
        '        top: 40%;'+
        '        -webkit-transform: translateY(-50%);'+
        '        transform: translateY(-50%);'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field .omni-input-f::-moz-placeholder {'+
        '        color: #afaeaf;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field .omni-input-f:-moz-placeholder {'+
        '        color: #afaeaf;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field .omni-input-f::-webkit-input-placeholder {'+
        '        color: #afaeaf;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field .omni-input-f::placeholder {'+
        '        color: #afaeaf;'+
        '    }'+
        '    '+
        '    #omni-appointment.omni-acq .omni-form-field .omni-input-f:-ms-input-placeholder {'+
        '        color: #afaeaf;'+
        '    }'+
        '    '+
        '    /* width */'+
        '    #omni-appointment.omni-acq .omni-dialog::-webkit-scrollbar {'+
        '        width: 10px;'+
        '    }'+
        '    '+
        '    /* Track */'+
        '    #omni-appointment.omni-acq .omni-dialog::-webkit-scrollbar-track {'+
        '        background: #d8d8d8;'+
        '    }'+
        '    '+
        '    /* Handle */'+
        '    #omni-appointment.omni-acq .omni-dialog::-webkit-scrollbar-thumb {'+
        '        background: #737272;'+
        '        border-radius: 10px;'+
        '    }'+
        '    '+
        '    /* in page blue ribbon style */'+
        '    #o-contact-ribbon-acq {'+
        '        padding: 7px 0;'+
        '        background-color: #f8d147;'+
        '    }'+
        '    '+
        '    #o-contact-ribbon-acq.o-double {'+
        '        border-top: 1px solid #ffffff;'+
        '    }'+
        '    '+
        '    #o-contact-ribbon-acq .container::after {'+
        '        content: \'\';'+
        '        display: table;'+
        '        clear: both;'+
        '    }'+
        '    '+
        '    #o-contact-ribbon-acq .container {'+
        '        position: relative;'+
        '        padding: 0;'+
        '    }'+
        '    '+
        '    #o-contact-ribbon-acq .o-ribbon-text {'+
        '        text-align: center;'+
        '        font-size: 15px;'+
        '        padding: 2px 0;'+
        '        line-height: 1;'+
        '    }'+
        '    '+
        '    #o-contact-ribbon-acq .o-ribbon-text svg {'+
        '        vertical-align: middle;'+
        '        margin-right: 10px;'+
        '    }'+
        '    '+
        '    #o-contact-ribbon-acq .o-action {'+
        '        cursor: pointer;'+
        '        color: #000;'+
        '        text-decoration: underline;'+
        '        white-space: nowrap;'+
        '    }'+
        '    '+
        '    #o-contact-ribbon-acq.o-double .container {'+
        '        padding: 0 20px;'+
        '    }'+
        '    '+
        '    #o-contact-ribbon-acq.o-double .o-action {'+
        '        right: 15px;'+
        '    }'+
        '    '+
        '    @media (max-width: 1200px) {'+
        '        #o-contact-ribbon-acq.o-double {'+
        '            margin-top: 0;'+
        '        }'+
        '    '+
        '        #o-contact-ribbon-acq {'+
        '            margin-top: 10px;'+
        '        }'+
        '    }'+
        '    '+
        '    @media (max-width: 767px) {'+
        '        #omni-appointment.omni-acq .omni-dialog {'+
        '            right: 0;'+
        '            left: 0;'+
        '            margin: 0 auto;'+
        '            max-height: calc(100% - 50px);'+
        '            overflow-y: auto;'+
        '        }'+
        '    '+
        '        #omni-appointment.omni-acq .modal-head-text::before {'+
        '            left: 5px;'+
        '        }'+
        '    '+
        '        #omni-appointment.omni-acq .modal-body {'+
        '            padding: 10px;'+
        '        }'+
        '    '+
        '        #omni-appointment.omni-acq .omni-form-field:not(:first-of-type) {'+
        '            margin-top: 12px;'+
        '        }'+
        '    '+
        '        #o-contact-ribbon-acq {'+
        '            margin-top: 0;'+
        '        }'+
        '    '+
        '        #o-contact-ribbon-acq .container {'+
        '            padding: 0 15px;'+
        '        }'+
        '    '+
        '        #o-contact-ribbon-acq .o-action {'+
        '            right: 15px;'+
        '        }'+
        '    }'+
        '    '+
        '    @media (max-width: 580px) {'+
        '        #omni-appointment.omni-acq {'+
        '            left: 0;'+
        '            right: 0;'+
        '            margin: 0 auto;'+
        '            border-radius: 0;'+
        '        }'+
        '    '+
        '        .o-closed_acq #omni-appointment.omni-acq {'+
        '            -webkit-transform: translateY(calc(100% - 65px));'+
        '            transform: translateY(calc(100% - 65px));'+
        '        }'+
        '    }'+
        '    '+
        '    @media (max-width: 359px) {'+
        '        #omni-appointment.omni-acq#omni-appointment.omni-acq .modal-head-text {'+
        '            font-size: 13px;'+
        '            padding-left: 40px;'+
        '            background-size: 17px;'+
        '        }'+
        '    '+
        '        #omni-appointment.omni-acq .modal-head-text::before {'+
        '            background-size: 19px;'+
        '            left: 0;'+
        '        }'+
        '}'
    ;

    var htmlRibbon = '' +
        '<div id="o-contact-ribbon-acq">' +
        '    <div class="container">' +
        '        <div class="o-ribbon-text">' +
        '           <svg width="24" height="24">' +
        '               <image xlink:href="//cdn-x.omniconvert.com/public/uploads/2020-05/a964cdbb648fcf0a86af4e9a4366199b.svg" src="//cdn-x.omniconvert.com/public/uploads/2020-05/953a357a61abf736dc65b2fa1d71e5bc.png" width="24" height="24"/>' +
        '           </svg>' +
        '            Ești client nou și vrei o ofertă personalizată?' +
        '           <a class="o-action">Detalii aici.</a>' +
        '        </div>' +
        '    </div>' +
        '</div>';
    var htmlModalLead = '' +
        '<div class="omni-acq" id="omni-appointment">' +
        '    <div class="omni-dialog">' +
        '        <div class="modal-content">' +
        '        <div class="modal-head">' +
        '           <div class="modal-head-text">' +
        '               <svg width="32" height="32">' +
        '                   <image xlink:href="//cdn-x.omniconvert.com/public/uploads/2020-05/a964cdbb648fcf0a86af4e9a4366199b.svg" src="//cdn-x.omniconvert.com/public/uploads/2020-05/953a357a61abf736dc65b2fa1d71e5bc.png" width="32" height="32"/>' +
        '               </svg>' +
        '               Completează formularul rapid!' +
        '           </div>' +
        '        </div>' +
        '            <div class="modal-body clearfix">' +
        '                <!-- form -->' +
        '                <form class="omni-form-acq acq-step-1">' +
        '                    <!-- form title -->' +
        '                        <div class="omni-form-body">' +
        '                            <div class="omni-fields">' +
        '                                <!-- field -->' +
        '                                <div class="omni-form-field f-name" f_type="input" f_state>' +
        '                                    <label>Nume și Prenume</label>' +
        '                                    <input class="omni-input-f" name="name" type="text" placeholder="Nume și Prenume" autocomplete="off" f_state>' +
        '                                </div>' +
        '                                <!-- field -->' +
        '                                <div class="omni-form-field f-phone" f_type="input" f_state f_required>' +
        '                                    <label>Număr de telefon<sup>*</sup></label>' +
        '                                    <input class="omni-input-f" name="phone" type="tel" inputmode="tel" placeholder="07XX XXX XXX" autocomplete="off" f_state>' +
        '                                </div>' +
        '                                <!-- field -->' +
        '                                <div class="omni-form-field f-topic" f_type="select" f_state f_required disabled>' +
        '                                    <label>Subiectul<sup>*</sup></label>' +
        '                                    <select class="omni-input-f" name="topic" f_state required disabled>' +
        '                                        <option class="omni-default-option" value="" selected disabled>Alege subiectul</option>' +
        '                                        <optgroup label="Abonament nou">' +
        '                                           <option value="Abonament nou - Internet fix si TV">Internet fix si TV</option>' +
        '                                           <option value="Abonament nou - Voce mobila cu telefon">Voce mobilă cu telefon</option>' +
        '                                           <option value="Abonament nou - Internet mobil">Internet mobil</option>' +
        '                                           <option value="Abonament nou - Televiziune prin satelit">Televiziune prin satelit</option>' +
        '                                        </optgroup>' +
        '                                        <option value="Migrare cartela la abonament" class="main-option">Migrare cartelă la abonament</option>' +
        '                                    </select>' +
        '                                </div>' +
        '                                <!-- field -->' +
        '                                <div class="omni-form-field f-consent" f_type="checkbox" >' +
        '                                    <input name="gdpr" type="checkbox" id="i_acq">' +
        '                                    <label for="i_acq" class="omni_checkbox" />' +
        '                                    <label class="omni_gdprText" for="i_acq">' +
        '                                        Sunt de acord ca Orange să mă contacteze și să ...' +
        '                                    </label>' +
        '                                    <span class="omni_seeMore" />' +
        '                                </div>' +
        '                                <div class="omni_fieldsInfo"><sup>*</sup>Câmpuri obligatorii</div>' +
        '                               <!-- submit button -->' +
        '                               <div class="omni-form-action f-submit">' +
        '                                   <button type="submit" class="omni-submit omni-f-app-submit btn btn-important" f_type="button" f_state disabled>Trimite</button>' +
        '                               </div>' +
        '                            </div>' +
        '                        </div>' +
        '                    </form>' +
        '                </div>' +
        '            <!-- close button -->' +
        '            <div class="omni-form-close" title="Deschide formuarul" />' +
        '            </div>' +
        '        </div>' +
        '    </div>';
    var htmlModalMessage = '' +
        '<form class="omni-form-acq acq-step-2">' +
        '    <div class="omni-form-title">Îți mulțumim!</div>' +
        '    <!-- form title -->' +
        '    <div class="omni-form-body">' +
        '        <div class="omni-fields">' +
        '            <!-- field -->' +
        '            <div class="omni-form-field f-message" f_type="textarea" f_state>' +
        '                <textarea class="omni-input-f" name="message" placeholder="Te mai putem ajuta cu ceva?" f_state />' +
        '            </div>' +
        '            <!-- submit button -->' +
        '            <div class="omni-form-action f-submit">' +
        '                <button type="submit" class="omni-submit omni-f-app-submit btn btn-important" f_type="button" f_state disabled>Trimite</button>' +
        '            </div>' +
        '        </div>' +
        '    </div>' +
        '</form>';

    run();


    function run() {
        /* jQuery is not available.. or bootstrap */
        /* for /case-de-marcat/ */
        if (!window.jQuery) {
            console.log('jQuery and Bootstrap scripts are unavailable. Loading them...');


            var jQueryScript = document.createElement('script');
            jQueryScript.src = 'https://code.jquery.com/jquery-1.11.3.min.js';
            jQueryScript.type = 'text/javascript';
            jQueryScript.onload = function() {
                var bootstrapScript = document.createElement('script');
                bootstrapScript.src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js';
                bootstrapScript.type = 'text/javascript';
                bootstrapScript.onload = function () {
                    console.log('jQuery and Bootstrap scripts were loaded. Re-running...');
                    run();
                };
                document.body.appendChild(bootstrapScript);
            };
            document.body.appendChild(jQueryScript);
            return;
        }


        $ = window.jQuery;


        $body = $('body');
        $modal = $(htmlModalLead);
        $body.addClass('o-closed_acq');


        insertStyle();
        insertPageRibbon();
        integrateModal();
        handleClose();
        runForm();
        trackGoals();


        // fix modal width for all steps, depending on 1st step
        $('.omni-acq').css('width', Math.ceil($('.omni-acq').width() + 1)+'px')
    }


    function insertStyle() {
        $('<style>').attr('id', 'omni-style-acq').html(css).appendTo($('head'));
    }


    function integrateModal() {
        var $outer = $('<div class="modal-backdrop omni-backdrop fade in" />');
        $outer.on('click', closeModal);
        $body.append($modal.add($outer));
    }


    function handleClose() {
        $modal.find('.omni-form-close').on('click', closeModal);
    }


    function closeModal() {
        $body.addClass('o-closed_acq');
        $modal.trigger('custom.close');
    }


    function runForm() {
        handleGdprButton();
        handleSubmit();


        var $selectFields = $modal.find('select');
        var $inputFields = $modal.find('[name]');


        $selectFields.on('change', toggleFieldAvailability);
        $inputFields.on('keyup blur', toggleFieldAvailability);


        removeErrorsOnFocus($inputFields);
    }


    function toggleFieldAvailability(e) {
        var $self = $(this);
        var $thisField = $self.parent();
        var $fields = $thisField.nextAll('div[f_state]');
        var validField = isFieldValid($self);


        if (validField) {
            enableFields($fields);
        } else {
            disableFields($fields);
        }


        handleEnterKey(e);
        handleCtaAvailability();
    }


    function handleEnterKey(e) {
        if (e.keyCode === 13) {
            var $self = $(e.target);
            var $inputs = $self.closest('form').find(':input');
            var index = $inputs.index(e.target);


            if (index === $inputs.length - 1) {
                $inputs[0].focus();
            } else {
                $inputs[index + 1].focus();
            }
        }
    }


    function handleCtaAvailability() {
        var $btn = $modal.find('.btn');
        var numAnyCompleted = $modal.find('.omni-success-acq').length;
        var numRequiredCompleted = $modal.find('[f_required].omni-success-acq').length;
        var numRequired = $modal.find('[f_required]').length;
        var allFieldsCompleted = numRequiredCompleted === numRequired;


        if (allFieldsCompleted && numAnyCompleted > 0) {
            $btn.removeAttr('disabled');
        } else {
            $btn.attr('disabled', '');
        }
    }


    function enableFields($nextFields) {
        var $nextField = $nextFields.eq(0);


        $nextField
            .removeAttr('disabled')
            .find('[f_state]')
            .removeAttr('disabled');


        for (var i = 0; i < $nextFields.length; i++) {
            var $currentField = $nextFields.eq(i).removeClass('omni-error-acq');
            var $self = $currentField.find('[f_state]');


            if ($self.val()) {
                $currentField.removeAttr('disabled').attr('completed', '');
                $self.removeAttr('disabled');


                if (!isFieldValid($self)) {
                    return;
                }


            } else if ($currentField.prev().hasClass('omni-success-acq')) {
                $currentField.removeAttr('disabled').attr('completed', '');
                $self.removeAttr('disabled');
                return;
            }
        }
    }


    function disableFields($fields) {
        for (var i = 0; i < $fields.length; i++) {
            disableField($fields.eq(i));
        }
    }


    function disableField($field) {
        $field
            .attr('disabled', '')
            .removeAttr('completed')
            .removeClass('omni-error-acq')
            .find('[f_state]')
            .attr('disabled', '');
    }


    function handleSubmit() {
        $modal
            .find('.omni-f-app-submit')
            .on('click', tryToSubmit);
    }


    function tryToSubmit(e) {
        e.preventDefault();


        var $form = $(e.target).closest('form');
        var step;
        if ($form.hasClass('acq-step-1')) {
            step = 1;
        } else if ($form.hasClass('acq-step-2')) {
            step = 2;
        }


        var $errors = getErrors();


        /* scroll to first error */
        if ($errors.length) {
            scrollToElement($errors.first());
            return;
        }


        sendLead(step);
    }


    function getErrors() {
        var $inputFields = $modal.find('[name]');
        for (var i = 0; i < $inputFields.length; i++) {
            isFieldValid($inputFields.eq(i));
        }


        return $('.omni-error-acq');
    }


    function isFieldValid($self) {
        var fieldName = $self.attr('name');


        switch (fieldName) {
            case 'name':
                return validateName($self);


            case 'phone':
                return validatePhoneNumber($self);


            case 'topic':
                return validateTopic($self);


            case 'message':
                 return validateMessage($self);


            /*
             case 'gdpr':
                     return validateGDPR($self);
            */


            default:
                return false;
        }
    }


    function validateName($self) {
        var value = $self.val().trim();
        var nameRegex = /^\D+$/;
        var minNameLength = 2;
        var valid = value.length === 0 || value.length > minNameLength && nameRegex.test(value);


        if (valid) {
            $self.attr('placeholder', 'Nume și Prenume');
            removeError($self.parent());
        } else if (value.length !== 0) {
            $self.attr('placeholder', '* Numele tău nu este corect');
            addError($self.parent());
        } else if (value.length <= minNameLength) {
            $self.attr('placeholder', 'Numele tău nu este complet');
            addError($self.parent());
        }


        return valid;
    }


    function validatePhoneNumber($self) {
        var maxLength = 10;
        var phoneValue = $self.val().replace(/[^0-9+]/g, '');
        var withCountryCode = /^(\+)/.test(phoneValue);
        var withDoubleZ = /^(00)/.test(phoneValue);


        if (withCountryCode) {
            maxLength = 12;
            phoneValue = '+' + phoneValue.replace(/[^0-9]/g, '');
        } else if (withDoubleZ) {
            maxLength = 13;
            phoneValue = phoneValue.replace(/[^0-9]/g, '');
        } else {
            maxLength = 10;
            phoneValue = phoneValue.replace(/[^0-9]/g, '');
        }


        $self.val(phoneValue);


        var valid = phoneValue.length == maxLength;


        if (valid) {
            removeError($self.parent(), 'o-length-error');
        } else if (phoneValue.length > maxLength) {
            addError($self.parent(), 'o-length-error');
        } else {
            addError($self.parent());
        }


        return valid;
    }


    function validateTopic($self) {
        var valid = $self.val() && $self.val().length > 0;
        if (valid) {
            removeError($self.parent());
        } else {
            addError($self.parent());
        }


        return valid;
    }


    function validateMessage($self) {
        var valid = $self.val().replace(/\s/g, '').length > 3;
        if (valid) {
            removeError($self.parent());
        } else {
            addError($self.parent());
        }


        return valid;
    }


    function validateGDPR($self) {
        var valid = $self.is(':checked');
        if (valid) {
            removeError($self.parent());
        } else {
            addError($self.parent());
        }


        return valid;
    }


    function addError($field, optionalClass) {
        optionalClass = optionalClass || '';
        $field
            .addClass('omni-error-acq ' + optionalClass)
            .removeClass('omni-success-acq');
    }


    function removeError($field, optionalClass) {
        optionalClass = optionalClass || '';
        $field
            .removeClass('omni-error-acq ' + optionalClass)
            .addClass('omni-success-acq');
    }


    function removeErrorsOnFocus($inputFields) {
        $inputFields.on('focus change', function () {
            $(this)
                .parent()
                .removeClass('omni-error-acq');
        });
    }


    function handleGdprButton() {
        $modal.find('.omni_seeMore').on('click', showFullGdpr);
    }


    function showFullGdpr() {
        var fullGdpr = '' +
            'Sunt de acord ca Orange să mă contacteze și ' +
            'să utilizeze datele mele personale, inclusiv ' +
            'zona în care utilizez serviciile Orange, pentru ' +
            'a-mi oferi cele mai bune oferte și servicii ' +
            'personalizate în funcție de nevoile mele.';


        $modal
            .addClass('o_fullGdpr')
            .find('.omni_gdprText')
            .html(fullGdpr);


        $(this).remove();
    }


    function sendLead(step) {
        var leadData;


        if (step === 1) {
            var nameVal = $modal.find('.f-name [name]').val();
            var phoneVal = $modal.find('.f-phone [name]').val();
            var topicVal = $modal.find('.f-topic [name]').val();
            var gdprVal = $modal.find('.f-consent [name]').is(':checked') ? 'DA' : 'NU';


            leadData = {
                name: nameVal,
                phone: phoneVal,
                topic: topicVal,
                gdpr: gdprVal
            };


            // save lead data for next steps
            savedLeadData = leadData;
        } else {
            // load lead data from previous steps
            leadData = savedLeadData;
        }


        if (step === 2) {
            leadData.message = $modal.find('.f-message [name]').val();
        }


        $modal
            .find('.omni-submit')
            .attr('disabled', 'disabled')
            .text('Se trimite...');


        sendOroLead(leadData, step);
    }


    function sendOroLead(leadData, step) {
        var handleOroLeadStepSuccess = handleOroLeadSuccess.bind(null, step, leadData);
        __drFormSaver(leadData, 'homapage-wecallyou');
        /*
        $.ajax({
            data: leadData,
            type: 'POST',
            url: 'https://orfobq.appspot.com/c',
            success: handleOroLeadStepSuccess,
            error: function () {
                showErrorMessage();
            }
        });
        */
        // simulate success from API
        handleOroLeadStepSuccess(JSON.stringify({meta: {success: {title: "success"}}}));
        setFormCookie();

        // todo: fetch data and send lead
        console.log('Sending lead data...', leadData);
    }


    function handleOroLeadSuccess(step, leadData, response) {
        var res = JSON.parse(response) || '';
        var path = window.location.pathname;


        if (res.meta && res.meta.success.title == 'success') {
            nextFormStep(step);


            // track success
            switch (step) {
                case 1:
                    var gaLabel = 'ga:' + getGaClientId() + '|' + anonimizePhone(leadData.phone) + '|' + leadData.topic + '|GDPR:' + leadData.gdpr;
                    gaTrack('Trimite1', gaLabel);
                    break;
                case 2:
                    gaTrack('Trimite2', 'mesaj');
                    break;
                default:
                    console.error('The step ' + step + ' is not available within ACQ lead success');
            }
        } else if (res.errors) {
            showErrorMessage();
        }
    }


    function nextFormStep(step) {
        switch (step) {
            case 1:
                showAdditionalMessage();
                break;
            case 2:
                showThankYouMessage();
                break;
            default:
                console.error('The step ' + step + ' is not available within ACQ next form step');
        }
    }


    function scrollToElement($target) {
        var offset = $target.offset();


        if (!offset) {
            return;
        }
        $modal
            .find('.modal-dialog')
            .animate({
                scrollTop: offset.top
            }, 500);
    }


    function showAdditionalMessage() {
        var $additionalMessageForm = $(htmlModalMessage).css('display', 'none');
        $modal.find('.modal-body').append($additionalMessageForm);
        $('.omni-form-acq.acq-step-1').remove();
        $('.modal-head').hide();
        $additionalMessageForm.show();
        runForm();
    }


    function showThankYouMessage() {
        var $endMessage = $('<div class="omni-end-acq omni-end-ty" style="display: none;" />');
        $modal.find('.omni-form-acq').append($endMessage);
        $endMessage.fadeIn();


        gaTrack('View TYP2', 'afisare final');


        setTimeout(function () {
            shouldSend = false;
            closeModal();
            $modal.remove();
            $('#o-contact-ribbon-acq').remove();
        }, 3000);
    }


    function showErrorMessage() {
        var $endMessage = $('<div class="omni-end-acq omni-end-error" style="display: none;" />');
        $modal.find('.omni-form-acq').append($endMessage);
        $endMessage.fadeIn();


        setTimeout(function () {
            $endMessage.fadeOut('fast', function () {
                $modal.find('.omni-submit').text('Trimite').removeAttr('disabled');
                $endMessage.remove();
            });
        }, 3000);
    }


    function trackGoals() {
        $modal.on('custom.close', function () {
            if (shouldSend) {
                var step;
                if ($('.omni-end-acq.omni-end-ty').length) {
                    step = 3;
                } else if ($('[name="message"]').length) {
                    step = 2;
                } else if ($('[name="name"]').length) {
                    step = 1;
                }
                gaTrack('Close Modal', 'pasul ' + step);
            }
        });


        $modal
            .find('select, input, textarea')
            .on('click', function () {
                var fieldName = $(this).attr('name');
                gaTrack(fieldName, 'ga:' + getGaClientId());
            });
    }


    function insertPageRibbon() {
        var $ribbon = $(htmlRibbon);


        $ribbon
            .insertAfter('.search-wrap:first')
            .find('.o-action')
            .on('click', function () {
                $body.removeClass('o-closed_acq');
                gaTrack('Display Modal', 'ga:' + getGaClientId());
            });


        if ($('div#widget_love_local').length) {
            $ribbon.addClass('o-double').insertAfter('div#widget_love_local');
        }
    }


    function getGaClientId() {
        return ga.getAll()[0].get('clientId');
    }


    function anonimizePhone(phone) {
        return phone.substring(0, phone.length - 6) + 'XXXXXX';
    }


    function gaTrack(action, label) {
        if (!window.ga) {
            console.error('Google Analytics tracker is not available');


            return;
        }
        var trackerName = ga.getAll()[0].get('name');


        window.ga(trackerName + '.send', 'event', 'Formular ACQ', action, label);
    }

    function __drFormSaver(formData, formName) {
        var __reqDr = new XMLHttpRequest();
        var __issetFunc = function(object) {
            if (typeof object !== 'undefined') {
                return object;
            }
            return '';
        }
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        if (__issetFunc(formData.first_name) && __issetFunc(formData.last_name) ) {
            var name = formData.first_name + " " + formData.last_name;
        } else if (__issetFunc(formData.firstName) && __issetFunc(formData.lastName) ) {
            var name = formData.firstName + " " + formData.lastName;
        } else {
            var name = __issetFunc(formData.name);
        }
        if (typeof mySSOid == 'undefined') { var mySSOid = {'loggedIn': false, 'ssoId': null}; }
        __reqDr.open("POST", "https://cbox.dash.rocks/4B450080E7F4EAC23A8C1C7082BB1A0090A0DF4EF990C96FE36697D63E6030CE");
        __reqDr.setRequestHeader("cache-control", "no-cache");
        __reqDr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        __reqDr.send(JSON.stringify({
                "robit": formName.concat('2afbd389089a5176e12ede2422e3540a'),
                "logged_id": mySSOid.loggedIn,
                "form": formName,
                "page": window.location.href,
                "agent": userAgent,

                "name": name,
                "email": __issetFunc(formData.email),
                "phone": __issetFunc(formData.phone),
                "state": __issetFunc(formData.state),
                "city": __issetFunc(formData.city),
                "address": __issetFunc(formData.address),
                "token": __issetFunc(fcmUserToken),
                "sso_id": mySSOid.ssoId,
                "subject": __issetFunc(formData.subject),
                "message": __issetFunc(formData.message),
                "gdpr": __issetFunc(formData.gdpr),
                "post_data": formData,
        }));
    }

    function setFormCookie() {
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + 1000*36000;
        now.setTime(expireTime);
        document.cookie = 'mk_client_homepage=formSubmitted;expires='+now.toGMTString()+';path=/';
    }

    function hasFormCookie() {
        return document.cookie.match(/^(.*;)?\s*mk_client_homepage\s*=\s*[^;]+(.*)?$/);
    }
}());
</script>
