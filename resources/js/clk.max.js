console.log('cbox collector loaded v1.5');
(function(){
    var __regexTestEmail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var pagePath = window.location.href;

    function getUtmValues() {
        const queryParams = new Proxy(new URLSearchParams(window.location.search), {
            get: (searchParams, prop) => searchParams.get(prop),
        });
        var utmParams = {
            'utm_campaign': queryParams.utm_campaign,
            'utm_source': queryParams.utm_source,
            'utm_medium': queryParams.utm_medium
        };
        var utmz = getCookie('utmz-global');
        if (utmz) {
            const queryParamsCookie = new Proxy(new URLSearchParams(decodeURIComponent(utmz)), {
                get: (searchParams, prop) => searchParams.get(prop),
            });
            if (queryParamsCookie.cb_campaign !== null && utmParams['utm_campaign'] == null) utmParams['utm_campaign'] = queryParamsCookie.cb_campaign;
            if (queryParamsCookie.cb_source   !== null && utmParams['utm_source']   == null) utmParams['utm_source']   = queryParamsCookie.cb_source;
            if (queryParamsCookie.cb_medium   !== null && utmParams['utm_medium']   == null) utmParams['utm_medium']   = queryParamsCookie.cb_medium;
        }
        return utmParams;
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function getReff(){
        var lastref = "direct";
        var cookieReff = getCookie('__reff');
        if (cookieReff != undefined){
            lastref = cookieReff.split("|")[cookieReff.split("|").length-1];
            var regex = /\[\[.*\]\]+(.*)\&[0-9.]+/g;
            lastref = lastref.replace(regex, "$1");
            if (lastref.indexOf('campaign')>-1){
              var regex2= /.*\m\:+(.*)/g;
              lastref = "m:"+lastref.replace(regex2,"$1");
            }
        }
        return lastref;
    }

    function __drFetchFormData(form, mapper) {
        var ___formData = new Object();
        if (typeof form == typeof undefined) {
            return ___formData;
        }
        var elements = form.elements;
        for (var i = 0, element; element = elements[i++];) {
            if (typeof element.name !== typeof undefined && element.name !== false && element.type != 'submit') {
                if(element.type == 'checkbox') {
                     ___formData[element.name] = element.checked ? 1 : 0;
                 } else {
                    ___formData[element.name] = element.value;
                 }
            }
        }
        for (var key in mapper) {
            var newKey = mapper[key];
            ___formData[newKey] = ___formData[key];
            delete ___formData[key];
        }
        return ___formData;
    }

    function __wrapElem(element) {
        var org_html = element.innerHTML;
        new_html = "<div class='submit_button_wrap'>" + org_html + "</div>";
        element.innerHTML = new_html;
    }

    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function setCookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

    function __drFormSaver(formData, formName) {
        var __reqDr = new XMLHttpRequest();
        var __issetFunc = function(object) {
            if (typeof object !== 'undefined') {
                return object;
            }
            return '';
        }
        var fcmUserToken = (typeof fcmUserToken === 'undefined') ? null : fcmUserToken;
        if (__issetFunc(formData.first_name) && __issetFunc(formData.last_name) ) {
            var name = formData.first_name + " " + formData.last_name;
        } else if (__issetFunc(formData.firstName) && __issetFunc(formData.lastName) ) {
            var name = formData.firstName + " " + formData.lastName;
        } else {
            var name = __issetFunc(formData.name);
        }
        var __drSource = getUtmValues();

        if (typeof mySSOid == 'undefined') { mySSOid = {'loggedIn': false, 'ssoId': null}; }
        __reqDr.open("POST", "https://cbox.mydigitalorange.ro/4B450080E7F4EAC23A8C1C7082BB1A0090A0DF4EF990C96FE36697D63E6030CE");
        __reqDr.setRequestHeader("cache-control", "no-cache");
        __reqDr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        __reqDr.send(JSON.stringify({
                "robit": formName.concat('2afbd389089a5176e12ede2422e3540a'),
                "logged_id": mySSOid.loggedIn,
                "form": formName,
                "page": window.location.href,
                "agent": (navigator.userAgent || navigator.vendor || window.opera),
                "name": name,
                "email": __issetFunc(formData.email),
                "phone": __issetFunc(formData.phone),
                "state": __issetFunc(formData.state),
                "city": __issetFunc(formData.city),
                "address": __issetFunc(formData.address),
                "token": __issetFunc(fcmUserToken),
                "sso_id": mySSOid.ssoId,
                "subject": __issetFunc(formData.subject),
                "message": __issetFunc(formData.message),
                "gdpr": __issetFunc(formData.gdpr),
                'source': __issetFunc(formData.source),
                'referrer': getReff(),
                'utm_campaign': __drSource['utm_campaign'],
                'utm_medium': __drSource['utm_medium'],
                'utm_source': __drSource['utm_source'],
                "post_data": formData,
        }));
        return true;
    }

    function orangeColector() {
        var campaignNodes = document.querySelectorAll('[id^="campaignCodeLeads"], [id^="sourceLeads"], [id^="mediumLeads"]');
        campaignNodes.forEach(myFunction);

        function myFunction(item, index, arr) {
            var __drUtms = getUtmValues();
            if(document.getElementById(item.id)) {
                if(item.id.toString().indexOf('campaignCodeLeads') !== -1) {
                    document.getElementById(item.id).value = __drUtms['utm_campaign'];
                }
                if(item.id.toString().indexOf('sourceLeads') !== -1) {
                    document.getElementById(item.id).value = __drUtms['utm_source'];
                }
                if(item.id.toString().indexOf('mediumLeads') !== -1) {
                    document.getElementById(item.id).value = __drUtms['utm_medium'];
                }
            }
        }
    }

    function cBoxCollector() {
        if( pagePath.indexOf("/antreprenori") > -1 ) {
            var form_name = "antreprenori";
            if( pagePath.indexOf("/antreprenori/love/local") > -1 ) {
                form_name = "antreprenori-love-local";
            } else if( pagePath.indexOf("/antreprenori/love") > -1 ) {
                form_name = "antreprenori-love";
            }
            setTimeout(function(){
                var form_anlolo = document.getElementsByClassName("omni-form-AG18792")[0];
                if(form_anlolo) {
                    var submit_anlolo = form_anlolo.getElementsByClassName("omni-form-action f-submit")[0];
                    submit_anlolo && submit_anlolo.addEventListener('click', function(e) {
                        if(document.getElementById('i_AG18792').checked) {
                            __drFormSaver(
                                __drFetchFormData(form_anlolo, {'contactName' : 'name', 'phoneNumber': 'phone'}),
                                form_name
                            );
                        }
                    });
                }
            }, 2000);

            var form_business = document.getElementById("form-business");
            if(form_business) {
                var submit_business = document.getElementById("sendForm");
                submit_business && submit_business.addEventListener('click', function(e) {
                    var inputNumber = document.getElementById('nrTel').value;
                    if(document.getElementById('gdprBusiness').checked &&
                        inputNumber.length == 10 && isNumeric(inputNumber) &&
                        submit_business.disabled === false ) {
                        __drFormSaver({
                            'phone' : document.getElementById('nrTel').value,
                            'cif' : document.getElementById('atributFiscal').value + document.getElementById('codFiscal').value,
                            'message' : document.getElementById('b2b-msg').value,
                            'gdpr' : 1,
                        }, form_name);
                    }
                });
            }
        }
        else if (pagePath.indexOf("/verifica-acoperirea") > -1) {
            var verifAcopPhone = document.getElementById('phoneNumber');
            var verifAcopGdpr = document.getElementById('gdprCheck');
            var __verificAcopTrack = function(phone, gdpr, form) {
                if(phone.value.length == 10 && gdpr.checked ) {
                    __drFormSaver({
                        'phone' : phone.value,
                        'gdpr' : 1,
                        'campaignCheck' :  document.getElementById('campaignCheck') && document.getElementById('campaignCheck').checked ? 1 : 0,
                        'state' : document.getElementById('searchJudet').value,
                        'city' : document.getElementById('searchLocalitate').value,
                        'address' : document.getElementById('searchStreet').value + ', ' +
                        document.getElementById('searchNumber').value + ', ' +
                        document.getElementById('searchBl').value + ', ' +
                        document.getElementById('searchScara').value
                    }, form);
                }
            }
            var eltostep3 = document.getElementById('to-step-3');
            eltostep3 && eltostep3.addEventListener('click', function(e) {
                __verificAcopTrack(verifAcopPhone, verifAcopGdpr, "verifica-acoperirea");
            });
            var elsubmitButton = document.getElementById('submitButton');
            elsubmitButton && elsubmitButton.addEventListener('click', function(e) {
                __verificAcopTrack(verifAcopPhone, verifAcopGdpr, "verifica-acoperirea")
            });
            if (document.getElementById('requestContact')) {
                document.getElementById('requestContact').parentNode.classList.add("verifica-acoperirea-submit-d");
                document.getElementsByClassName("verifica-acoperirea-submit-d")[0].addEventListener('click', function(e) {
                    __verificAcopTrack(verifAcopPhone, verifAcopGdpr, "verifica-acoperirea-servicii")
                });
            }
            if (document.getElementById('submitRequest')) {
                document.getElementById('submitRequest').parentNode.classList.add("verifica-acoperirea-submit-dd");
                document.getElementsByClassName("verifica-acoperirea-submit-dd")[0].addEventListener('click', function(e) {
                    __verificAcopTrack(document.getElementById('phoneNumber2'), document.getElementById('gdprCheck2'), "verifica-acoperirea-servicii");
                });
            }
        }
        else if (pagePath.indexOf("/love/configurator") > -1) {
            var __loveConfigTrack = function(phone, gdpr, form) {
                if(phone.value.length == 10 && gdpr.checked ) {
                    __drFormSaver({
                        'phone' : phone.value,
                        'gdpr' : 1,
                        'form' : form,
                        'state' : document.getElementById('searchJudet').value,
                        'city' : document.getElementById('searchLocalitate').value,
                        'address' : document.getElementById('searchStreet').value + ', ' +
                        document.getElementById('searchNumber').value + ', ' +
                        document.getElementById('searchBl').value + ', ' +
                        document.getElementById('searchScara').value
                    }, form);
                }
            }
            if (document.getElementById('submitLead')) {
                __wrapElem(document.getElementById('submitLead'));
                var submit_lead = document.getElementsByClassName("submit_button_wrap")[0];
                document.getElementsByClassName("submit_button_wrap")[0].addEventListener('click', function(e) {
                    __loveConfigTrack(document.getElementById('phoneNumber'), document.getElementById('gdprCheck'), "online-leads-love-configurator");
                });
            }
        }
        else if (pagePath.indexOf("/shops") > -1) {
            setTimeout(function(){
                var form_shops = document.getElementsByClassName("omni-form-AG18646")[0];
                if(form_shops) {
                    var submit_shops = form_shops.getElementsByClassName("omni-submit")[0];
                    submit_shops && submit_shops.addEventListener('click', function(e) {
                        if(document.getElementsByName('gdpr')[0].checked) {
                            __drFormSaver(__drFetchFormData(form_shops, {}), "shops");
                        }
                    });
                }
            }, 1500);
        }
        else if (pagePath.indexOf('/business/oferte-pentru-companii/oferte-beneficii-flexibile') > -1) {
            var form_micos = document.getElementsByClassName("omni-form-AG21927")[0];
            if(form_micos) {
                var submit_micos = form_micos.getElementsByClassName("omni-form-action f-submit")[0];
                submit_micos.addEventListener('click', function(e) {
                    __drFormSaver(__drFetchFormData(form_micos, {}), "microsoft");
                });
            }
        }
        else if (pagePath.indexOf('/contact/') > -1) {
            var form_contact = document.getElementById("eContact");
            if(form_contact) {
                var submit_contact = form_contact.querySelector('button[type="submit"]');
                __wrapElem(submit_contact);
                var submit_contact = form_contact.getElementsByClassName("submit_button_wrap")[0];
                submit_contact && submit_contact.addEventListener('click', function(e) {
                    var inputNumber = document.getElementById('inputNumber').value;
                    var inputName = document.getElementById('inputName').value;
                    var inputEmail = document.getElementById('inputEmail').value;
                    var inputTextarea = document.getElementById('inputTextarea').value;
                    var exampleFormControlSelect1 = document.getElementById('exampleFormControlSelect1');
                    var subcategory = document.getElementById('subcategory');
                    var subcategorySelected = (subcategory && subcategory.options) ? subcategory.options[ subcategory.options.selectedIndex ].text : '';
                    if( inputNumber.length == 10 && isNumeric(inputNumber)
                        && inputName !== ""
                        && inputEmail !== "" && __regexTestEmail.test(inputEmail)
                        && exampleFormControlSelect1.value !== ""
                        && inputTextarea.length > 30  && inputTextarea.length <= 4000
                        && form_contact.querySelectorAll('.error').length < 2) {
                            __drFormSaver({
                                'name' : inputName,
                                'phone' : inputNumber,
                                'email' : inputEmail,
                                'category' : exampleFormControlSelect1.options[ exampleFormControlSelect1.options.selectedIndex ].text,
                                'subcategory' : subcategorySelected,
                                'message' : inputTextarea,
                                'gdpr' : 0
                            }, "contact");
                    }
                });
            }
        }
        else if (pagePath.indexOf('/client-nou') > -1 && pagePath.indexOf('abonamente/oferta-portare/client-nou/') == -1) {
            var submit_clinou = document.getElementById("submitOffer");
            if(submit_clinou) {
                __wrapElem(submit_clinou);
                var submit_clinou = document.getElementsByClassName("submit_button_wrap")[0];
                submit_clinou && submit_clinou.addEventListener('click', function(e) {
                    if( document.getElementById('phoneNumberLeads').value.length == 10  && isNumeric(document.getElementById('phoneNumberLeads').value)
                        && document.getElementById('gdprCheckLeads').checked) {
                        __drFormSaver({
                            'phone' : document.getElementById('phoneNumberLeads').value,
                            'gdpr' : 1,
                        }, "client-nou");
                    }
                });
            }
        }
        else if (pagePath.indexOf('/newsletter') > -1) {
            var form_newsle = document.getElementById("subscribe");
            if(form_newsle) {
                var submit_newsle = form_newsle.querySelector('.abonws');
                submit_newsle && submit_newsle.addEventListener('click', function(e) {
                    if(document.getElementById('Email').value !== ""
                        && __regexTestEmail.test(document.getElementById('Email').value)
                        && document.getElementById('gdprForm').checked) {
                        __drFormSaver({
                            'email' : document.getElementById('Email').value,
                            'gdpr' : 1
                        }, "newsletter-page");
                    }
                });
            }
        }
        else if (pagePath.indexOf('/business/oferte-pentru-companii/scoala-') > -1) {
            var submit_bopcso = document.getElementById('sendForm');
            submit_bopcso && submit_bopcso.addEventListener('click', function(e) {
               if(  document.getElementsByClassName("error-phone")[0].innerHTML.length == 0
                    && isNumeric(document.getElementById('phone_number').value)
                    && document.getElementById('numeClient').value !== "" && document.getElementById('localitateInstitutie').value !== ""
                    && document.getElementById('judetInstitutie').value !== "" && document.getElementById('numeleInstitutie').value !== ""
                    && document.getElementById('emailAdress').value !== "" && __regexTestEmail.test(document.getElementById('emailAdress').value)
                    && document.getElementById('gdpr').checked ) {
                    __drFormSaver({
                        'name' : document.getElementById('numeClient').value,
                        'phone' : document.getElementById('phone_number').value,
                        'email' : document.getElementById('emailAdress').value,
                        'gdpr' : 1,
                        'localitateInstitutie' : document.getElementById('localitateInstitutie').value,
                        'judetInstitutie' : document.getElementById('judetInstitutie').value,
                        'numeleInstitutie' :  document.getElementById('numeleInstitutie').value
                    }, "scoala_digitala");
                }
            });
        }
        else if (pagePath.indexOf('/business/solutii/securitate-cibernetica') > -1) {
            setTimeout(function(){
                var form_seccib = document.getElementById("omni-appointment");
                if(form_seccib) {
                    __wrapElem( form_seccib.querySelector('.omni-submit') );
                    var submit_seccib = document.getElementsByClassName("submit_button_wrap")[0];
                    submit_seccib && submit_seccib.addEventListener('click', function(e) {
                        if(  isNumeric(form_seccib.querySelector('input[name=phone]').value )
                            && form_seccib.querySelector('input[name=phone]').value !== ""
                            && form_seccib.querySelector('input[name=name]').value !== ""
                            && form_seccib.querySelector('input[name=email]').value !== ""
                            && __regexTestEmail.test(form_seccib.querySelector('input[name=email]').value)
                            && form_seccib.querySelector('input[name=gdpr]').checked ) {
                            __drFormSaver({
                                'name' : form_seccib.querySelector('input[name=name]').value,
                                'phone' : form_seccib.querySelector('input[name=phone]').value,
                                'company' : form_seccib.querySelector('input[name=company]').value,
                                'role' : form_seccib.querySelector('input[name=role]').value ,
                                'email' : form_seccib.querySelector('input[name=email]').value,
                                'gdpr' : 1
                            }, "securitate_cibernetica");
                        }
                    });
                }
            }, 2000);

            var __setCollectorSeccib2 = function() {
                var form_seccib2 = document.getElementById("raportbisform");
                if(form_seccib2) {
                    __wrapElem( document.getElementById("sendForm").parentElement );
                    var submit_seccib2 = document.getElementsByClassName("submit_button_wrap")[0];
                    submit_seccib2 && submit_seccib2.addEventListener('click', function(e) {
                        if(  isNumeric(document.getElementById("telefon").value)
                            && document.getElementById("telefon").value !== ""
                            && document.getElementById("nume").value !== "" && document.getElementById("prenume").value !== ""
                            && document.getElementById("emailclient").value !== ""
                            && __regexTestEmail.test(document.getElementById("emailclient").value)
                            && document.getElementById("gdpr").checked) {
                            __drFormSaver({
                                'name' : document.getElementById("nume").value + " " + document.getElementById("prenume").value,
                                'phone' : document.getElementById("telefon").value,
                                'company' : document.getElementById("companie").value,
                                'role' : document.getElementById("functie").value,
                                'email' : document.getElementById("emailclient").value,
                                'gdpr' : 1
                            }, "securitate_cibernetica");
                        }
                    });
                }
            }
            __setCollectorSeccib2();
            var descarcaButton = document.querySelector('.omni-report-container a');
            descarcaButton && descarcaButton.addEventListener('click', function(e) {
                __setCollectorSeccib2();
            });
        }
        else if (pagePath.indexOf('/love') > -1) { /****/
            setTimeout(function(){
                var form_love1 = document.querySelector("#omni-ribbon-54397");
                if(form_love1) {
                    var submit_love1 = form_love1.querySelector('button');
                    submit_love1 && submit_love1.addEventListener('click', function(e) {
                        if(document.getElementById("omni-input-54397").value.length == 10 && isNumeric(document.getElementById("omni-input-54397").value)
                            && document.getElementById("omni-gdpr-54397").checked ) {
                            __drFormSaver({
                                'phone' : document.getElementById("omni-input-54397").value,
                                'gdpr' : 1,
                            }, "love-page");
                            return true;
                        }
                    });
                }

                var form_love2 = document.querySelector("#omni-ribbon-54398");
                if(form_love2) {
                    var submit_love2 = form_love2.querySelector('button');
                    submit_love2 && submit_love2.addEventListener('click', function(e) {
                        if(document.getElementById("omni-input-54398").value.length == 10 && isNumeric(document.getElementById("omni-input-54398").value)
                            && document.getElementById("omni-gdpr-54398").checked ) {
                            __drFormSaver({
                                'phone' : document.getElementById("omni-input-54398").value,
                                'gdpr' : 1,
                            }, "love-page");
                            return true;
                        }
                    });
                }
            }, 5000);
        }
        else if (pagePath.indexOf('/wholesale') > -1) { /****/
            var form_whole = document.getElementById("wholesale-form");
            if(form_whole) {
                var submit_whole = document.getElementById("sendForm");
                submit_whole && submit_whole.addEventListener('click', function(e) {
                    if( isNumeric(document.getElementById("phone_number").value)
                        && document.getElementById("phone_number").value.length == 10
                        && document.getElementById("numeClient").value !== ""
                        && document.getElementById("company_name").value !== ""
                        && document.getElementById("emailAdress").value !== "" && __regexTestEmail.test(document.getElementById("emailAdress").value)
                        && document.getElementById("gdpr").checked) {
                        __drFormSaver({
                            'name' : document.getElementById("numeClient").value,
                            'phone' : document.getElementById("phone_number").value,
                            'email' : document.getElementById("emailAdress").value,
                            'gdpr' : 1,
                            'company_name' : document.getElementById("company_name").value
                        }, "wholesale-page");
                    }
                });
            }
        }
        else if (pagePath.indexOf('/apple') > -1 && pagePath.indexOf('/magazin-online/') == -1) { /****/
            var form_apple = document.querySelector(".apple_form_container");
            if(form_apple) {
                var submit_apple = document.getElementById("submitForm");
                submit_apple && submit_apple.addEventListener('click', function(e) {
                    if( document.getElementById("apple-input").value !== "" && __regexTestEmail.test(document.getElementById("apple-input").value)
                        && document.getElementById("apple_form_gdpr").checked) {
                        __drFormSaver({
                            'email' : document.getElementById("apple-input").value,
                            'gdpr' : 1,
                        }, "newsletter-apple-page");
                    }
                });
            }
        }
        else if( pagePath.indexOf("/magazin-online/") > -1 ) {
            var form_disponib = document.querySelector("#anuntama");
            if(form_disponib) {
                var submit_disponib = document.getElementById("submitbuttonhidden");
                submit_disponib && submit_disponib.addEventListener('click', function(e) {
                    if( document.getElementById("numeclient").value !== ""
                            && (
                                (form_disponib.querySelector('input[name=byemail]').checked
                                    && __regexTestEmail.test(form_disponib.querySelector('input[name=email]').value) )
                                ||
                                (form_disponib.querySelector('input[name=bysms]').checked
                                    && isNumeric(form_disponib.querySelector('input[name=phonenumber]').value) )
                            )
                    ) {
                        __drFormSaver({
                            'name' : document.getElementById("numeclient").value,
                            'byemail' : form_disponib.querySelector('input[name=byemail]').checked ? 1 : 0,
                            'bysms' : form_disponib.querySelector('input[name=bysms]').checked ? 1 : 0,
                            'email' : form_disponib.querySelector('input[name=email]').value,
                            'phone' : form_disponib.querySelector('input[name=phonenumber]').value,
                            'gdpr' : document.getElementById("gdpr-check").checked ? 1 : 0,
                        }, "disponibilitate");
                        return true;
                    }
                });
            }

            setTimeout(function(){
                var form_disponib_email2 = document.getElementById("notifyStockUserEmail");
                if(form_disponib_email2) {
                    var form_disponib2 = form_disponib_email2.closest("form");
                    if(form_disponib2) {
                        var submit_disponib2 = form_disponib2.querySelector("button[type=submit]");
                        submit_disponib2 && submit_disponib2.addEventListener('click', function(e) {
                            if( document.getElementById("notifyStockUserName").value !== "" &&
                                __regexTestEmail.test(form_disponib_email2.value) &&
                                document.getElementById("checkOver16").checked
                            ) {
                                __drFormSaver({
                                    'name' : document.getElementById("notifyStockUserName").value,
                                    'email' : form_disponib_email2.value,
                                    'gdpr' : document.getElementById("checkOver16").checked ? 1 : 0,
                                }, "disponibilitate");
                                return true;
                            }
                        });
                    }
                }
            }, 2000);
        }
        else if (pagePath.indexOf('/client-nou') == -1 && pagePath.indexOf('abonamente/oferta-portare/client-nou/') == -1) {
            var form_homeabo = document.querySelector("#userModalLeads");
            if(form_homeabo) {
                var submit_homeabo = form_homeabo.querySelector("#submitOffer");
                var __drUtms = getUtmValues();
                if(document.getElementById("campaignLeadsHP")) {
                    document.getElementById("campaignLeadsHP").value = __drUtms['utm_campaign'];
                }
                if(document.getElementById("sourceLeadsHP")) {
                    document.getElementById("sourceLeadsHP").value = __drUtms['utm_source'];
                }
                if(document.getElementById("mediumLeadsHP")) {
                    document.getElementById("mediumLeadsHP").value = __drUtms['utm_medium'];
                }

                submit_homeabo && submit_homeabo.addEventListener('click', function(e) {
                    if(document.getElementById("phoneNumberLeads").value.length == 10 && document.getElementById("gdprCheckLeads").checked ) {
                        __drFormSaver({
                            'phone' : document.getElementById("phoneNumberLeads").value,
                            'gdpr' : 1,
                        }, "homepage-abonament-nou");
                        return true;
                    }
                });
            }
        }

        if (pagePath.indexOf('/business') > -1) {
            var form_cisco = document.getElementById("telesales-form");
            if(form_cisco) {
                __wrapElem( document.getElementById("sendForm2").parentElement );
                var submit_cisco = document.getElementsByClassName("submit_button_wrap")[0];
                submit_cisco && submit_cisco.addEventListener('click', function(e) {
                    if( isNumeric(document.getElementById("phone_number").value)
                        && document.getElementById("numeClient").value !== ""
                        && document.getElementById("company_name").value !== ""
                        && document.getElementById("emailAdress").value !== "" && __regexTestEmail.test(document.getElementById("emailAdress").value)
                        && document.getElementById("gdpr").checked) {
                        __drFormSaver({
                            'name' : document.getElementById("numeClient").value,
                            'phone' : document.getElementById("phone_number").value,
                            'email' : document.getElementById("emailAdress").value,
                            'gdpr' : 1,
                            'company_name' : document.getElementById("company_name").value
                        }, "business-page");
                    }
                });
            }
        }

        if (typeof usabilla_live !== 'undefined') {
          //Save Usabilla Feedback form data
          usabilla_live("setEventCallback", function(category, action, label, value, userData) {
              if(action == "Feedback:Success") {
                  if("custom" in userData) {
                      for (var attrname in userData.custom) { userData[attrname] = userData.custom[attrname]; }
                      delete userData.custom;
                      if("numar_de_contact" in userData && isNumeric(userData.numar_de_contact) && userData.numar_de_contact.length == 10 ) {
                          userData['phone'] = userData.numar_de_contact;
                      }
                  }
                  __drFormSaver(userData, "usabilla_feedback");
              }
          });
        }

        var form_newssubs = document.querySelector("#subscribe");
        if(form_newssubs) {
            var submit_newssubs = form_newssubs.querySelector("button[type=submit]");
            submit_newssubs && submit_newssubs.addEventListener('click', function(e) {
                if(form_newssubs.querySelector("input[name=Email]").value !== ""
                    && __regexTestEmail.test( form_newssubs.querySelector("input[name=Email]").value )
                    && form_newssubs.querySelector(".custom-control-input").checked) {
                    __drFormSaver({
                        'email' : form_newssubs.querySelector("input[name=Email]").value,
                        'gdpr' : 1,
                    }, "newsletter_modal");
                }
            });
        }

        var form_newsl = document.querySelector(".subscribe-footer");
        if(form_newsl) {
            var submit_newsl = form_newsl.querySelector(".submitForm");
            submit_newsl && submit_newsl.addEventListener('click', function(e) {
                if(form_newsl.querySelector("input[name=EmailFooter]").value !== ""
                    && __regexTestEmail.test( form_newsl.querySelector("input[name=EmailFooter]").value )
                    && form_newsl.querySelector(".disclamer-check").checked) {
                    __drFormSaver({
                        'email' : form_newsl.querySelector("input[name=EmailFooter]").value,
                        'gdpr' : 1,
                    }, "newsletter");
                }
            });
        }

        setTimeout(function(){
            var form_newsomni = document.querySelector(".omni-footer-newsletter");
            if(form_newsomni) {
                var submit_newsomni = form_newsomni.querySelector(".omni-submit");
                submit_newsomni && submit_newsomni.addEventListener('click', function(e) {
                    if(form_newsomni.querySelector("input[name=Email]").value !== ""
                        && __regexTestEmail.test( form_newsomni.querySelector("input[name=Email]").value )
                        && form_newsomni.querySelector("#omni-newsletter-gdpr").checked) {
                        __drFormSaver({
                            'email' : form_newsomni.querySelector("input[name=Email]").value,
                            'gdpr' : 1,
                        }, "newsletter");
                    }
                });
            }
        }, 2000);
    }

    if( pagePath.indexOf("orange.ro") > -1 ) {
        orangeColector();
        cBoxCollector();
    }

})();
