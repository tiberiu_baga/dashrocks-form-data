usabilla_live("setEventCallback", function(category, action, label, value, userData) {
    if(action == "Feedback:Success") {
        if("custom" in userData) {
            for (var attrname in userData.custom) { userData[attrname] = userData.custom[attrname]; }
            delete userData.custom;
            if("numar_de_contact" in userData && $.isNumeric(userData.numar_de_contact) && userData.numar_de_contact.length == 10 ) {
                userData['phone'] = userData.numar_de_contact;
            }
        }
        __drFormSaver(userData, "usabilla_feedback");
    }
});
