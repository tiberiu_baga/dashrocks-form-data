<script>
(function(){
  if (!window.jQuery) { console.log("no jQ loaded"); return; }
  // define modals ids
  var default_modals = [
    // {
    //   'name':'modalYoxoport',
    //   'show':1000, // numarul de afisari pana la off
    //   'onclose':'off' // display on sau off afer close click
    // },
    // {
    //   'name':'modalZumzetInitial',
    //   'show':1000,
    //   'onclose':'off' // display on sau off afer close click
    // },
    // {
    //   'name':'modalPinata',
    //   'show':1000,
    //   'onclose':'off' // display on sau off afer close click
    // },
    // {
    //   'name':'modalhotsummer',
    //   'show':1000,
    //   'onclose':'off' // display on sau off afer close click
    // }
    {
      'name':'a20aniModal',
      'show':1,
      'onclose':'off' // display on sau off afer close click
    }
  ];
  
  //var oro_modals_cookie = {'numemodal':{'show':1,'close':'on'},'nume3':{'show':1,'close':'on'}};
  var oro_modals_cookie = ({{oro_modals_cookie}}) ? JSON.parse({{oro_modals_cookie}}) : {};
  // rules for display modal
  // check for modal presence in order to redefine available modals
  // need to define modal cookie related
  // name of cookie is oro_modals
  // cookie syntax json encoded: {'modalName':numberOfDisplayed,'modalName':numberOfDisplayed}: Ex: {'numemodal':1,'modaldoi':3}
  // recalculate saturation
  var available_modals = [];
  for(var i=0; i<default_modals.length;i++){
     var modal_name = default_modals[i].name;
     var modal_show = default_modals[i].show;
     if(document.getElementById(modal_name)!==null){
       // check saturation in cookie
       if(typeof(oro_modals_cookie)!=='undefined'){
         // if modal is displayed already 
         if(typeof(oro_modals_cookie[modal_name])!=='undefined'){
           // but not enough displayed
           if(oro_modals_cookie[modal_name].show <= default_modals[i].show){
             // check if is off on close 
             if(default_modals[i].onclose === 'on' || oro_modals_cookie[modal_name].onclose === 'on'){
               available_modals.push(default_modals[i].name);
             };
           };
         } else {
           available_modals.push(default_modals[i].name);
         }
       } else {
          available_modals.push(default_modals[i].name); 
       }
     }
  };
  //  display extra condition
  // defaults for yoxo page visitors
  var index = -1;
  if ({{Page URL}}.indexOf('/yoxo/portare/')!==-1){
     index = available_modals.indexOf('modalYoxoport');
     if (index > -1) {
        available_modals.splice(index, 1);
     }
  };
  // end display extra condition
  // calculate which modal is displayed max 10 modals in same time and display modal if exists
  if(available_modals.length>0){
    var modal_displayed = available_modals[Math.floor(Math.random() * 10) % available_modals.length];
    // display modal if exists
    $("#"+modal_displayed).modal('show');
    // calculate new cookie value and set new cookie 
    oro_modals_cookie[modal_displayed] = (oro_modals_cookie[modal_displayed]) ? {'show':oro_modals_cookie[modal_displayed].show + 1,'onclose':'on'}:{'show':1,'onclose':'on'};  
    {{setCookie}}('oro_modals',JSON.stringify(oro_modals_cookie),7);
    // send push to dataLayer
    dataLayer.push({
      'event':'gaEventTriggered',
      'eventCategory':'Orange Modals',
      'eventAction':'Displayed Modal: ' + modal_displayed,
      'eventLabel': 'Number of displayed: ' + oro_modals_cookie[modal_displayed].show
    });
    // send event on close
     // listener for hide
    $("#"+modal_displayed).on('hidden.bs.modal', function (e) {
      dataLayer.push({
            'event':'gaEventTriggered',
            'eventCategory':'Orange Modals',
            'eventAction':'Close Modal: ' + modal_displayed,
            'eventLabel':'Number of displayed: ' + oro_modals_cookie[modal_displayed].show
       });
       // calculate new cookie value and set new cookie 
        oro_modals_cookie[modal_displayed] = {'show':oro_modals_cookie[modal_displayed].show ,'onclose':'off'};
        {{setCookie}}('oro_modals',JSON.stringify(oro_modals_cookie),7);
    });
   };
  })();  
</script>
