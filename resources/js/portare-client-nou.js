(function () {

var phoneNumber;
var gdprErrEl = document.createElement("p");
gdprErrEl.innerHTML = "";
gdprErrEl.classList.add("d-none", "text-danger", "mb-0");
gdprErrEl.setAttribute("id", "gdprError");
var gdpr = document.getElementById("gdprCheckLeads");
insertAfter(gdprErrEl, gdpr.parentNode);

var inputPhone = document.getElementById('phoneNumberLeads');
var submitButton = document.getElementById('submitOffer');
var errorPhone = document.getElementsByClassName('phoneError')[0];
submitButton.removeAttribute('disabled');

// Phone number validation script
inputPhone.addEventListener("keyup", function () {
    var expOne  = '40';
    var expTwo = '004';
    // check if user type non number
    this.value = this.value.replace(/\D/, "");
    // check if number starts with +40 || 0040
    var enteredText = this.value;
    if(enteredText.length <= 3) {
        setTimeout(function(){
            if(enteredText === expOne || enteredText === expTwo) {
                inputPhone.value = 0;
            }
        }, 100);
    }
});

// More button behaviour
var more = document.querySelectorAll('.showMore');
more.forEach(function (item){
    item.addEventListener('click', function (){
        item.nextElementSibling.classList.remove('d-none')
        item.style.display = 'none';
        dataLayer.push({
            'event':'gaEventTriggered',
            'eventCategory':'Leads Online Voce-QR Page',
            'eventAction':'GDPR',
            'eventLabel':'mai mult'
        });
    })
})

// validate GDPR checkbox
function checkPhoneNumber() {
    var phVal = inputPhone.value;
    var startVal = phVal.substring(0,2);

    if(inputPhone.value.length < 10) {
        errorPhone.classList.remove("d-none");
        errorPhone.innerHTML = 'Numărul de telefon trebuie să conțina minim 10 caractere';
        errorPhone.style.display = 'block';
        return false;
    } else if( startVal !== '07' ){
        errorPhone.classList.remove("d-none");
        errorPhone.innerHTML = 'Numărul de telefon trebuie să inceapa cu "07" ';
        errorPhone.style.display = 'block';
    } else {
        errorPhone.style.display = 'none';
        phoneNumber = inputPhone.value;
        return true;
    }

}

function validateGDPR() {
    if(gdpr.checked) {
        document.getElementById('gdprError').style.display = 'none';
        return true;
    } else {
        document.getElementById('gdprError').classList.remove("d-none");
        document.getElementById('gdprError').innerHTML = "Pentru a continua trebuie să putem să te contactăm" ;
        document.getElementById('gdprError').style.display = 'block';
        return false;
    }

}

submitButton.addEventListener('click', function(e) {
    e.preventDefault();
    var number =  checkPhoneNumber();
    var checkGDRP = validateGDPR();
    if (number && checkGDRP ) {
        phoneNumber = inputPhone.value;
        if ( sendTocBox() ) {
            dataLayer.push({
                'event':'gaEventTriggered',
                'eventCategory':'Leads Online Voce-QR Page',
                'eventAction':'Lead Success',
                'eventLabel':'Click'
            });
            document.getElementById('defaultBody').style.display = 'none';
            document.getElementById('successBody').style.display = 'block';
        } else {
            dataLayer.push({
                'event':'gaEventTriggered',
                'eventCategory':'Leads Online Voce-QR Page',
                'eventAction':'Lead Failed',
                'eventLabel':'Click'
            });
            document.getElementById('defaultBody').style.display = 'none';
            document.getElementById('errorBody').style.display = 'block';
        }
    }
});

var checkAgainButton = document.getElementById('checkAgain');
checkAgainButton.addEventListener('click', function(e) {
    e.preventDefault();
    document.getElementById('defaultBody').style.display = 'block';
    document.getElementById('successBody').style.display = 'none';
    document.getElementById('errorBody').style.display = 'none';
});



function insertAfter(newNode, existingNode) {
    existingNode.parentNode.insertBefore(newNode, existingNode.nextSibling);
}

var __queryConvert = function(queryString) {
    var queryArr = queryString.split('&'), queryParams = {'utm_campaign': "", 'utm_medium': "", 'utm_source': ""};
    for (var q = 0, qArrLength = queryArr.length; q < qArrLength; q++) {
        var qArr = queryArr[q].split('=');
        queryParams[qArr[0]] = qArr[1];
    }
    return queryParams;
}

function sendTocBox() {
    if (!window.jQuery) {
        console.log('No jQuery Loaded');
        return;
    }
    return __drFormSaver({
        'phone' : phoneNumber,
        'gdpr' : 1
    }, "abonamente-oferta-portare-client-nou");
}
})();

