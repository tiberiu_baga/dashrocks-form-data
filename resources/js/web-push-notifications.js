<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js"></script>
<script>
var nrpg = 3;
if ({{cookie - __sreff}}) {
     nrpg = Number({{cookie - __sreff}}.split('.')[2]);
};
if (nrpg > 1 || {{Page URL}}.indexOf('aplicatii/extensie-chrome') != -1) {
    var myOSTags = {};
    if (document.location.pathname.indexOf('servicii-fixe') != -1)
    {
      myOSTags.argoVisit = Math.floor(Date.now() / 1000);
    }
    if (typeof {{ecommerceDataLayer-Detail}} == "object" && typeof {{ecommerceDataLayer-Detail}}.products == "object" && typeof {{ecommerceDataLayer-Detail}}.products[0].brand != "undefined")
    {
      myOSTags[{{ecommerceDataLayer-Detail}}.products[0].brand] = "yes";
    }
    if ({{GeoLatitude}} != "na" && {{GeoLongitude}} != "na")
    {
      myOSTags.latitude = {{GeoLatitude}};
      myOSTags.longitude = {{GeoLongitude}};
    }
};

var fcmUserToken = '';
var fcmNotifPermissionRequested = localStorage.getItem('fcmNotifPermissionRequested');

var firebaseConfig = {
  apiKey: "AIzaSyB_glG1v9ctpnbwUgy1BV_ASUc03MEyPQQ",
  authDomain: "push-notifications-c0fc0.firebaseapp.com",
  databaseURL: "https://push-notifications-c0fc0.firebaseio.com",
  projectId: "push-notifications-c0fc0",
  storageBucket: "push-notifications-c0fc0.appspot.com",
  messagingSenderId: "819391863621",
  appId: "1:819391863621:web:76a324a2d9e26dbc5ebc2c",
  measurementId: "G-XMXPXHFSGK"
};

if(window.location.host == "www.orange.ro") {
  firebase.initializeApp(firebaseConfig);
  var messaging = firebase.messaging();
  messaging.usePublicVapidKey('BHsJV2b0gGXNFbm_dhdXu8P-vDqi6615jXuzzlYOit66oIEiTIY_CFop68DBi8vTMoL3DQNAJBwIQRqFMdq4YHY');

  messaging.onMessage(function(payload) {
        if ("notification" in payload) {
            payload.notification.requireInteraction = true;
            var myNotification = new Notification(payload.notification.title, payload.notification);
            myNotification.onclick = function(event) {
                event.preventDefault();
                window.open(payload.notification.click_action, '_blank');
            }
        } else if ("data" in payload) {
            var myNotification = new Notification(payload.data.title, payload.data);
            myNotification.onclick = function(event) {
                event.preventDefault();
                window.open(payload.data.click_action, '_blank');
            }
        }
    });

    if ({{Page Path}}.indexOf("/verifica-acoperirea") > -1) {
        var eltostep3 = document.getElementById('to-step-3');
        eltostep3 && eltostep3.addEventListener('click', function(e) {
            return trackFcmAction(fcmUserToken, "event", 'verifica-acoperirea-date-contact');
        });
        var elsubmitButton = document.getElementById('submitButton');
        elsubmitButton && elsubmitButton.addEventListener('click', function(e) {
            if(document.getElementById('phoneNumber').value.length == 10 && document.getElementById('gdprCheck').checked ) {
              return trackFcmAction(fcmUserToken, "event", 'verifica-acoperirea-date-contact');
            }
        });

        var submitreqcontact3 = document.getElementById('requestContact')
        submitreqcontact3 && submitreqcontact3.addEventListener('click', function(e) {
            if(document.getElementById('phoneNumber').value.length == 10 && document.getElementById('gdprCheck').checked ) {
              return trackFcmAction(fcmUserToken, "event", 'verifica-acoperirea-date-contact');
            }
        });

        var submitreqcontact4 = document.getElementById('submitRequest')
        submitreqcontact4 && submitreqcontact4.addEventListener('click', function(e) {
            if(document.getElementById('phoneNumber2').value.length == 10 && document.getElementById('gdprCheck2').checked ) {
              return trackFcmAction(fcmUserToken, "event", 'verifica-acoperirea-servicii');
            }
        });
    } else if ({{Page Path}}.indexOf("/love") > -1) {
      var __isNumeric = function (n) { return !isNaN(parseFloat(n)) && isFinite(n); }
      setTimeout(function() {
        var form_love1 = document.querySelector("#omni-ribbon-54397");
        if(form_love1) {
            __wrapElem(form_love1.querySelector('button'));
            var submit_love1 = ;
            submit_love1 && submit_love1.addEventListener('click', function(e) {
                if(document.getElementById("omni-input-54397").value.length == 10 && __isNumeric(document.getElementById("omni-input-54397").value)
                    && document.getElementById("omni-gdpr-54397").checked ) {
                    return trackFcmAction(fcmUserToken, "event", 'love-ribbon-colectare-lead');
                }
            });
        }

        var form_love2 = document.querySelector("#omni-ribbon-54398");
        if(form_love2) {
            var submit_love2 = form_love2.querySelector('button');
            submit_love2 && submit_love2.addEventListener('click', function(e) {
                if(document.getElementById("omni-input-54398").value.length == 10 && __isNumeric(document.getElementById("omni-input-54398").value)
                    && document.getElementById("omni-gdpr-54398").checked ) {
                    return trackFcmAction(fcmUserToken, "event", 'love-ribbon-colectare-lead');
                }
            });
        }
      }, 5000);
    }

}

function resetUI() {
  messaging.getToken().then(function(currentToken) {
    if (currentToken) {
      fcmUserToken = currentToken;
      if (fcmNotifPermissionRequested == 1) {
        localStorage.setItem('fcmNotifPermissionRequested', null);
        trackFcmAction(fcmUserToken, "registration", Math.floor(Date.now() / 1000));
      } else {
        trackFcmPageAction(fcmUserToken);
      }
    } else {
      if (nrpg > 1 && {{Page URL}}.indexOf('aplicatii/extensie-chrome') == -1  && {{Page URL}}.indexOf('/galaxy/') == -1) {
        requestPermission();
      }
    }
  }).catch(function(err){
    if( err.code.indexOf('messaging/notifications-blocked') != -1 ) {
      //console.log('Notification were blocked.');
    } else {
      //console.log('An error occurred while retrieving token. ', err);
    }
  });
}

function requestPermission() {
  Notification.requestPermission().then(function(permission){
    if (permission === 'granted') {
      localStorage.setItem('fcmNotifPermissionRequested', 1);
      resetUI();
    }
  });
}

function getFcmToken() {
  return fcmUserToken;
}

function deleteToken() {
  messaging.getToken().then(function(currentToken){
    messaging.deleteToken(currentToken).then(function(){
      resetUI();
    }).catch(function(err){
    });
  }).catch(function(err){
  });
}

function trackFcmPageAction(token) {
  var value = encodeURIComponent(window.location.href.replace(/(^\w+:|^)\/\//, ''));
  return trackFcmAction(token, "page", value);
}

function trackFcmAction(token, event, value) {
  var trackingPixel = new Image(1, 1);
  var pixelSrc = "//dashrocks-solutions.s3.eu-west-3.amazonaws.com/images/1pixel.png" +
    "?token=" + token +
    "&event=" + event +
    "&value=" + value +
    "&ord=" + (new Date()).getTime();
  trackingPixel.src = pixelSrc;
}

if(window.location.host == "www.orange.ro") {
  messaging.onTokenRefresh(function(){
    messaging.getToken().then(function(refreshedToken){
      resetUI();
    }).catch(function(err){
    });
  });

  resetUI();
}
</script>
