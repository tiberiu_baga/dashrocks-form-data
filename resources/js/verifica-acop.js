
if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}


var step1 = document.getElementById('step-1');
let phoneNumber;


// const modalTombola = document.querySelector('.modal-tombola');
// const showModal = document.querySelector('.show-modal');
// const modalBg = document.querySelector('.modal-bg-tombola');
// const closeTombolaModal = document.querySelectorAll('.close-tombola-modal');


function hideStep1() {
    var step1 = document.getElementById('step-1');
    var loader1 = document.querySelector('#step-1 .loader');
    var content1 = document.querySelector('#step-1 .content');
    var fold1 = document.querySelector('#step-1 .not-expended');
    content1.style.display = 'none';
    fold1.style.display = 'block';
    loader1.style.display = 'flex';
    setTimeout(function () {
        loader1.style.display = 'none';
        step1.classList.remove('active');
        step1.classList.add('passed');
    }, 650);
    progressMobile.style.width = '67' + '%';
}

document.addEventListener("DOMContentLoaded", function () {
    var recivedstep = sessionStorage.getItem("selectedStep");

    if (recivedstep === 'second') {
        goToStep2();
        sessionStorage.removeItem("selectedStep");
    } else {
        var activStep = document.querySelector('.dot-1');
        activStep.classList.add('active');
        step1.classList.add('active');
    }
}); // polyFill browser compatibility IE

document.getElementById('openModalInstall').addEventListener('click', function (){
    dataLayer.push({
        'event':'gaEventTriggered',
        'eventCategory':'Orange Home',
        'eventAction': 'Instalare Serviciilor',
        'eventLabel':'Video'}
        );
})

document.getElementById('tvGo').addEventListener('click', function (){
    dataLayer.push({
        'event':'gaEventTriggered',
        'eventCategory':'Orange Home',
        'eventAction': 'Orange Tv Go',
        'eventLabel':'Tv Go'})
    ;
})

document.getElementById('rate').addEventListener('click', function (e){
    e.preventDefault()
    dataLayer.push({
        'event':'gaEventTriggered',
        'eventCategory':'Orange Home',
        'eventAction':'Vezi oferta de produse in rate',
        'eventLabel': -'Pas Final'});
    window.location.href = 'https://www.orange.ro/love/produse-in-rate';

})

const stepTwoBtn = document.getElementById('to-step-2');
const progressMobile = document.getElementById('fill');
const inputPhone = document.getElementById('phoneNumber');
const submitButton = document.getElementById('submitButton');
const errorPhone = document.getElementById('phoneNumberError');


var pachetSelectat;
var noNumberEligible;
var isOfferChecked;


const selectOpt = function(el) {
    el.addEventListener('click', function (e) {
        e.stopPropagation();
        if(el.classList.contains('passed')) {
            el.classList.add('active');
            el.classList.remove('passed');
            el.style.display = 'flex';
            const elInner = el.querySelector('.content');
            const elinnerHidden  = el.querySelector('.not-expended');
            elInner.style.display = 'block';
            elinnerHidden.style.display = 'none';

            document.getElementById('step-2').classList.remove('active');
            document.getElementById('step-2').style.display = 'none';

            document.getElementById('step-3').classList.remove('active');
            document.getElementById('step-3').style.display = 'none';
        }
    })

};


stepTwoBtn.addEventListener("click", function (e){
    e.preventDefault();
    goToStep2();
    dataLayer.push({
        'event':'gaEventTriggered',
        'eventCategory':'Orange Home',
        'eventAction':'Datele de Contact',
        'eventLabel':'Continua fara numar'
    });

});

selectOpt(document.getElementById('step-1'));

window.addEventListener('DOMContentLoaded', function(){
    progressMobile.style.width = '33' + '%';
});

// Phone number validation script

inputPhone.addEventListener("keyup", function () {
    const expOne  = '40';
    const expTwo = '004';
    // check if user type non number
    this.value = this.value.replace(/\D/, "");
    // check if number starts with +40 || 0040
    const enteredText = this.value;
    if(enteredText.length <= 3) {
        setTimeout(function(){
            if(enteredText === expOne || enteredText === expTwo) {
                inputPhone.value = 0;
            }
        }, 100);
    }
});



//validate GDPR checkbox
const gdpr = document.getElementById("gdprCheck");

function checkPhoneNumber() {
    const phVal = inputPhone.value;
    const startVal = phVal.substring(0,2);

    if(inputPhone.value.length < 10) {
        errorPhone.innerHTML = 'Numarul de telefon trebuie sa contina minim 10 caractere';
        errorPhone.style.display = 'block';
        return false;
    } else if( startVal !== '07' ){
        errorPhone.innerHTML = 'Numarul de telefon trebuie sa inceapa cu "07" ';
        errorPhone.style.display = 'block';
    } else {
        errorPhone.style.display = 'none';
        phoneNumber = inputPhone.value;
        return true;
    }

}

function validateGDPR() {
    if(gdpr.checked) {
        document.getElementById('gdprError').style.display = 'none';
        dataLayer.push({
            'event':'gaEventTriggered',
            'eventCategory':'Orange Home',
            'eventAction':'GDPR',
            'eventLabel':'Bifa Oferte Personalizate 1st Step'
        });
        return true;
    } else {
        document.getElementById('gdprError').innerHTML = "Pentru a continua trebuie sÄ putem sa te contactÄm" ;
        document.getElementById('gdprError').style.display = 'block';
        return false;
    }

}

var vantiveID=[];

var selectedInternetType;
var campaingName;
var isTV;

submitButton.addEventListener('click', function(e){
    e.preventDefault();
    const number =  checkPhoneNumber();
    const checkGDRP = validateGDPR();

    if (number && checkGDRP ) {
        phoneNumber = inputPhone.value;
        document.getElementById('telephone').innerText = phoneNumber;
        document.getElementById('address').innerText = "Verificata in functie de numarul de telefon.";
        hideStep1();

        dataLayer.push({
            'event':'gaEventTriggered',
            'eventCategory':'Orange Home',
            'eventAction':'Datele de Contact',
            'eventLabel':'Continua cu numar'
        });

        var xhr = new XMLHttpRequest(),
            method = "POST",
            url = ' https://www.orange.ro/argo/eligibility/check-msisdn-eligibility/' + phoneNumber;

        xhr.open(method, url, true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        xhr.setRequestHeader('Data-type', 'json');
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                let response = JSON.parse(xhr.response);

                var envDev = 'https://www.orange.ro/ux-admin/api/love/getPackages?' ;

                var optionsPachages = document.getElementById('packagesOptions');
                if (response.availableServices !== null) {

                    goToStepThree();

                    var recivedTehnology = response.availableServices.technology;
                    var recivedProvider = response.availableServices.provider;
                    var recivedTvCapacity = response.availableServices.tvcapacity;


                    selectedInternetType = recivedTehnology;
                    if ( recivedTehnology === 'FTTB' || recivedTehnology === 'FTTH' ) {
                        serviceInternet = true;
                        serviceTV = true;
                        serviceMobile = false;
                        internetPower = 'fiber';
                        tvPower = 'cable';
                        var urlFinal;

                        if(recivedTvCapacity !== null) {
                            isTV = true
                        } else {
                            isTV = false;
                        }

                        if (recivedTehnology === 'FTTH') {
                            urlFinal  = envDev + 'serviceInternet=' + serviceInternet + '&' + 'serviceTV=' +  serviceTV + '&' + 'serviceMobile=' + serviceMobile + '&' + 'serviceProvider=argo&' + '&' + 'internetPower=' + internetPower + '&' + 'tvPower=' + tvPower + '&' + 'isRecommanded=' + isRecommanded + '&' + 'filter=false' + '&' + 'isClient=' + isClient + '&' + 'clientType=b2c&serviceOperator=OR' + '&' + 'isVisible=true'

                            if(recivedProvider === 'TKR') {
                                campaingName = 'Fiber'
                            } else {
                                campaingName = "VHBB"
                            }
                        } else {
                            if(recivedProvider === 'TKR') {
                                campaingName = 'Fiber'
                            } else {
                                campaingName = "VHBB"
                            }
                            urlFinal  = envDev + 'serviceInternet=' + serviceInternet + '&' + 'serviceTV=' +  serviceTV + '&' + 'serviceMobile=' + serviceMobile + '&' + 'serviceProvider=argo&' + '&' + 'internetPower=' + internetPower + '&' + 'tvPower=' + tvPower + '&' + 'isRecommanded=' + isRecommanded + '&' + 'filter=false' + '&' + 'isClient=' + isClient + '&' + 'clientType=b2c&serviceOperator=OR' + '&' + 'isVisible=false'
                        }
                        xhr.open(method, urlFinal, true);
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState === XMLHttpRequest.DONE) {
                                var status = xhr.status;

                                if (status === 0 || status >= 200 && status < 400) {
                                    var responsePackage = JSON.parse(xhr.response);
                                    var packages = responsePackage.packages;
                                    var packagesArray = Object.keys(packages);
                                    var options = [];
                                    for (let el in packages) {
                                        options.push({name: packages[el].title, code: packages[el].idVantive})
                                    }
                                    options.forEach(function(item){
                                        var items = document.createElement('li');
                                        items.dataset.id = item.code;

                                        let title = item.name;

                                        items.innerHTML =
                                            `<span onclick="selectPackage(this)">${title}</span>`
                                        ;
                                        optionsPachages.appendChild(items);

                                    })

                                    var selectedPachage = sessionStorage.getItem('selectedPachage');
                                    if(sessionStorage.getItem('selectedPachage') === null) {
                                        document.getElementById('noMatchPackage').style.display = 'none';
                                        var elements = document.querySelectorAll('#packagesOptions li');
                                        for (i = 0; i < elements.length; i++) {
                                            const defaultValue = elements[0];
                                            console.log(defaultValue);
                                            var pachage = defaultValue.dataset.id;
                                            const packagename = defaultValue.querySelectorAll('span')[0].innerHTML;

                                            pachetSelectat = packagename;
                                            document.querySelector('.packageContent .pachageBenefits').innerHTML = '';

                                            document.querySelector('#packages .custom-input').value = packagename;

                                            setTimeout(function (){
                                                var staticUrl = 'https://www.orange.ro/ux-admin/api/love/getPackage?packageID=';

                                                var xhr = new XMLHttpRequest(),
                                                    method = "POST",
                                                    urlPackage = staticUrl + pachage;

                                                xhr.open(method, urlPackage, true);
                                                xhr.onreadystatechange = function () {
                                                    if (xhr.readyState === XMLHttpRequest.DONE) {
                                                        var status = xhr.status;
                                                        if (status === 0 || status >= 200 && status < 400) {
                                                            let responsePackage = JSON.parse(xhr.response);
                                                            var packageWrapper = document.querySelector('.packageContent');
                                                            var packageTitle = document.querySelector('.packageContent .packageContent-title');
                                                            var packageBenefits = document.querySelector('.packageContent .pachageBenefits');
                                                            packageWrapper.style.display = 'block';
                                                            packageTitle.innerHTML =  responsePackage.package.title;

                                                            if (responsePackage.package.features) {
                                                                const featuresList = responsePackage.package.features;
                                                                const features =  responsePackage.features;
                                                                var options = [];
                                                                for (let el in features) {
                                                                    options.push({
                                                                        description: features[el].description,
                                                                        icon: features[el].iconBlack,
                                                                        idDB: features[el].idDB,
                                                                        title: features[el].title,
                                                                    })
                                                                }

                                                                options.forEach(function(items){
                                                                    const itemId = items.idDB;
                                                                    if(featuresList.includes(itemId)){
                                                                        const icon = items.icon;
                                                                        const description = items.description;
                                                                        const title = items.title;

                                                                        const iconpath = 'https://www.orange.ro/images/' + icon

                                                                        var tvBox = document.createElement('div');
                                                                        tvBox.classList.add('media','align-items-center' , 'pb-4', 'd-flex');
                                                                        tvBox.innerHTML =    `
                            <div>
                                <img src="${iconpath}" alt="" class="pachageImage">
                            </div>
                            <div class="media-body servicePack">
                             <h6 class="mb-0 f-16">${items.title}</h6>
                             <small class="c-gray-4 f12">${description}</small>
                            </div> 
                             `
                                                                        packageBenefits.appendChild(tvBox);
                                                                    }
                                                                })

                                                                if(responsePackage.package.price !== undefined) {
                                                                    // var priceWrapper = document.getElementById('price');
                                                                    // var priceWrapperPrice = document.getElementById('exclusivePrice');
                                                                    // var priceWrapperOnline = document.getElementById('exclusivePriceOnline');
                                                                    //
                                                                    //

                                                                    // standard price//
                                                                    var standardPrice = responsePackage.package.price.regular;
                                                                    var standardPricePromo = responsePackage.package.price.promo;
                                                                    var priceCurrency = responsePackage.package.price.currency;
                                                                    var exclusivePrice =  responsePackage.package.price.exclusiveOnline;
                                                                    var exclusivePriceOnline= responsePackage.package.price.exclusiveOnlinePromo;
                                                                    var priceDescription = responsePackage.package.price.description;
                                                                    var priceDescriptionExclusive = responsePackage.package.price.descriptionExclusive;



                                                                    // first price //
                                                                    var firstPricePromo = responsePackage.package.firstPrice.promo;
                                                                    var firstPriceRegular = responsePackage.package.firstPrice.regular;
                                                                    var firstPriceRegularExclusiveOnlinePromo = responsePackage.package.firstPrice.exclusiveOnlinePromo;
                                                                    var firstPriceRegularExclusiveOnline = responsePackage.package.firstPrice.exclusiveOnline;

                                                                    var firstPriceCurency = responsePackage.package.firstPrice.currency;
                                                                    var firstPriceDescription = responsePackage.package.firstPrice.description;
                                                                    var firstPriceDescriptionExclusive = responsePackage.package.firstPrice.descriptionExclusive;


                                                                    // second price //
                                                                    var secondPricePromo = responsePackage.package.secondPrice.promo;
                                                                    var secondPriceRegular = responsePackage.package.secondPrice.regular;
                                                                    var secondPriceRegularExclusiveOnlinePromo = responsePackage.package.secondPrice.exclusiveOnlinePromo;
                                                                    var secondPriceRegularExclusiveOnline = responsePackage.package.secondPrice.exclusiveOnline;


                                                                    var secondPriceCurency = responsePackage.package.secondPrice.currency;
                                                                    var secondPriceDescription = responsePackage.package.secondPrice.description;
                                                                    var secondPriceDescriptionExclusive = responsePackage.package.secondPrice.descriptionExclusive;

                                                                    var onePriceRegulaPricesPromo = document.querySelector('.price .regularPrices-promo');
                                                                    var onePriceRegulaPricesRegular = document.querySelector('.price .regularPrices-regular');

                                                                    var onePriceRegularExclusivePromo = document.querySelector('.price .exclusiveOnlinePromo');
                                                                    var onePriceRegularExclusiveRegular = document.querySelector('.price .exclusiveOnline');


                                                                    var splitPricePricefirstPromo = document.querySelector('.price .splitPricefirst-promo');
                                                                    var splitPricePricefirstRegular = document.querySelector('.price .splitPricefirst-regular');


                                                                    var splitPriceFirstExclusivePromo = document.querySelector('.price .firstPriceExclusive-promo');
                                                                    var splitPriceFirstExclusiveRegular = document.querySelector('.price .firstPriceExclusive-regular');

                                                                    var splitPriceSecondPromo = document.querySelector('.price .secondPrice-promo');
                                                                    var splitPriceSecondRegular = document.querySelector('.price .secondPrice-regular');

                                                                    var splitPriceSecondExclusivePromo = document.querySelector('.price .secondPriceExclusive-promo');
                                                                    var splitPriceSecondExclusiveRegular = document.querySelector('.price .secondPriceExclusive-regular');


                                                                    // One Price Promo //
                                                                    // One Price Promo //
                                                                    if(standardPricePromo !== null){
                                                                        onePriceRegulaPricesPromo.innerHTML = `
                             <span class="value"> ${standardPricePromo}${priceCurrency}</span> <span class="descr">(${priceDescription})</span>
                        `;
                                                                    } else {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                    }


                                                                    if(standardPrice !== null) {
                                                                        onePriceRegulaPricesRegular.innerHTML = `
                             <span class="value"> ${standardPrice}${priceCurrency}</span> <span class="descr">(${priceDescription})</span>
                        `;
                                                                    } else {
                                                                        onePriceRegulaPricesRegular.style.display = 'none'
                                                                    }

                                                                    // One Price Regular //


                                                                    // One Price Exclusive Promo //

                                                                    if(exclusivePriceOnline !== null) {
                                                                        onePriceRegularExclusivePromo.innerHTML = `
                             <span class="value"> ${exclusivePriceOnline}${priceCurrency}</span> <span <span class="descr">(${priceDescriptionExclusive})</span>
                        `;
                                                                    } else {
                                                                        onePriceRegularExclusivePromo.style.display = 'none'
                                                                    }



                                                                    // One Price Exclusive Regular //

                                                                    if(exclusivePrice !== null) {
                                                                        onePriceRegularExclusiveRegular.innerHTML = `
                             <span class="value"> ${exclusivePrice}${priceCurrency}</span> <span class="descr">(${priceDescriptionExclusive})</span>
                        `;
                                                                    } else {
                                                                        onePriceRegularExclusiveRegular.style.display = 'none'
                                                                    }




                                                                    // split Price Exclusive Promo //

                                                                    if (firstPricePromo !== null) {
                                                                        splitPricePricefirstPromo.innerHTML = `
                             <span class="value"> ${firstPricePromo}${firstPriceCurency}</span> <span class="descr">(${firstPriceDescription})</span>
                        `;
                                                                    } else {
                                                                        splitPricePricefirstPromo.style.display = 'none'
                                                                    }


                                                                    // split Price Exclusive Regular //
                                                                    if(firstPriceRegular !== null) {
                                                                        splitPricePricefirstRegular.innerHTML = `
                             <span class="value"> ${firstPriceRegular}${firstPriceCurency}</span> <span <span class="descr">(${firstPriceDescription})</span>
                        `;

                                                                    } else {
                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                    }


                                                                    // split Price First Exclusive Promo //
                                                                    if(firstPriceRegularExclusiveOnlinePromo !== null) {
                                                                        splitPriceFirstExclusivePromo.innerHTML = `
                             <span class="value"> ${firstPriceRegularExclusiveOnlinePromo}${firstPriceCurency}</span> <span  class="descr">(${firstPriceDescriptionExclusive})</span>
                        `;
                                                                    } else {
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';
                                                                    }



                                                                    // split Price First Exclusive regular //

                                                                    if (firstPriceRegularExclusiveOnline !== null) {
                                                                        splitPriceFirstExclusiveRegular.innerHTML = `
                             <span class="value"> ${firstPriceRegularExclusiveOnline}${firstPriceCurency}</span> <span  class="descr">(${firstPriceDescriptionExclusive})</span>
                        `;

                                                                    } else {
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                    }


                                                                    // split Price Secodn Promo //
                                                                    if(secondPricePromo !== null) {
                                                                        splitPriceSecondPromo.innerHTML = `
                             <span class="value"> ${secondPricePromo}${secondPriceCurency}</span> <span  class="descr">(${secondPriceDescription})</span>
                        `;
                                                                    } else {
                                                                        splitPriceSecondPromo.style.display = 'none'
                                                                    }

                                                                    // split Price Second regular //

                                                                    if(secondPriceRegular !== null) {
                                                                        splitPriceSecondRegular.innerHTML = `
                             <span class="value"> ${secondPriceRegular}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                    } else {
                                                                        splitPriceSecondRegular.style.display = 'none'
                                                                    }
                                                                    // split Price Second exclusive promo //
                                                                    if(secondPriceRegularExclusiveOnlinePromo !== null) {
                                                                        splitPriceSecondExclusivePromo.innerHTML = `
                             <span class="value"> ${secondPriceRegularExclusiveOnlinePromo}${secondPriceCurency}</span> <span>(${secondPriceDescriptionExclusive})</span>
                        `;
                                                                    } else {
                                                                        splitPriceSecondExclusivePromo.style.display = 'none'
                                                                    }
                                                                    // split Price Second exclusive regular //
                                                                    if (secondPriceRegularExclusiveOnline !== null ) {
                                                                        splitPriceSecondExclusiveRegular.innerHTML = `
                             <span class="value"> ${secondPriceRegularExclusiveOnline}${secondPriceCurency}</span> <span>(${secondPriceDescriptionExclusive})</span>
                        `;
                                                                    } else {
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none '
                                                                    }


                                                                    if(firstPriceRegularExclusiveOnlinePromo && secondPriceRegularExclusiveOnlinePromo) {
                                                                        splitPriceFirstExclusivePromo.style.display = 'block';
                                                                        splitPriceSecondExclusivePromo.style.display = 'block';
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';

                                                                    } else if (firstPriceRegularExclusiveOnline && secondPriceRegularExclusiveOnline ) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'block';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'block';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }

                                                                    else if (firstPricePromo && secondPricePromo ) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'block';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'block';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }

                                                                    else if (firstPriceRegular && secondPriceRegular ) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'block';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'block';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                    else if (exclusivePriceOnline) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'block';
                                                                        onePriceRegulaPricesRegular.classList.add('discount-price');
                                                                        onePriceRegularExclusivePromo.style.display = 'block';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';
                                                                        document.querySelector('.onePrice').classList.add('multiple')

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                    else if (exclusivePrice) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'block';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                    else if (standardPricePromo) {
                                                                        onePriceRegulaPricesPromo.style.display = 'block';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                    else if (standardPrice) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'block';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                }

                                                                // Vantive services
                                                                var vantiveServices = responsePackage.package.vantiveServices;
                                                                var vantiveServicesInternet;
                                                                var vantiveServicesTv;
                                                                if (vantiveServices.idInternet !== null) {
                                                                    vantiveServicesInternet = vantiveServices.idInternet;
                                                                }
                                                                if (vantiveServices.idTV !== null ) {
                                                                    vantiveServicesTv = vantiveServices.idTV
                                                                }
                                                                vantiveID = [];
                                                                vantiveID.push(vantiveServicesInternet,vantiveServicesTv);
                                                            }
                                                        }
                                                    }
                                                }
                                                xhr.send();
                                            },200)


                                            break;
                                        }
                                    }
                                    else {
                                        setTimeout(function(){
                                            var elements = document.querySelectorAll('#packagesOptions li');

                                            for (i = 0; i < elements.length; i++) {
                                                var items =  elements[i].dataset.id;
                                                if(elements[i].dataset.id === selectedPachage) {
                                                    const packagename = elements[i].querySelectorAll('span')[0].innerHTML;

                                                    pachetSelectat = packagename;
                                                    document.querySelector('#packages .custom-input').value = packagename;
                                                    document.getElementById('noMatchPackage').style.display = 'none'
                                                    document.getElementById('matchPackage').style.display = 'block';
                                                    document.querySelector('.packageContent').style.display = 'block';
                                                    document.getElementById('selectedPackage').innerText = packagename;
                                                    document.querySelector('#default .description').style.display = 'none';
                                                    document.querySelector('#default #packages').style.display = 'none';
                                                    document.querySelector('.packageContent').classList.add('noBorder');

                                                    var package = (elements[i].dataset.id);
                                                    var staticUrl = 'https://www.orange.ro/ux-admin/api/love/getPackage?packageID=';

                                                    var xhr = new XMLHttpRequest(),
                                                        method = "POST",
                                                        urlPackage = staticUrl + package;

                                                    xhr.open(method, urlPackage, true);
                                                    xhr.onreadystatechange = function () {
                                                        if (xhr.readyState === XMLHttpRequest.DONE) {
                                                            var status = xhr.status;
                                                            if (status === 0 || status >= 200 && status < 400) {
                                                                let responsePackage = JSON.parse(xhr.response);
                                                                if (responsePackage.package.features) {
                                                                    // Vantive services
                                                                    var vantiveServices = responsePackage.package.vantiveServices;
                                                                    var vantiveServicesInternet;
                                                                    var vantiveServicesTv;
                                                                    if (vantiveServices.idInternet !== null) {
                                                                        vantiveServicesInternet = vantiveServices.idInternet;
                                                                    }
                                                                    if (vantiveServices.idTV !== null ) {
                                                                        vantiveServicesTv = vantiveServices.idTV
                                                                    }
                                                                    vantiveID = [];
                                                                    vantiveID.push(vantiveServicesInternet,vantiveServicesTv);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    xhr.send();

                                                    break;
                                                }
                                                else {
                                                    document.getElementById('noMatchPackage').style.display = 'block';
                                                    document.getElementById('matchPackage').style.display = 'none';
                                                    document.querySelector('#default .description').style.display = 'none';
                                                    document.querySelector('#default #packages').style.display = 'block';
                                                    document.querySelector('.packageContent').classList.remove('noBorder');


                                                    const defaultValue = elements[0];
                                                    console.log(defaultValue);
                                                    var pachage = defaultValue.dataset.id;
                                                    const packagename = defaultValue.querySelectorAll('span')[0].innerHTML;
                                                    pachetSelectat = packagename;
                                                    document.querySelector('.packageContent .pachageBenefits').innerHTML = '';

                                                    document.querySelector('#packages .custom-input').value = packagename;

                                                    setTimeout(function (){
                                                        var staticUrl = 'https://www.orange.ro/ux-admin/api/love/getPackage?packageID=';

                                                        var xhr = new XMLHttpRequest(),
                                                            method = "POST",
                                                            urlPackage = staticUrl + pachage;

                                                        xhr.open(method, urlPackage, true);
                                                        xhr.onreadystatechange = function () {
                                                            if (xhr.readyState === XMLHttpRequest.DONE) {
                                                                var status = xhr.status;
                                                                if (status === 0 || status >= 200 && status < 400) {
                                                                    let responsePackage = JSON.parse(xhr.response);
                                                                    var packageWrapper = document.querySelector('.packageContent');
                                                                    var packageTitle = document.querySelector('.packageContent .packageContent-title');
                                                                    var packageBenefits = document.querySelector('.packageContent .pachageBenefits');
                                                                    packageWrapper.style.display = 'block';
                                                                    packageTitle.innerHTML =  responsePackage.package.title;

                                                                    if (responsePackage.package.features) {
                                                                        const featuresList = responsePackage.package.features;
                                                                        const features =  responsePackage.features;
                                                                        var options = [];
                                                                        for (let el in features) {
                                                                            options.push({
                                                                                description: features[el].description,
                                                                                icon: features[el].iconBlack,
                                                                                idDB: features[el].idDB,
                                                                                title: features[el].title,
                                                                            })
                                                                        }

                                                                        options.forEach(function(items){
                                                                            const itemId = items.idDB;
                                                                            if(featuresList.includes(itemId)){
                                                                                const icon = items.icon;
                                                                                const description = items.description;
                                                                                const title = items.title;

                                                                                const iconpath = 'https://www.orange.ro/images/' + icon

                                                                                var tvBox = document.createElement('div');
                                                                                tvBox.classList.add('media','align-items-center' , 'pb-4', 'd-flex');
                                                                                tvBox.innerHTML =    `
                            <div>
                                <img src="${iconpath}" alt="" class="pachageImage">
                            </div>
                            <div class="media-body servicePack">
                             <h6 class="mb-0 f-16">${items.title}</h6>
                             <small class="c-gray-4 f12">${description}</small>
                            </div> 
                             `
                                                                                packageBenefits.appendChild(tvBox);
                                                                            }
                                                                        })

                                                                        if(responsePackage.package.price !== undefined) {
                                                                            // var priceWrapper = document.getElementById('price');
                                                                            // var priceWrapperPrice = document.getElementById('exclusivePrice');
                                                                            // var priceWrapperOnline = document.getElementById('exclusivePriceOnline');
                                                                            //
                                                                            //

                                                                            // standard price//
                                                                            var standardPrice = responsePackage.package.price.regular;
                                                                            var standardPricePromo = responsePackage.package.price.promo;
                                                                            var priceCurrency = responsePackage.package.price.currency;
                                                                            var exclusivePrice =  responsePackage.package.price.exclusiveOnline;
                                                                            var exclusivePriceOnline= responsePackage.package.price.exclusiveOnlinePromo;
                                                                            var priceDescription = responsePackage.package.price.description;
                                                                            var priceDescriptionExclusive = responsePackage.package.price.descriptionExclusive;



                                                                            // first price //
                                                                            var firstPricePromo = responsePackage.package.firstPrice.promo;
                                                                            var firstPriceRegular = responsePackage.package.firstPrice.regular;
                                                                            var firstPriceRegularExclusiveOnlinePromo = responsePackage.package.firstPrice.exclusiveOnlinePromo;
                                                                            var firstPriceRegularExclusiveOnline = responsePackage.package.firstPrice.exclusiveOnline;

                                                                            var firstPriceCurency = responsePackage.package.firstPrice.currency;
                                                                            var firstPriceDescription = responsePackage.package.firstPrice.description;
                                                                            var firstPriceDescriptionExclusive = responsePackage.package.firstPrice.descriptionExclusive;


                                                                            // second price //
                                                                            var secondPricePromo = responsePackage.package.secondPrice.promo;
                                                                            var secondPriceRegular = responsePackage.package.secondPrice.regular;
                                                                            var secondPriceRegularExclusiveOnlinePromo = responsePackage.package.secondPrice.exclusiveOnlinePromo;
                                                                            var secondPriceRegularExclusiveOnline = responsePackage.package.secondPrice.exclusiveOnline;


                                                                            var secondPriceCurency = responsePackage.package.secondPrice.currency;
                                                                            var secondPriceDescription = responsePackage.package.secondPrice.description;
                                                                            var secondPriceDescription = responsePackage.package.secondPrice.descriptionExclusive;

                                                                            var onePriceRegulaPricesPromo = document.querySelector('.price .regularPrices-promo');
                                                                            var onePriceRegulaPricesRegular = document.querySelector('.price .regularPrices-regular');

                                                                            var onePriceRegularExclusivePromo = document.querySelector('.price .exclusiveOnlinePromo');
                                                                            var onePriceRegularExclusiveRegular = document.querySelector('.price .exclusiveOnline');


                                                                            var splitPricePricefirstPromo = document.querySelector('.price .splitPricefirst-promo');
                                                                            var splitPricePricefirstRegular = document.querySelector('.price .splitPricefirst-regular');


                                                                            var splitPriceFirstExclusivePromo = document.querySelector('.price .firstPriceExclusive-promo');
                                                                            var splitPriceFirstExclusiveRegular = document.querySelector('.price .firstPriceExclusive-regular');

                                                                            var splitPriceSecondPromo = document.querySelector('.price .secondPrice-promo');
                                                                            var splitPriceSecondRegular = document.querySelector('.price .secondPrice-regular');

                                                                            var splitPriceSecondExclusivePromo = document.querySelector('.price .secondPriceExclusive-promo');
                                                                            var splitPriceSecondExclusiveRegular = document.querySelector('.price .secondPriceExclusive-regular');


                                                                            // One Price Promo //
                                                                            if(standardPricePromo !== null){
                                                                                onePriceRegulaPricesPromo.innerHTML = `
                             <span class="value"> ${standardPricePromo}${priceCurrency}</span> <span>(${priceDescription})</span>
                        `;
                                                                            } else {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                            }


                                                                            if(standardPrice !== null) {
                                                                                onePriceRegulaPricesRegular.innerHTML = `
                             <span class="value"> ${standardPrice}${priceCurrency}</span> <span>(${priceDescription})</span>
                        `;
                                                                            } else {
                                                                                onePriceRegulaPricesRegular.style.display = 'none'
                                                                            }

                                                                            // One Price Regular //


                                                                            // One Price Exclusive Promo //

                                                                            if(exclusivePriceOnline !== null) {
                                                                                onePriceRegularExclusivePromo.innerHTML = `
                             <span class="value"> ${exclusivePriceOnline}${priceCurrency}</span> <span>(${priceDescriptionExclusive})</span>
                        `;
                                                                            } else {
                                                                                onePriceRegularExclusivePromo.style.display = 'none'
                                                                            }



                                                                            // One Price Exclusive Regular //

                                                                            if(exclusivePrice !== null) {
                                                                                onePriceRegularExclusiveRegular.innerHTML = `
                             <span class="value"> ${exclusivePrice}${priceCurrency}</span> <span>(${priceDescriptionExclusive})</span>
                        `;
                                                                            } else {
                                                                                onePriceRegularExclusiveRegular.style.display = 'none'
                                                                            }




                                                                            // split Price Exclusive Promo //

                                                                            if (firstPricePromo !== null) {
                                                                                splitPricePricefirstPromo.innerHTML = `
                             <span class="value"> ${firstPricePromo}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPricePricefirstPromo.style.display = 'none'
                                                                            }


                                                                            // split Price Exclusive Regular //
                                                                            if(firstPriceRegular !== null) {
                                                                                splitPricePricefirstRegular.innerHTML = `
                             <span class="value"> ${firstPriceRegular}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                        `;

                                                                            } else {
                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                            }


                                                                            // split Price First Exclusive Promo //
                                                                            if(firstPriceRegularExclusiveOnlinePromo !== null) {
                                                                                splitPriceFirstExclusivePromo.innerHTML = `
                             <span class="value"> ${firstPriceRegularExclusiveOnlinePromo}${firstPriceCurency}</span> <span>(${firstPriceDescriptionExclusive})</span>
                        `;
                                                                            } else {
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';
                                                                            }



                                                                            // split Price First Exclusive regular //

                                                                            if (firstPriceRegularExclusiveOnline !== null) {
                                                                                splitPriceFirstExclusiveRegular.innerHTML = `
                             <span class="value"> ${firstPriceRegularExclusiveOnline}${firstPriceCurency}</span> <span>(${firstPriceDescriptionExclusive})</span>
                        `;

                                                                            } else {
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                            }


                                                                            // split Price Secodn Promo //
                                                                            if(secondPricePromo !== null) {
                                                                                splitPriceSecondPromo.innerHTML = `
                             <span class="value"> ${secondPricePromo}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPriceSecondPromo.style.display = 'none'
                                                                            }

                                                                            // split Price Second regular //

                                                                            if(secondPriceRegular !== null) {
                                                                                splitPriceSecondRegular.innerHTML = `
                             <span class="value"> ${secondPriceRegular}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPriceSecondRegular.style.display = 'none'
                                                                            }
                                                                            // split Price Second exclusive promo //
                                                                            if(secondPriceRegularExclusiveOnlinePromo !== null) {
                                                                                splitPriceSecondExclusivePromo.innerHTML = `
                             <span class="value"> ${secondPriceRegularExclusiveOnlinePromo}${secondPriceCurency}</span> <span>(${secondPriceDescriptionExclusive})</span>
                        `;
                                                                            } else {
                                                                                splitPriceSecondExclusivePromo.style.display = 'none'
                                                                            }
                                                                            // split Price Second exclusive regular //
                                                                            if (secondPriceRegularExclusiveOnline !== null ) {
                                                                                splitPriceSecondExclusiveRegular.innerHTML = `
                             <span class="value"> ${secondPriceRegularExclusiveOnline}${secondPriceCurency}</span> <span>(${secondPriceDescriptionExclusive})</span>
                        `;
                                                                            } else {
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none '
                                                                            }

                                                                            if(firstPriceRegularExclusiveOnlinePromo && secondPriceRegularExclusiveOnlinePromo) {
                                                                                splitPriceFirstExclusivePromo.style.display = 'block';
                                                                                splitPriceSecondExclusivePromo.style.display = 'block';
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';

                                                                                onePriceRegulaPricesRegular.classList.remove('discount-price');
                                                                                document.querySelector('.onePrice').classList.remove('multiple')

                                                                            } else if (firstPriceRegularExclusiveOnline && secondPriceRegularExclusiveOnline ) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'block';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'block';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';

                                                                                onePriceRegulaPricesRegular.classList.remove('discount-price');
                                                                                document.querySelector('.onePrice').classList.remove('multiple')
                                                                            }

                                                                            else if (firstPricePromo && secondPricePromo ) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'block';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'block';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';

                                                                                onePriceRegulaPricesRegular.classList.remove('discount-price');
                                                                                document.querySelector('.onePrice').classList.remove('multiple')
                                                                            }

                                                                            else if (firstPriceRegular && secondPriceRegular ) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'block';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'block';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';

                                                                                onePriceRegulaPricesRegular.classList.remove('discount-price');
                                                                                document.querySelector('.onePrice').classList.remove('multiple')
                                                                            }
                                                                            else if (exclusivePriceOnline) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'block';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.classList.add('discount-price');
                                                                                onePriceRegularExclusivePromo.style.display = 'block';
                                                                                document.querySelector('.onePrice').classList.add('multiple')

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';
                                                                            }
                                                                            else if (exclusivePrice) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'block';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';

                                                                                onePriceRegulaPricesRegular.classList.remove('discount-price');
                                                                                document.querySelector('.onePrice').classList.remove('multiple')
                                                                            }
                                                                            else if (standardPricePromo) {
                                                                                onePriceRegulaPricesPromo.style.display = 'block';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';

                                                                                onePriceRegulaPricesRegular.classList.remove('discount-price');
                                                                                document.querySelector('.onePrice').classList.remove('multiple')
                                                                            }
                                                                            else if (standardPrice) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'block';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';

                                                                                onePriceRegulaPricesRegular.classList.remove('discount-price');
                                                                                document.querySelector('.onePrice').classList.remove('multiple')
                                                                            }
                                                                        }

                                                                        // Vantive services
                                                                        var vantiveServices = responsePackage.package.vantiveServices;
                                                                        var vantiveServicesInternet;
                                                                        var vantiveServicesTv;
                                                                        if (vantiveServices.idInternet !== null) {
                                                                            vantiveServicesInternet = vantiveServices.idInternet;
                                                                        }
                                                                        if (vantiveServices.idTV !== null ) {
                                                                            vantiveServicesTv = vantiveServices.idTV
                                                                        }
                                                                        vantiveID = [];
                                                                        vantiveID.push(vantiveServicesInternet,vantiveServicesTv);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        xhr.send();
                                                    },200)


                                                    break;
                                                }
                                            }
                                        }, 100);
                                    }
                                }
                            }
                        };
                        xhr.send();

                    } else if ( recivedTehnology === 'VHBB') {
                        serviceInternet = true;
                        serviceTV = true;
                        serviceMobile = false;
                        internetPower = 'fiber';
                        tvPower = 'cable';

                        campaingName = "VHBB"

                        selectedInternetType = recivedTehnology;

                        if(recivedTvCapacity !== null) {
                            isTV = true
                        } else {
                            isTV = false;
                        }


                        var urlFinal = envDev + 'serviceInternet=' + serviceInternet + '&' + 'serviceTV=' +  serviceTV + '&' + 'serviceMobile=' + serviceMobile + '&' + 'serviceProvider=vhbb&' + '&' + 'internetPower=' + internetPower + '&' + 'tvPower=' + tvPower + '&' + 'isRecommanded=' + isRecommanded + '&' + 'filter=false' + '&' + 'isClient=' + isClient + '&' + 'clientType=b2c'

                        xhr.open(method, urlFinal, true);
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState === XMLHttpRequest.DONE) {
                                var status = xhr.status;

                                if (status === 0 || status >= 200 && status < 400) {
                                    var responsePackage = JSON.parse(xhr.response);
                                    var packages = responsePackage.packages;
                                    var packagesArray = Object.keys(packages);
                                    var options = [];
                                    for (let el in packages) {
                                        options.push({name: packages[el].title, code: packages[el].idVantive})
                                    }
                                    options.forEach(function(item){
                                        var items = document.createElement('li');
                                        items.dataset.id = item.code;

                                        let title = item.name;

                                        items.innerHTML =
                                            `<span onclick="selectPackage(this)">${title}</span>`
                                        ;
                                        optionsPachages.appendChild(items);

                                    })

                                    var selectedPachage = sessionStorage.getItem('selectedPachage');
                                    if(sessionStorage.getItem('selectedPachage') === null) {
                                        document.getElementById('noMatchPackage').style.display = 'none';
                                        var elements = document.querySelectorAll('#packagesOptions li');
                                        for (i = 0; i < elements.length; i++) {
                                            const defaultValue = elements[0];
                                            console.log(defaultValue);
                                            var pachage = defaultValue.dataset.id;
                                            const packagename = defaultValue.querySelectorAll('span')[0].innerHTML;
                                            pachetSelectat = packagename;
                                            document.querySelector('.packageContent .pachageBenefits').innerHTML = '';

                                            document.querySelector('#packages .custom-input').value = packagename;

                                            setTimeout(function (){
                                                var staticUrl = 'https://www.orange.ro/ux-admin/api/love/getPackage?packageID=';

                                                var xhr = new XMLHttpRequest(),
                                                    method = "POST",
                                                    urlPackage = staticUrl + pachage;

                                                xhr.open(method, urlPackage, true);
                                                xhr.onreadystatechange = function () {
                                                    if (xhr.readyState === XMLHttpRequest.DONE) {
                                                        var status = xhr.status;
                                                        if (status === 0 || status >= 200 && status < 400) {
                                                            let responsePackage = JSON.parse(xhr.response);
                                                            var packageWrapper = document.querySelector('.packageContent');
                                                            var packageTitle = document.querySelector('.packageContent .packageContent-title');
                                                            var packageBenefits = document.querySelector('.packageContent .pachageBenefits');
                                                            packageWrapper.style.display = 'block';
                                                            packageTitle.innerHTML =  responsePackage.package.title;

                                                            if (responsePackage.package.features) {
                                                                const featuresList = responsePackage.package.features;
                                                                const features =  responsePackage.features;
                                                                var options = [];
                                                                for (let el in features) {
                                                                    options.push({
                                                                        description: features[el].description,
                                                                        icon: features[el].iconBlack,
                                                                        idDB: features[el].idDB,
                                                                        title: features[el].title,
                                                                        hideExclusiveOnline: features[el].hideExclusiveOnline
                                                                    })
                                                                }

                                                                options.forEach(function(items){
                                                                    const itemId = items.idDB;
                                                                    if(featuresList.includes(itemId)){
                                                                        const icon = items.icon;
                                                                        const description = items.description;
                                                                        const title = items.title;

                                                                        const hideExclusive = items.hideExclusiveOnline;

                                                                        const iconpath = 'https://www.orange.ro/images/' + icon

                                                                        var tvBox = document.createElement('div');
                                                                        tvBox.classList.add('media','align-items-center' , 'pb-4', 'd-flex');
                                                                        tvBox.dataset.hide = hideExclusive;
                                                                        tvBox.innerHTML =    `
                                                                    <div>
                                                                        <img src="${iconpath}" alt="" class="pachageImage">
                                                                    </div>
                                                                    <div class="media-body servicePack">
                                                                     <h6 class="mb-0 f-16">${items.title}</h6>
                                                                     <small class="c-gray-4 f12">${description}</small>
                                                                    </div> 
                                                                     `
                                                                        packageBenefits.appendChild(tvBox);
                                                                    }
                                                                })

                                                                if(responsePackage.package.price !== undefined) {

                                                                    var standardPrice = responsePackage.package.price.regular;
                                                                    var standardPricePromo = responsePackage.package.price.promo;
                                                                    var priceCurrency = responsePackage.package.price.currency;
                                                                    var exclusivePrice =  responsePackage.package.price.exclusiveOnline;
                                                                    var exclusivePriceOnline= responsePackage.package.price.exclusiveOnlinePromo;
                                                                    var priceDescription = responsePackage.package.price.description;


                                                                    // first price //
                                                                    var firstPricePromo = responsePackage.package.firstPrice.promo;
                                                                    var firstPriceRegular = responsePackage.package.firstPrice.regular;
                                                                    var firstPriceRegularExclusiveOnlinePromo = responsePackage.package.firstPrice.exclusiveOnlinePromo;
                                                                    var firstPriceRegularExclusiveOnline = responsePackage.package.firstPrice.exclusiveOnline;

                                                                    var firstPriceCurency = responsePackage.package.firstPrice.currency;
                                                                    var firstPriceDescription = responsePackage.package.firstPrice.description;


                                                                    // second price //
                                                                    var secondPricePromo = responsePackage.package.secondPrice.promo;
                                                                    var secondPriceRegular = responsePackage.package.secondPrice.regular;
                                                                    var secondPriceRegularExclusiveOnlinePromo = responsePackage.package.secondPrice.exclusiveOnlinePromo;
                                                                    var secondPriceRegularExclusiveOnline = responsePackage.package.secondPrice.exclusiveOnline;


                                                                    var secondPriceCurency = responsePackage.package.secondPrice.currency;
                                                                    var secondPriceDescription = responsePackage.package.secondPrice.description;

                                                                    var onePriceRegulaPricesPromo = document.querySelector('.price .regularPrices-promo');
                                                                    var onePriceRegulaPricesRegular = document.querySelector('.price .regularPrices-regular');

                                                                    var onePriceRegularExclusivePromo = document.querySelector('.price .exclusiveOnlinePromo');
                                                                    var onePriceRegularExclusiveRegular = document.querySelector('.price .exclusiveOnline');


                                                                    var splitPricePricefirstPromo = document.querySelector('.price .splitPricefirst-promo');
                                                                    var splitPricePricefirstRegular = document.querySelector('.price .splitPricefirst-regular');


                                                                    var splitPriceFirstExclusivePromo = document.querySelector('.price .firstPriceExclusive-promo');
                                                                    var splitPriceFirstExclusiveRegular = document.querySelector('.price .firstPriceExclusive-regular');

                                                                    var splitPriceSecondPromo = document.querySelector('.price .secondPrice-promo');
                                                                    var splitPriceSecondRegular = document.querySelector('.price .secondPrice-regular');

                                                                    var splitPriceSecondExclusivePromo = document.querySelector('.price .secondPriceExclusive-promo');
                                                                    var splitPriceSecondExclusiveRegular = document.querySelector('.price .secondPriceExclusive-regular');


                                                                    // One Price Promo //
                                                                    if(standardPricePromo !== null){
                                                                        onePriceRegulaPricesPromo.innerHTML = `
                                                                     <span class="value"> ${standardPricePromo}${priceCurrency}</span> <span>(${priceDescription})</span>
                                                                `;
                                                                    } else {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                    }


                                                                    if(standardPrice !== null) {
                                                                        onePriceRegulaPricesRegular.innerHTML = `
                                                                         <span class="value"> ${standardPrice}${priceCurrency}</span> <span>(${priceDescription})</span>
                                                                    `;
                                                                    } else {
                                                                        onePriceRegulaPricesRegular.style.display = 'none'
                                                                    }

                                                                    // One Price Regular //


                                                                    // One Price Exclusive Promo //

                                                                    if(exclusivePriceOnline !== null) {
                                                                        onePriceRegularExclusivePromo.innerHTML = `
                                                                         <span class="value"> ${exclusivePriceOnline}${priceCurrency}</span> <span>(${priceDescription})</span>
                                                                    `;
                                                                    } else {
                                                                        onePriceRegularExclusivePromo.style.display = 'none'
                                                                    }



                                                                    // One Price Exclusive Regular //

                                                                    if(exclusivePrice !== null) {
                                                                        onePriceRegularExclusiveRegular.innerHTML = `
                                                                         <span class="value"> ${exclusivePrice}${priceCurrency}</span> <span>(${priceDescription})</span>
                                                                    `;
                                                                    } else {
                                                                        onePriceRegularExclusiveRegular.style.display = 'none'
                                                                    }




                                                                    // split Price Exclusive Promo //

                                                                    if (firstPricePromo !== null) {
                                                                        splitPricePricefirstPromo.innerHTML = `
                                                                     <span class="value"> ${firstPricePromo}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                                                                `;
                                                                    } else {
                                                                        splitPricePricefirstPromo.style.display = 'none'
                                                                    }


                                                                    // split Price Exclusive Regular //
                                                                    if(firstPriceRegular !== null) {
                                                                        splitPricePricefirstRegular.innerHTML = `
                                                                     <span class="value"> ${firstPriceRegular}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                                                                `;

                                                                    } else {
                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                    }


                                                                    // split Price First Exclusive Promo //
                                                                    if(firstPriceRegularExclusiveOnlinePromo !== null) {
                                                                        splitPriceFirstExclusivePromo.innerHTML = `
                                                                         <span class="value"> ${firstPriceRegularExclusiveOnlinePromo}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                                                                    `;
                                                                    } else {
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';
                                                                    }



                                                                    // split Price First Exclusive regular //

                                                                    if (firstPriceRegularExclusiveOnline !== null) {
                                                                        splitPriceFirstExclusiveRegular.innerHTML = `
                                                                         <span class="value"> ${firstPriceRegularExclusiveOnline}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                                                                    `;

                                                                    } else {
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                    }


                                                                    // split Price Secodn Promo //
                                                                    if(secondPricePromo !== null) {
                                                                        splitPriceSecondPromo.innerHTML = `
                                                                         <span class="value"> ${secondPricePromo}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                                                                    `;
                                                                    } else {
                                                                        splitPriceSecondPromo.style.display = 'none'
                                                                    }

                                                                    // split Price Second regular //

                                                                    if(secondPriceRegular !== null) {splitPriceSecondRegular.innerHTML = `
                                                                             <span class="value"> ${secondPriceRegular}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                                                                        `;
                                                                    } else {
                                                                        splitPriceSecondRegular.style.display = 'none'
                                                                    }
                                                                    // split Price Second exclusive promo //
                                                                    if(secondPriceRegularExclusiveOnlinePromo !== null) {
                                                                        splitPriceSecondExclusivePromo.innerHTML = `
                             <span class="value"> ${secondPriceRegularExclusiveOnlinePromo}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                    } else {
                                                                        splitPriceSecondExclusivePromo.style.display = 'none'
                                                                    }
                                                                    // split Price Second exclusive regular //
                                                                    if (secondPriceRegularExclusiveOnline !== null ) {
                                                                        splitPriceSecondExclusiveRegular.innerHTML = `
                             <span class="value"> ${secondPriceRegularExclusiveOnline}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                    } else {
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none '
                                                                    }

                                                                    if(firstPriceRegularExclusiveOnlinePromo && secondPriceRegularExclusiveOnlinePromo) {
                                                                        splitPriceFirstExclusivePromo.style.display = 'block';
                                                                        splitPriceSecondExclusivePromo.style.display = 'block';
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';

                                                                    } else if (firstPriceRegularExclusiveOnline && secondPriceRegularExclusiveOnline ) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'block';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'block';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }

                                                                    else if (firstPricePromo && secondPricePromo ) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'block';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'block';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }

                                                                    else if (firstPriceRegular && secondPriceRegular ) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'block';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'block';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                    else if (exclusivePriceOnline) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'block';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                    else if (exclusivePrice) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'block';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                    else if (standardPricePromo) {
                                                                        onePriceRegulaPricesPromo.style.display = 'block';
                                                                        onePriceRegulaPricesRegular.style.display = 'none';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                    else if (standardPrice) {
                                                                        onePriceRegulaPricesPromo.style.display = 'none';
                                                                        onePriceRegulaPricesRegular.style.display = 'block';
                                                                        onePriceRegularExclusivePromo.style.display = 'none';
                                                                        onePriceRegularExclusiveRegular.style.display = 'none';

                                                                        splitPricePricefirstRegular.style.display = 'none';
                                                                        splitPricePricefirstPromo.style.display = 'none';
                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceFirstExclusivePromo.style.display = 'none';

                                                                        splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondPromo.style.display = 'none';
                                                                        splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                        splitPriceSecondExclusivePromo.style.display = 'none';
                                                                    }
                                                                }

                                                                // Vantive services
                                                                var vantiveServices = responsePackage.package.vantiveServices;
                                                                var vantiveServicesInternet;
                                                                var vantiveServicesTv;
                                                                if (vantiveServices.idInternet !== null) {
                                                                    vantiveServicesInternet = vantiveServices.idInternet;
                                                                }
                                                                if (vantiveServices.idTV !== null ) {
                                                                    vantiveServicesTv = vantiveServices.idTV
                                                                }

                                                                vantiveID.push(vantiveServicesInternet,vantiveServicesTv);
                                                            }
                                                        }
                                                    }
                                                }
                                                xhr.send();
                                            },200)


                                            break;
                                        }
                                    }
                                    else {
                                        setTimeout(function(){
                                            var elements = document.querySelectorAll('#packagesOptions li');

                                            for (i = 0; i < elements.length; i++) {
                                                var items =  elements[i].dataset.id;
                                                if(elements[i].dataset.id === selectedPachage) {
                                                    const packagename = elements[i].querySelectorAll('span')[0].innerHTML;
                                                    pachetSelectat = packagename;
                                                    document.querySelector('#packages .custom-input').value = packagename;
                                                    document.getElementById('noMatchPackage').style.display = 'none'
                                                    document.getElementById('matchPackage').style.display = 'block';
                                                    document.querySelector('.packageContent').style.display = 'block';
                                                    document.getElementById('selectedPackage').innerText = packagename;
                                                    document.querySelector('#default .description').style.display = 'none';
                                                    document.querySelector('#default #packages').style.display = 'none';
                                                    document.querySelector('.packageContent').classList.add('noBorder');

                                                    var package = (elements[i].dataset.id);
                                                    var staticUrl = 'https://www.orange.ro/ux-admin/api/love/getPackage?packageID=';

                                                    var xhr = new XMLHttpRequest(),
                                                        method = "POST",
                                                        urlPackage = staticUrl + package;

                                                    xhr.open(method, urlPackage, true);
                                                    xhr.onreadystatechange = function () {
                                                        if (xhr.readyState === XMLHttpRequest.DONE) {
                                                            var status = xhr.status;
                                                            if (status === 0 || status >= 200 && status < 400) {
                                                                let responsePackage = JSON.parse(xhr.response);
                                                                if (responsePackage.package.features) {
                                                                    // Vantive services
                                                                    var vantiveServices = responsePackage.package.vantiveServices;
                                                                    var vantiveServicesInternet;
                                                                    var vantiveServicesTv;
                                                                    if (vantiveServices.idInternet !== null) {
                                                                        vantiveServicesInternet = vantiveServices.idInternet;
                                                                    }
                                                                    if (vantiveServices.idTV !== null ) {
                                                                        vantiveServicesTv = vantiveServices.idTV
                                                                    }

                                                                    vantiveID.push(vantiveServicesInternet,vantiveServicesTv);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    xhr.send();

                                                    break;
                                                }
                                                else {
                                                    document.getElementById('noMatchPackage').style.display = 'block';
                                                    document.getElementById('matchPackage').style.display = 'none';
                                                    document.querySelector('#default .description').style.display = 'none';
                                                    document.querySelector('#default #packages').style.display = 'block';
                                                    document.querySelector('.packageContent').classList.remove('noBorder');


                                                    const defaultValue = elements[0];
                                                    console.log(defaultValue);
                                                    var pachage = defaultValue.dataset.id;
                                                    const packagename = defaultValue.querySelectorAll('span')[0].innerHTML;
                                                    pachetSelectat = packagename;
                                                    document.querySelector('.packageContent .pachageBenefits').innerHTML = '';

                                                    document.querySelector('#packages .custom-input').value = packagename;

                                                    setTimeout(function (){
                                                        var staticUrl = 'https://www.orange.ro/ux-admin/api/love/getPackage?packageID=';

                                                        var xhr = new XMLHttpRequest(),
                                                            method = "POST",
                                                            urlPackage = staticUrl + pachage;

                                                        xhr.open(method, urlPackage, true);
                                                        xhr.onreadystatechange = function () {
                                                            if (xhr.readyState === XMLHttpRequest.DONE) {
                                                                var status = xhr.status;
                                                                if (status === 0 || status >= 200 && status < 400) {
                                                                    let responsePackage = JSON.parse(xhr.response);
                                                                    var packageWrapper = document.querySelector('.packageContent');
                                                                    var packageTitle = document.querySelector('.packageContent .packageContent-title');
                                                                    var packageBenefits = document.querySelector('.packageContent .pachageBenefits');
                                                                    packageWrapper.style.display = 'block';
                                                                    packageTitle.innerHTML =  responsePackage.package.title;

                                                                    if (responsePackage.package.features) {
                                                                        const featuresList = responsePackage.package.features;
                                                                        const features =  responsePackage.features;
                                                                        var options = [];
                                                                        for (let el in features) {
                                                                            options.push({
                                                                                description: features[el].description,
                                                                                icon: features[el].iconBlack,
                                                                                idDB: features[el].idDB,
                                                                                title: features[el].title,
                                                                                hideExclusiveOnline: features[el].hideExclusiveOnline
                                                                            })
                                                                        }

                                                                        options.forEach(function(items){
                                                                            const itemId = items.idDB;
                                                                            if(featuresList.includes(itemId)){
                                                                                const icon = items.icon;
                                                                                const description = items.description;
                                                                                const title = items.title;
                                                                                const hideExclusive = items.hideExclusiveOnline;

                                                                                const iconpath = 'https://www.orange.ro/images/' + icon

                                                                                var tvBox = document.createElement('div');
                                                                                tvBox.classList.add('media','align-items-center' , 'pb-4', 'd-flex');
                                                                                tvBox.dataset.hide = hideExclusive;
                                                                                tvBox.innerHTML =    `
                            <div>
                                <img src="${iconpath}" alt="" class="pachageImage">
                            </div>
                            <div class="media-body servicePack">
                             <h6 class="mb-0 f-16">${items.title}</h6>
                             <small class="c-gray-4 f12">${description}</small>
                            </div> 
                             `
                                                                                packageBenefits.appendChild(tvBox);
                                                                            }
                                                                        })

                                                                        if(responsePackage.package.price !== undefined) {
                                                                            // var priceWrapper = document.getElementById('price');
                                                                            // var priceWrapperPrice = document.getElementById('exclusivePrice');
                                                                            // var priceWrapperOnline = document.getElementById('exclusivePriceOnline');
                                                                            //
                                                                            //

                                                                            // standard price//
                                                                            var standardPrice = responsePackage.package.price.regular;
                                                                            var standardPricePromo = responsePackage.package.price.promo;
                                                                            var priceCurrency = responsePackage.package.price.currency;
                                                                            var exclusivePrice =  responsePackage.package.price.exclusiveOnline;
                                                                            var exclusivePriceOnline= responsePackage.package.price.exclusiveOnlinePromo;
                                                                            var priceDescription = responsePackage.package.price.description;


                                                                            // first price //
                                                                            var firstPricePromo = responsePackage.package.firstPrice.promo;
                                                                            var firstPriceRegular = responsePackage.package.firstPrice.regular;
                                                                            var firstPriceRegularExclusiveOnlinePromo = responsePackage.package.firstPrice.exclusiveOnlinePromo;
                                                                            var firstPriceRegularExclusiveOnline = responsePackage.package.firstPrice.exclusiveOnline;

                                                                            var firstPriceCurency = responsePackage.package.firstPrice.currency;
                                                                            var firstPriceDescription = responsePackage.package.firstPrice.description;


                                                                            // second price //
                                                                            var secondPricePromo = responsePackage.package.secondPrice.promo;
                                                                            var secondPriceRegular = responsePackage.package.secondPrice.regular;
                                                                            var secondPriceRegularExclusiveOnlinePromo = responsePackage.package.secondPrice.exclusiveOnlinePromo;
                                                                            var secondPriceRegularExclusiveOnline = responsePackage.package.secondPrice.exclusiveOnline;


                                                                            var secondPriceCurency = responsePackage.package.secondPrice.currency;
                                                                            var secondPriceDescription = responsePackage.package.secondPrice.description;

                                                                            var onePriceRegulaPricesPromo = document.querySelector('.price .regularPrices-promo');
                                                                            var onePriceRegulaPricesRegular = document.querySelector('.price .regularPrices-regular');

                                                                            var onePriceRegularExclusivePromo = document.querySelector('.price .exclusiveOnlinePromo');
                                                                            var onePriceRegularExclusiveRegular = document.querySelector('.price .exclusiveOnline');


                                                                            var splitPricePricefirstPromo = document.querySelector('.price .splitPricefirst-promo');
                                                                            var splitPricePricefirstRegular = document.querySelector('.price .splitPricefirst-regular');


                                                                            var splitPriceFirstExclusivePromo = document.querySelector('.price .firstPriceExclusive-promo');
                                                                            var splitPriceFirstExclusiveRegular = document.querySelector('.price .firstPriceExclusive-regular');

                                                                            var splitPriceSecondPromo = document.querySelector('.price .secondPrice-promo');
                                                                            var splitPriceSecondRegular = document.querySelector('.price .secondPrice-regular');

                                                                            var splitPriceSecondExclusivePromo = document.querySelector('.price .secondPriceExclusive-promo');
                                                                            var splitPriceSecondExclusiveRegular = document.querySelector('.price .secondPriceExclusive-regular');


                                                                            // One Price Promo //
                                                                            if(standardPricePromo !== null){
                                                                                onePriceRegulaPricesPromo.innerHTML = `
                             <span class="value"> ${standardPricePromo}${priceCurrency}</span> <span>(${priceDescription})</span>
                        `;
                                                                            } else {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                            }


                                                                            if(standardPrice !== null) {
                                                                                onePriceRegulaPricesRegular.innerHTML = `
                             <span class="value"> ${standardPrice}${priceCurrency}</span> <span>(${priceDescription})</span>
                        `;
                                                                            } else {
                                                                                onePriceRegulaPricesRegular.style.display = 'none'
                                                                            }

                                                                            // One Price Regular //


                                                                            // One Price Exclusive Promo //

                                                                            if(exclusivePriceOnline !== null) {
                                                                                onePriceRegularExclusivePromo.innerHTML = `
                             <span class="value"> ${exclusivePriceOnline}${priceCurrency}</span> <span>(${priceDescription})</span>
                        `;
                                                                            } else {
                                                                                onePriceRegularExclusivePromo.style.display = 'none'
                                                                            }



                                                                            // One Price Exclusive Regular //

                                                                            if(exclusivePrice !== null) {
                                                                                onePriceRegularExclusiveRegular.innerHTML = `
                             <span class="value"> ${exclusivePrice}${priceCurrency}</span> <span>(${priceDescription})</span>
                        `;
                                                                            } else {
                                                                                onePriceRegularExclusiveRegular.style.display = 'none'
                                                                            }




                                                                            // split Price Exclusive Promo //

                                                                            if (firstPricePromo !== null) {
                                                                                splitPricePricefirstPromo.innerHTML = `
                             <span class="value"> ${firstPricePromo}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPricePricefirstPromo.style.display = 'none'
                                                                            }


                                                                            // split Price Exclusive Regular //
                                                                            if(firstPriceRegular !== null) {
                                                                                splitPricePricefirstRegular.innerHTML = `
                             <span class="value"> ${firstPriceRegular}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                        `;

                                                                            } else {
                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                            }


                                                                            // split Price First Exclusive Promo //
                                                                            if(firstPriceRegularExclusiveOnlinePromo !== null) {
                                                                                splitPriceFirstExclusivePromo.innerHTML = `
                             <span class="value"> ${firstPriceRegularExclusiveOnlinePromo}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';
                                                                            }



                                                                            // split Price First Exclusive regular //

                                                                            if (firstPriceRegularExclusiveOnline !== null) {
                                                                                splitPriceFirstExclusiveRegular.innerHTML = `
                             <span class="value"> ${firstPriceRegularExclusiveOnline}${firstPriceCurency}</span> <span>(${firstPriceDescription})</span>
                        `;

                                                                            } else {
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                            }


                                                                            // split Price Secodn Promo //
                                                                            if(secondPricePromo !== null) {
                                                                                splitPriceSecondPromo.innerHTML = `
                             <span class="value"> ${secondPricePromo}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPriceSecondPromo.style.display = 'none'
                                                                            }

                                                                            // split Price Second regular //

                                                                            if(secondPriceRegular !== null) {
                                                                                splitPriceSecondRegular.innerHTML = `
                             <span class="value"> ${secondPriceRegular}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPriceSecondRegular.style.display = 'none'
                                                                            }
                                                                            // split Price Second exclusive promo //
                                                                            if(secondPriceRegularExclusiveOnlinePromo !== null) {
                                                                                splitPriceSecondExclusivePromo.innerHTML = `
                             <span class="value"> ${secondPriceRegularExclusiveOnlinePromo}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPriceSecondExclusivePromo.style.display = 'none'
                                                                            }
                                                                            // split Price Second exclusive regular //
                                                                            if (secondPriceRegularExclusiveOnline !== null ) {
                                                                                splitPriceSecondExclusiveRegular.innerHTML = `
                             <span class="value"> ${secondPriceRegularExclusiveOnline}${secondPriceCurency}</span> <span>(${secondPriceDescription})</span>
                        `;
                                                                            } else {
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none '
                                                                            }

                                                                            if(firstPriceRegularExclusiveOnlinePromo && secondPriceRegularExclusiveOnlinePromo) {
                                                                                splitPriceFirstExclusivePromo.style.display = 'block';
                                                                                splitPriceSecondExclusivePromo.style.display = 'block';
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';

                                                                            } else if (firstPriceRegularExclusiveOnline && secondPriceRegularExclusiveOnline ) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'block';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'block';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';
                                                                            }

                                                                            else if (firstPricePromo && secondPricePromo ) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'block';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'block';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';
                                                                            }

                                                                            else if (firstPriceRegular && secondPriceRegular ) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'block';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'block';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';
                                                                            }
                                                                            else if (exclusivePriceOnline) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'block';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';
                                                                            }
                                                                            else if (exclusivePrice) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'block';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';
                                                                            }
                                                                            else if (standardPricePromo) {
                                                                                onePriceRegulaPricesPromo.style.display = 'block';
                                                                                onePriceRegulaPricesRegular.style.display = 'none';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';
                                                                            }
                                                                            else if (standardPrice) {
                                                                                onePriceRegulaPricesPromo.style.display = 'none';
                                                                                onePriceRegulaPricesRegular.style.display = 'block';
                                                                                onePriceRegularExclusivePromo.style.display = 'none';
                                                                                onePriceRegularExclusiveRegular.style.display = 'none';

                                                                                splitPricePricefirstRegular.style.display = 'none';
                                                                                splitPricePricefirstPromo.style.display = 'none';
                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceFirstExclusivePromo.style.display = 'none';

                                                                                splitPriceFirstExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondPromo.style.display = 'none';
                                                                                splitPriceSecondExclusiveRegular.style.display = 'none';
                                                                                splitPriceSecondExclusivePromo.style.display = 'none';
                                                                            }
                                                                        }

                                                                        // Vantive services
                                                                        var vantiveServices = responsePackage.package.vantiveServices;
                                                                        var vantiveServicesInternet;
                                                                        var vantiveServicesTv;
                                                                        if (vantiveServices.idInternet !== null) {
                                                                            vantiveServicesInternet = vantiveServices.idInternet;
                                                                        }
                                                                        if (vantiveServices.idTV !== null ) {
                                                                            vantiveServicesTv = vantiveServices.idTV
                                                                        }

                                                                        vantiveID.push(vantiveServicesInternet,vantiveServicesTv);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        xhr.send();
                                                    },200)


                                                    break;
                                                }
                                            }
                                        }, 100);
                                    }
                                }
                            }
                        };
                        xhr.send();
                    }
                    dataLayer.push({
                        'event': 'gaEventTriggered',
                        'eventCategory': 'Orange Home',
                        'eventAction':'Adresa Ta',
                        'eventLabel': 'eligibila'
                    });
                }
                else {

                    goToStep2();
                    document.getElementById('step-3').style.display = 'none';
                    document.getElementById('telephone').innerText = phoneNumber;

                    setTimeout(function(){
                        document.querySelector('#step-2 .not-expended').style.display = 'none';
                    },200);


                    document.getElementById('nullPhone').style.display = 'block';

                    noNumberEligible = true;

                }


            }
        };
        xhr.send()
    }

});

var more = document.querySelectorAll('.more');
more.forEach(function (item){
    item.addEventListener('click', function (){
        item.nextElementSibling.classList.add('show')
        item.style.display = 'none';

        dataLayer.push({
            'event':'gaEventTriggered',
            'eventCategory':'Orange Home',
            'eventAction':'GDPR',
            'eventLabel':'mai mult 1st Step'
        });

    })
})

// Open select
const input = document.querySelectorAll('.input-wrapper');
const inputWrapper = document.querySelector('.form-group');
const options = document.querySelectorAll('.options ul li');
const inputSearch = document.querySelectorAll('.input-wrapper .custom-input');


input.forEach(function(itsm){
    itsm.addEventListener('click', function(e){
        const elTarget = e.target.parentNode;
        const otherOptions = document.querySelectorAll('.options');

        otherOptions.forEach(function(el) {
            el.classList.remove('active');
        });
        elTarget.nextElementSibling.classList.add('active');

    });

});


const $menu = $('.input-wrapper');
$(document).mouseup(e => {
    if (!$menu.is(e.target) // if the target of the click isn't the container...
        && $menu.has(e.target).length === 0) // ... nor a descendant of the container
    {
        const opt = document.querySelectorAll('.options');
        opt.forEach(function(items){
            if(items.classList.contains('active')) {
                items.classList.remove('active');
            }
        })
    }
});

const els = function(el){
    el.forEach(function(items){
        items.addEventListener('click', function(){
            const closestInput = items.parentNode.parentElement.previousElementSibling;
            const closestInputEl = closestInput.querySelector('.custom-input');
            const clickedEl = items.innerHTML;
            items.parentNode.parentNode.classList.remove('active');
            closestInputEl.value = clickedEl;

            const nextInput = items.parentNode.parentElement.parentElement.nextElementSibling;

            if(nextInput) {
                items.parentNode.parentElement.parentElement.classList.remove('active-field');
                nextInput.classList.add('active-field');
                const elNextInput = nextInput.querySelector('input').focus();


                setTimeout(function(){
                    items.parentNode.parentElement.parentElement.classList.add('passed-option');
                }, 500)

                setTimeout(function (){
                    nextInput.querySelector('.custom-options ').classList.add('active');
                },700)

            } else {
                document.getElementById('to-step-3').classList.remove('disabled')
            }
        })
    })
};


els(options);


function test(el){
    const targetEl = el.parentNode.parentNode.parentNode.previousElementSibling;
    const targetValue = targetEl.querySelector('.input-select').value
    targetEl.parentElement.classList.remove('active-field');
    targetEl.parentElement.classList.add('passed-option');
    el.parentNode.parentNode.parentNode.classList.remove('active');
    if( targetEl.parentElement.nextElementSibling) {
        targetEl.parentElement.nextElementSibling.classList.add('active-field')
    }
    buildingNumber = targetValue;
    blockNo = 0;
    filledAddress.innerHTML = selectedElName + ', ' + selectedCityName + ', ' + streetName + ', ' + streetNumber + ', ' + buildingNumber;

}


function scrollToElement(element){
    var windowWidth = window.innerWidth;
    if (windowWidth < 767) {
        element.addEventListener('click', function (){
            const offsetTop = element.getBoundingClientRect().top;
            const yOffset = -60;
            const y = offsetTop + window.pageYOffset + yOffset;
            window.scrollTo({top: y, behavior: 'smooth'});
        })
    }

}

var closeModal = document.querySelectorAll('.closeModal');

closeModal.forEach(function (item){
    item.addEventListener('click', function (e){
        e.preventDefault();
            document.getElementById('modal-phone').classList.remove('open');
            dataLayer.push({
                'event':'gaEventTriggered',
                'eventCategory':'Orange Home',
                'eventAction':'Action-modal',
                'eventLabel': 'Label Inchide'
            });
    })
});
