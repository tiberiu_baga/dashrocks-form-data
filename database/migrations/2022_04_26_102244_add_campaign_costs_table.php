$table->bigIncrements('id');<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampaignCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date')->comment('date');
            $table->string('business_area')->comment('Linii de business')->nullable();
            $table->string('campaign')->comment('campanie')->nullable();
            $table->string('source')->comment('Source')->nullable();
            $table->string('medium')->comment('Medium')->nullable();
            $table->float('cost')->comment('cost in Euro')->default(0);

            $table->timestamps();
            $table->index(['date', 'campaign']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_costs');
    }
}
