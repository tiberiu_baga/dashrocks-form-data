<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToActionsCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions', function (Blueprint $table) {
            $table->index('form');
            $table->index('phone');
            $table->index('created_at');
            $table->index('crm_send');
            $table->index('crm_type');
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->index('email');
            $table->index('phone');
            $table->index('sso_id');
            $table->index('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actions', function (Blueprint $table) {
            $table->dropIndex('actions_form_index');
            $table->dropIndex('actions_phone_index');
            $table->dropIndex('actions_created_at_index');
            $table->dropIndex('actions_crm_send_index');
            $table->dropIndex('actions_crm_type_index');
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->index('customers_email_index');
            $table->index('customers_phone_index');
            $table->index('customers_sso_id_index');
            $table->index('customer_token_index');
        });
    }
}
