<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoogleCampaignsTable extends Migration
{
    public function up()
    {
        Schema::create('google_campaigns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('campaign_id')->unique();
            $table->string('campaign', 255);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('google_campaigns');
    }
}
