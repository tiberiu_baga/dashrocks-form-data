<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrangeGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orange_games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->default('summer-lotery');
            $table->string('order_id');
            $table->string('order_date');
            $table->string('order_status')->nullable();
            $table->text('order_items')->nullable();
            $table->string('code');
            $table->string('phone')->nullable();
            $table->string('status')->nullable()->comment('code|winner|error|not-winner');
            $table->integer('prize_id')->nullable();
            $table->string('prize_level')->nullable();
            $table->text('extras')->nullable();
            $table->text('errors')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orange_games');
    }
}
