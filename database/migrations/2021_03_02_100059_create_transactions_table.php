<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('date')->comment('data comanda - tranzactie')->nullable();
            $table->integer('numar_comanda')->comment('name of client')->nullable();

            $table->string('name')->comment('name of client')->nullable();
            $table->string('email')->comment('email of client')->nullable();
            $table->string('phone')->comment('phone of client')->nullable();
            $table->string('token')->comment('web push token')->nullable();
            $table->string('sso_id')->comment('logged in internal id')->nullable();

            $table->string('state')->comment('state of client')->nullable();
            $table->string('city')->comment('city of client')->nullable();
            $table->string('address')->comment('address of client')->nullable();

            $table->string('status')->comment('status comanda')->nullable();
            $table->string('produs')->comment('produse comandate')->nullable();
            $table->string('produs_cod_oa')->comment('cod OA')->nullable();
            $table->string('motive_rejectare')->comment('motive rejectare')->nullable();
            $table->string('valoare_cu_tva')->comment('valoare produs cu TVA')->nullable();
            $table->text('extras')->comment('json format of extra fields')->nullable();
            $table->timestamps();

            $table->index(['numar_comanda']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
