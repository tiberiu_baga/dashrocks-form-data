<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVoucherPinFieldToOrangeGamePrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orange_game_prizes', function (Blueprint $table) {
            $table->string('voucher_pin')->after('voucher_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orange_game_prizes', function (Blueprint $table) {
            $table->dropColumn('voucher_pin');
        });
    }
}
