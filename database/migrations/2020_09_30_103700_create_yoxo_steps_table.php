<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoxoStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yoxo_user_steps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sso_id')->comment('logged in internal id')->nullable();
            $table->date('step1_date')->comment('step 1 date')->nullable();
            $table->date('step2_date')->comment('step 2 date')->nullable();
            $table->date('step3_date')->comment('step 3 date')->nullable();
            $table->date('step4_date')->comment('step 4 date')->nullable();
            $table->date('step5_date')->comment('step 5 date')->nullable();
            $table->date('step6_date')->comment('step 6 date')->nullable();
            $table->date('step7_date')->comment('step 7 date')->nullable();
            $table->date('step8_date')->comment('step 8 date')->nullable();
            $table->date('step9_date')->comment('step 9 date')->nullable();
            $table->date('step10_date')->comment('step 10 date')->nullable();
            $table->date('step11_date')->comment('step 11 date')->nullable();
            $table->date('step12_date')->comment('step 12 date')->nullable();
            $table->date('step13_date')->comment('step 13 date')->nullable();
            $table->date('step14_date')->comment('step 14 date')->nullable();

            $table->date('goal1_date')->comment('goal 1 date')->nullable();
            $table->date('goal2_date')->comment('goal 2 date')->nullable();
            $table->date('goal3_date')->comment('goal 3 date')->nullable();
            $table->date('goal4_date')->comment('goal 4 date')->nullable();
            $table->date('goal5_date')->comment('goal 5 date')->nullable();
            $table->date('goal6_date')->comment('goal 6 date')->nullable();
            $table->date('goal7_date')->comment('goal 7 date')->nullable();
            $table->date('goal8_date')->comment('goal 8 date')->nullable();
            $table->date('goal9_date')->comment('goal 9 date')->nullable();
            $table->date('goal10_date')->comment('goal 10 date')->nullable();

            $table->timestamps();
        });

        Schema::create('yoxo_step_labels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('step');
            $table->string('label');
        });

        Schema::create('yoxo_features', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('feature_name');
            $table->integer('accounts');
        });

        Schema::create('yoxo_signupintents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->integer('signupintent');
        });

        Schema::create('yoxo_visitors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->integer('visitors');
        });

        \DB::statement(
            "INSERT INTO yoxo_step_labels (`label`, `step`) VALUES
            ('Created account', 'step1'),
            ('Insert phone number', 'step2'),
            ('Verify PIN Screen', 'step3'),
            ('Number Eligibility confirmed', 'step4'),
            ('Scan ID', 'step5'),
            ('Scan ID Summary', 'step6'),
            ('GDPR Screen (eligible owner)', 'step7'),
            ('Purchased subscription', 'step8'),
            ('Became active', 'step9'),
            ('Eligibility denial', 'goal1'),
            ('Reconfigure Subscription', 'goal2'),
            ('Canceled subscription', 'goal3'),
            ('Purchased option', 'goal4')"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yoxo_user_steps');
        Schema::dropIfExists('yoxo_step_labels');
        Schema::dropIfExists('yoxo_features');
        Schema::dropIfExists('yoxo_signupintent');
        Schema::dropIfExists('yoxo_visitors');
    }
}
