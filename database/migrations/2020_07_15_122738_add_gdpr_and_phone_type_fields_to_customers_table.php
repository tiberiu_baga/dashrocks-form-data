<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGdprAndPhoneTypeFieldsToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('phone_type')->after('package_code')->nullable();

            $table->boolean('gdpr_directmarketing')->after('phone_type')->default(false);
            $table->boolean('gdpr_marketresearch')->after('gdpr_directmarketing')->default(false);
            $table->boolean('gdpr_orangemoney')->after('gdpr_marketresearch')->default(false);
            $table->boolean('gdpr_thirdparty')->after('gdpr_orangemoney')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('phone_type');
            $table->dropColumn('gdpr_directmarketing');
            $table->dropColumn('gdpr_marketresearch');
            $table->dropColumn('gdpr_orangemoney');
            $table->dropColumn('gdpr_thirdparty');
        });
    }
}
