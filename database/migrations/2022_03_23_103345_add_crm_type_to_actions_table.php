<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCrmTypeToActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions', function (Blueprint $table) {
            $table->string('crm_type')->after('status')->nullable();
        });

        \DB::statement(
            "UPDATE actions SET crm_type = 'oro-sfa' WHERE
                ( crm_send = 1 AND crm_send_response != 'email' )
                OR ( crm_send = 0 AND crm_send_response IS NOT NULL AND crm_send_response != 'email')"
        );
        \DB::statement("UPDATE actions SET crm_type = 'email' WHERE crm_send_response = 'email'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actions', function (Blueprint $table) {
            $table->dropColumn('crm_type');
        });
    }
}
