<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date')->comment('date');
            $table->string('type')->comment('Linii de business')->nullable();
            $table->string('business_area')->comment('Linii de business')->nullable();
            $table->string('campaign')->comment('campanie')->nullable();
            $table->string('source')->comment('Source')->nullable();
            $table->string('medium')->comment('Medium')->nullable();
            $table->integer('count')->comment('count')->default(0);
            $table->integer('clicks')->comment('count')->default(0);
            $table->integer('impressions')->comment('count')->default(0);
            $table->float('cost')->comment('cost in Euro')->default(0);

            $table->timestamps();
            $table->index(['date', 'type', 'campaign']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_infos');
    }
}
