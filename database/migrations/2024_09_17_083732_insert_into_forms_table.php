<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table = DB::table('forms');

        foreach ($this->getData() as $formName => $data) {
            $row = [
                'name' => $formName,
                'active' => 1,
                'page' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            if (!$data) {
                $table->insert($row);

                continue;
            }
            if (is_array($data)) {
                foreach ($data as $value) {
                    $row['page'] = $value;

                    $table->insert($row);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('forms')
            ->whereIn('name', array_keys($this->getData()))
            ->delete();
    }

    private function getData(): array
    {
        return [
            'love-contact-forms' => [
                'https://www.orange.ro/',
                'https://www.orange.ro/oferte-speciale/',
                'https://www.orange.ro/abonamente/',
                'https://www.orange.ro/love/',
                'https://www.orange.ro/verifica-acoperirea/',
                'https://www.orange.ro/oferta-voce/',
                'https://www.orange.ro/promotii-voce/',
                'https://internet-mobil.ro/',
                'https://internet-acasa.ro/',
            ],
            'online-leads-love-configurator' => null,
            'comparator-love' => null,
            'myaccount-reshape' => null,
            'myaccount-reshape-packages-and-options' => null,
            'myaccount-reshape-invoice-cronos' => null,
        ];
    }
};
