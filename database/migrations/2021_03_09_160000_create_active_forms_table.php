<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateActiveFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('forms')) {
            Schema::create('forms', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->comment('form name')->nullable();
                $table->string('description')->comment('description of the form')->nullable();
                $table->boolean('active')->comment('state of the form')->nullable()->default(true);
                $table->timestamps();
            });
        }
        if (Schema::hasTable('forms')) {
            DB::statement('INSERT INTO forms (name) SELECT distinct(form) FROM actions');
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }

}
