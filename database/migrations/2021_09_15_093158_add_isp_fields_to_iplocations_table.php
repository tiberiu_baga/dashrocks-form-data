<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIspFieldsToIplocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ip_locations', function (Blueprint $table) {
            $table->string('asn')->after('rdns')->nullable();
            $table->string('isp')->after('asn')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ip_locations', function (Blueprint $table) {
            $table->dropColumn('asn');
            $table->dropColumn('isp');
        });
    }
}
