<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSourceFieldsToActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions', function (Blueprint $table) {
            $table->string('source')->after('crm_send_response')->nullable();
            $table->string('referrer')->after('source')->nullable();
            $table->string('utm_campaign')->after('referrer')->nullable();
            $table->string('utm_medium')->after('utm_campaign')->nullable();
            $table->string('utm_source')->after('utm_medium')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actions', function (Blueprint $table) {
            $table->dropColumn('source');
            $table->dropColumn('referrer');
            $table->dropColumn('utm_campaign');
            $table->dropColumn('utm_medium');
            $table->dropColumn('utm_source');
        });
    }
}
