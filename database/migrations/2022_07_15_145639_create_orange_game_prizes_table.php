<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrangeGamePrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orange_game_prizes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->default('summer-lotery');
            $table->string('voucher_code');
            $table->float('voucher_value');
            $table->string('voucher_type')->nullable();
            $table->string('description')->nullable();
            $table->boolean('assigned')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orange_game_prizes');
    }
}
