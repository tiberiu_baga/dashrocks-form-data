<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasketAbandonersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basket_abandoners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('datetime')->comment('datetime');
            $table->string('sso_id')->nullable();
            $table->string('email')->nullable();
            $table->boolean('sent')->default(false);
            $table->string('status')->nullable();
            $table->string('response', 400)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basket_abandoners');
    }
}
