<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('logged_in')->default(0);

            $table->string('form')->comment('the name of the form we are saving');
            $table->string('page')->comment('the page on which the form was accessed')->nullable();
            $table->string('medium')->comment('android or ios or web')->default('web');
            $table->string('agent')->comment('chrome, safari, firefox')->default('none');

            $table->string('name')->comment('name of client')->nullable();
            $table->string('address')->comment('address of client')->nullable();
            $table->string('email')->comment('email of client')->nullable();
            $table->string('phone')->comment('phone of client')->nullable();
            $table->string('token')->comment('web push token')->nullable();
            $table->string('sso_id')->comment('logged in internal id')->nullable();

            $table->string('subject')->comment('form subject')->nullable();
            $table->text('message')->comment('form message')->nullable();

            $table->boolean('gdpr')->default(false);
            $table->text('post_data')->comment('json format of posted data')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
