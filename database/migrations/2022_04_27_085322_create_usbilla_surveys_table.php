<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsbillaSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usabilla_surveys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('external_id');
            $table->date('date')->comment('form creation date');
            $table->timestamp('timestamp')->comment('timestamp');
            $table->datetime('feedback_date')->comment('date when feedback was sent ');
            $table->string('app_id')->comment('appId');
            $table->string('app_name')->comment('appName');
            $table->string('app_version')->comment('appVersion');
            $table->string('device_name')->comment('deviceName')->nullable();
            $table->string('os_name')->comment('osName')->nullable();
            $table->string('os_version')->comment('osVersion')->nullable();
            $table->string('language')->comment('language')->nullable();
            $table->string('rooted')->comment('rooted')->nullable();
            $table->string('free_memory')->comment('freeMemory')->nullable();
            $table->string('total_memory')->comment('totalMemory')->nullable();
            $table->string('orientation')->comment('orientation')->nullable();
            $table->string('geo_location')->comment('geoLocation')->nullable();
            $table->string('location')->comment('location')->nullable();
            $table->string('connection')->comment('connection')->nullable();

            $table->string('custom_data_email')->comment('customData email')->nullable();
            $table->string('custom_data_msisdn')->comment('customData msisdn')->nullable();
            $table->string('custom_data_username')->comment('customData username')->nullable();

            $table->string('closed_feedback_loop')->comment('Closed_Feedback_Loop')->nullable();
            $table->string('customer_effort_score')->comment('Customer_Effort_Score')->nullable();
            $table->string('negative_nps_reason')->comment('Negative_NPS_reason')->nullable();
            $table->string('other_reason_neutral_detractor')->comment('Other_reason_Neutral_Detractor')->nullable();
            $table->string('other_reason_promoter')->comment('Other_reason_Promoter')->nullable();
            $table->string('positive_nps_reason')->comment('Positive_NPS_reason')->nullable();
            $table->string('process_time')->comment('Process_time')->nullable();
            $table->string('mood')->comment('mood')->nullable();
            $table->string('nps')->comment('nps')->nullable();
            $table->string('screenshot')->comment('screenshot')->nullable();
            $table->string('screensize')->comment('screensize')->nullable();

            $table->timestamps();
            $table->index(['app_id', 'feedback_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usabilla_surveys');
    }
}
