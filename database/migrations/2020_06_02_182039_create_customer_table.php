<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sso_id')->comment('logged in internal id')->nullable();
            $table->string('phone')->comment('phone of client')->nullable();
            $table->string('email')->comment('email of client')->nullable();
            $table->string('token')->comment('web push token')->nullable();
            $table->string('ip')->comment('ip address')->nullable();

            $table->string('medium')->comment('android or ios or web - last access medium')->default('web');
            $table->string('agent')->comment('chrome, safari, firefox')->default('none');

            $table->string('name')->comment('name of client')->nullable();
            $table->string('state')->comment('state of client')->nullable();
            $table->string('city')->comment('city of client')->nullable();

            $table->string('address')->comment('address of client')->nullable();

            $table->boolean('gdpr')->default(false);

            $table->string('type')->nullable();
            $table->float('loyalty_eur')->nullable();
            $table->text('offer_eligibility')->nullable();
            $table->datetime('last_loyalty_date')->nullable();
            $table->datetime('contract_expiration_date')->nullable();
            $table->string('package_code')->nullable();
            $table->text('extras')->comment('json format of extra fields')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
