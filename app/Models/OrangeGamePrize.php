<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrangeGamePrize extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function setAsUnavailable()
    {
        $this->assigned = true;
        $this->save();
    }
}
