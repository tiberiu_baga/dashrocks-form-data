<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function setCrmResponse($statusCode, $response, $crmType)
    {
        $this->crm_type = $crmType;
        $this->crm_send = in_array($statusCode, [200, 201, 202]);

        $crmSendResponse = null;
        $decoded = json_decode($response);
        if($decoded) {
            if(is_array($decoded) && isset($decoded[0]) && isset($decoded[0]->defaultMessage)) {
                $returnResponse = $decoded[0]->defaultMessage;
                $crmSendResponse = $returnResponse . substr($response, 0, 400);
            }
            if(isset($decoded->message)) {
                $crmSendResponse = $decoded->message;
            }
        }

        if (!$crmSendResponse && (is_string($response) || is_numeric($response))) {
            $crmSendResponse = $response;
        }

        $this->crm_send_response = $crmSendResponse;

        $this->save();
    }

    public function setStatus($status)
    {
        $this->status = $status;
        $this->save();
    }
}
