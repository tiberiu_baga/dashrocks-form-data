<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoogleCampaign extends Model
{

    protected $table = 'google_campaigns';

    protected $guarded = ['id'];
}
