<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrangeGame extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $casts = [
        'errors' => 'array',
        'order_items' => 'array',
        'extras' => 'array',
    ];

    public function prize()
    {
        return $this->hasOne(OrangeGamePrize::class, 'id', 'prize_id');
    }

    public function prizes()
    {
        return $this->hasMany(OrangeGamePrize::class, 'type', 'type');
    }

    public function availablePrizes()
    {
        return $this->prizes()->where('assigned', 0);
    }

    public function setAsWinner()
    {
        $this->status = 'winner';
        $this->save();
    }

    public function setAsNotWinner()
    {
        $this->status = 'not-winner';
        $this->save();
    }
}
