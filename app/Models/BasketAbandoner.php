<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasketAbandoner extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function setAsSent()
    {
        $this->sent = true;
        $this->save();
    }
}
