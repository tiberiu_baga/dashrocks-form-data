<?php

namespace App\Services\BigQuery;

// A fluent interface to provide a sync configuration
// for a BigQuerySyncService
// Parameters:
// - $fromTable: The table to sync
// - $toTable: The table to sync to
// - $fromConnection: The connection to sync from
// - $isIncremental: Whether to sync incrementally
// - $fromSelectClosure: Closure to provide the Laravel query builder, it will be called with the query builder as the first parameter
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SyncConfig
{
    public string $projectId = 'oro-bigquery';
    public string $datasetId = 'oro_dl';

    public string $bigQueryKeyFilePath = '';

    public string $fromTable;
    public string $toTable;
    public string $fromConnection;
    public bool $incrementalSync = false;

    public array $excludeColumns = [];

    private array $tableColumns = [];

    public ?\Closure $fromSelectClosure = null;

    public string $datasetLocation = 'europe-west3';

    public string $bucketName = 'bigquery-datalake-imports';

    public function __construct(
        string    $fromTable,
        string    $toTable,
        string    $fromConnection,
        bool      $isIncremental,
        ?\Closure $fromSelectClosure,
        array     $excludeColumns = [],
        string    $projectId = 'oro-bigquery',
        string    $datasetId = 'oro_dl',
        string    $bigQueryKeyFilePath = null,
        string    $datasetLocation = 'eu-west3',
        string    $bucketName = 'bigquery-datalake-imports'
    )
    {
        $this->fromTable = $fromTable;
        $this->toTable = $toTable;
        $this->fromConnection = $fromConnection;
        $this->incrementalSync = $isIncremental;
        $this->fromSelectClosure = $fromSelectClosure;
        $this->bigQueryKeyFilePath = $bigQueryKeyFilePath ?? base_path('oro-bigquery-055587fd35ce.json');
        $this->excludeColumns = $excludeColumns;
        $this->projectId = $projectId;
        $this->datasetId = $datasetId;
        $this->datasetLocation = $datasetLocation;
        $this->bucketName = $bucketName;
    }

    public static function fromSyncConfig(string $syncKey, array $syncConfig): SyncConfig
    {
        return new SyncConfig(
            $syncConfig['from_table'],
            $syncConfig['to_table'] ?? $syncKey,
            $syncConfig['from_connection'] ?? '',
            $syncConfig['incremental_sync'] ?? false,
            $syncConfig['from_select_closure'] ?? null,
            $syncConfig['exclude_columns'] ?? [],
            $syncConfig['project_id'] ?? 'oro-bigquery',
            $syncConfig['dataset_id'] ?? 'oro_dl',
            $syncConfig['big_query_key_file_path'] ?? base_path('oro-bigquery-055587fd35ce.json'),
            $syncConfig['dataset_location'] ?? 'europe-west3',
            $syncConfig['bucket_name'] ?? 'bigquery-datalake-imports'
        );
    }

    public static function fromTable(string $fromTable, string $toTable, string $fromConnection): SyncConfig
    {
        return new SyncConfig($fromTable, $toTable, $fromConnection, false, null);
    }

    public static function usingSelectClosure(\Closure $fromSelectClosure, string $fromTable, string $toTable, string $fromConnection): SyncConfig
    {
        return new SyncConfig($fromTable, $toTable, $fromConnection, false, $fromSelectClosure);
    }

    public function incrementalSync(): SyncConfig
    {
        $this->incrementalSync = true;
        return $this;
    }

    public function isFull(): SyncConfig
    {
        $this->incrementalSync = false;
        return $this;
    }

    public function setColumns(array $columns): SyncConfig
    {
        $this->tableColumns = $columns;
        return $this;
    }

    public function getColumns($force = false): array
    {
        if (count($this->tableColumns) === 0 || $force) {
            DB::connection($this->fromConnection)->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            $this->tableColumns = [];
            $columnNames = Schema::connection($this->fromConnection)->getColumnListing($this->fromTable);
            foreach ($columnNames as $columnName) {
                if (in_array($columnName, $this->excludeColumns, true)) {
                    continue;
                }
                $this->tableColumns[$columnName] = [
                    'name' => $columnName,
                    'type' => $this->mapToBigQueryColumnType(Schema::connection($this->fromConnection)->getColumnType($this->fromTable, $columnName))
                ];
            }
        }

        return $this->tableColumns;
    }

    private function mapToBigQueryColumnType(string $mysqlColumnType): string
    {
        $mysqlColumnType = strtolower($mysqlColumnType);

        switch ($mysqlColumnType) {
            case 'integer':
            case 'bigint':
                return 'INTEGER';
            case 'string':
            case 'text':
                return 'STRING';
            case 'datetime':
                return 'DATETIME';
            case 'date':
                return 'DATE';
            case 'boolean':
                return 'BOOLEAN';
            case 'float':
                return 'FLOAT';
            case 'json':
                return 'RECORD';
            case 'decimal':
                return 'NUMERIC';
            default:
                throw new \Exception('Unsupported column type: ' . $mysqlColumnType);
        }

    }


}
