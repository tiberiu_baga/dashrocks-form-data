<?php

namespace App\Services\BigQuery;

interface SaveOperationInterface
{
    public function save(array $row);

    public function commit();

}
