<?php

namespace App\Services\BigQuery;

use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Core\ExponentialBackoff;
use Google\Cloud\Storage\StorageClient;

class LoadOperation implements SaveOperationInterface
{
    private SyncConfig $syncConfig;

    private $jsonNdFileHandle;
    private $jsonNdFilePath;
    private $jsonNdFileName;

    private $append;

    private $rowCount = 0;

    public function __construct(SyncConfig $syncConfig, bool $append = false)
    {
        $this->syncConfig = $syncConfig;
        $this->jsonNdFileName = $syncConfig->projectId . '-' . $syncConfig->toTable . '.ndjson';
        $this->jsonNdFilePath = storage_path('app/' . $this->jsonNdFileName);
        $this->append = $append;
    }

    public function setAppendMode(bool $append = true)
    {
        $this->append = $append;
    }

    public function save(array $row)
    {
        if (!$this->jsonNdFileHandle) {
            $this->jsonNdFileHandle = fopen($this->jsonNdFilePath, 'w');
        }

        $payload = $row['payload'] ?? json_encode($row);
        $payload = str_replace('"0000-00-00 00:00:00"', 'null', $payload);
        $payload = str_replace("\r", "", $payload);
        $payload = str_replace("\n", "", $payload);
        fwrite($this->jsonNdFileHandle, $payload . PHP_EOL);
        $this->rowCount++;
    }

    public function commit()
    {
        if ($this->rowCount === 0) {
            echo "No rows to load into BigQuery table {$this->syncConfig->toTable}" . PHP_EOL;
            return;
        }

        fclose($this->jsonNdFileHandle);
        $this->uploadTableDataFileToGoogleCloud();

        $this->loadTableDataFileIntoBigQuery();
    }

    private function uploadTableDataFileToGoogleCloud()
    {
        $storage = new StorageClient([
            'keyFilePath' => base_path('oro-bigquery-055587fd35ce.json'),
        ]);
        $bucketName = $this->syncConfig->bucketName;
        $bucket = $storage->bucket($bucketName);
        $chunkSize = 262144; // chunk in bytes

        $options = [
            'chunkSize' => $chunkSize,
            'name' => $this->jsonNdFileName,
        ];

        $uploader = $bucket->getResumableUploader(
            fopen($this->jsonNdFilePath, 'r'),
            $options
        );

        try {
            $object = $uploader->upload();
        } catch (GoogleException $ex) {
            $resumeUri = $uploader->getResumeUri();
            $object = $uploader->resume($resumeUri);
        }
    }

    private function loadTableDataFileIntoBigQuery()
    {
        $tableId = $this->syncConfig->toTable;

        // instantiate the bigquery table service
        $bigQuery = new BigQueryClient([
            'projectId' => $this->syncConfig->projectId,
            'keyFilePath' => $this->syncConfig->bigQueryKeyFilePath
        ]);

        if ($bigQuery->dataset($this->syncConfig->datasetId)->exists()) {
            $dataset = $bigQuery->dataset($this->syncConfig->datasetId);
        } else {
            $dataset = $bigQuery->createDataset($this->syncConfig->datasetId, [
                'location' => $this->syncConfig->datasetLocation
            ]);
        }

        $table = $dataset->table($tableId);

        // create the import job
        $gcsUri = 'gs://' . $this->syncConfig->bucketName . '/' . $this->jsonNdFileName;

        $schema = [
            'fields' => array_map(static function ($column) {
                return [
                    'name' => $column['name'],
                    'type' => $column['type']
                ];
            }, array_values($this->syncConfig->getColumns()))
        ];

        $loadConfig = $table->loadFromStorage($gcsUri)
            ->schema($schema)
            ->sourceFormat('NEWLINE_DELIMITED_JSON')
            ->writeDisposition($this->append ? 'WRITE_APPEND' : 'WRITE_TRUNCATE');
        $job = $table->runJob($loadConfig);
        // poll the job until it is complete
        $backoff = new ExponentialBackoff(10);
        $backoff->execute(function () use ($job) {
            $job->reload();
            if (!$job->isComplete()) {
                throw new \Exception('Job has not yet completed', 500);
            }
        });
        // check if the job has errors
        if (isset($job->info()['status']['errorResult'])) {
            $error = $job->info()['status']['errorResult']['message'];
            throw new \Exception(sprintf('Error running BigQuery load job: %s' . PHP_EOL, $error));
        }
    }
}
