<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Action;
use App\Models\GoogleCampaign;

class GoogleAdsLeadCampaignMatcher
{
    private $campaignData;

    public function __construct(array $campaignData = null)
    {
        $this->campaignData = $campaignData;
    }

    public function matchCampaign($postData, $lead)
    {
        if (isset($postData['campaign_id']) && $campaignName = $this->getCampaignName($postData['campaign_id'])) {
            if ($lead instanceof Action) {
                $lead->utm_campaign = $campaignName;
                $lead->utm_source = $lead->utm_source ?? 'google';
                $lead->utm_medium = $lead->utm_medium ?? 'cpc';
                $lead->save();
                return $lead;
            }

            if (is_array($lead)) {
                $lead['utm_campaign'] = $campaignName;
                $lead['utm_source'] = $lead['utm_source'] ?? 'google';
                $lead['utm_medium'] = $lead['utm_medium'] ?? 'cpc';
                return $lead;
            }
        }

        return null;
    }

    private function getCampaignName($campaignId)
    {
        if ($this->campaignData) {
            return $this->campaignData[$campaignId] ?? null;
        }

        $result = GoogleCampaign::select('campaign')
            ->where('campaign_id', $campaignId)
            ->first();
        return $result->campaign ?? null;
    }
}
