<?php

namespace App\Services;

use App\Models\Customer;
use App\Models\Action;

class CustomerService
{
    private $action;
    private $columns = ['sso_id', 'phone', 'email', 'token', 'ip', 'medium', 'agent', 'name', 'state', 'city', 'address', 'gdpr', ];
    private $update = []; //the update/create value set

    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    public function store()
    {
        $data = $this->buildUpdateSet();

        $customer = false;
        if (isset($data['phone']) && isset($data['sso_id'])) {
            $customer = Customer::where('phone', $data['phone'])
                ->where('sso_id', $data['sso_id'])
                ->first();
        }

        if (!$customer && isset($data['phone'])) {
            $customer = Customer::where('phone', $data['phone'])->first();
        }

        if (!$customer && isset($data['sso_id'])) {
            $customer = Customer::where('sso_id', $data['sso_id'])->first();
        }

        if (!$customer && isset($data['email'])) {
            $customer = Customer::where('email', $data['email'])->first();
        }

        if (!$customer && isset($data['token'])) {
            $customer = Customer::where('token', $data['token'])->first();
        }

        if (!$customer && isset($data['ip'])) {
            $customer = Customer::where('ip', $data['ip'])->first();
        }

        if ($customer) {
            $customer = Customer::updateOrCreate(
                ['id' => $customer->id],
                $data
            );
        } else {
            $customer = Customer::create($data);
        }

        // add form to forms field
        $forms = $customer->forms ? explode(",", $customer->forms) : [];
        $forms[] = $this->action->form;
        $customer->forms = implode(",", array_unique($forms));
        $customer->save();

        return $customer;
    }

    protected function buildUpdateSet()
    {
        foreach ($this->columns as $col) {
            if (is_null($this->action->$col)) {
                continue;
            }
            $this->update[$col] = $this->action->$col;
        }
        return $this->update;
    }
}
