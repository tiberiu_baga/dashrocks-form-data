<?php
declare(strict_types=1);

namespace App\Services;

use Google\Client;
use Google\Service\Sheets;

class GoogleSheets
{
    protected const CREDENTIAL_FILE = 'orange-195015-d31071c6b263.json';
    protected $spreadsheetId;
    protected $client;
    protected $sheets;

    public function __construct(string $spreadsheetId)
    {
        $this->getClient();
        $this->spreadsheetId = $spreadsheetId;
        $this->sheets = new Sheets($this->client);
    }

    protected function getClient()
    {
        $this->client = new Client();
        $this->client->setApplicationName('CBox');
        $this->client->setScopes([Sheets::SPREADSHEETS]);
        $this->client->setAccessType('offline');
        $this->client->setAuthConfig(base_path() . DIRECTORY_SEPARATOR . self::CREDENTIAL_FILE);
    }

    public function readSheetValues($range): array
    {
        $response = $this->sheets->spreadsheets_values->get($this->spreadsheetId, $range);
        return $response->getValues();
    }
}
