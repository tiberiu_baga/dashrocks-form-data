<?php

namespace App\Services;

use App\Models\Action;

class OrangeTelesalesCrm implements CrmInterface
{
    use PhonePerFormDayLimiter;

    private $action;

    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    public function upload()
    {
        $data = $this->buildData();

        if ($this->leadWithInvalidPhone($this->action)) {
            $this->action->setCrmResponse(400, "Not sent. Phone number invalid." , 'oro-tkr');

            \Log::info('data NOT sent to OrangeTkrCRM \n Reason : Phone number invalid. Data:' . print_r($data, 1));
            return true;
        }

        if ($this->leadAlreadyRegisteredToday($this->action)) {
            $this->action->setCrmResponse(400, "Not sent. Phone number already registered for the form today.", 'oro-tel');

            \Log::info('data NOT sent to OrangeTelesalesCrm \n Reason : Phone number already registered for the form today. Data:' . print_r($data, 1));
            return true;
        }

        $telesalesUrl = config('app.telesales.url');

        $isTestUrl = preg_match('/^https:\/\/[0-9.:]*/', $telesalesUrl) === 1;

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => config('app.telesales.url'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_USERPWD => config('app.telesales.user') . ':' . config('app.telesales.pwd'),
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
            ],
            CURLOPT_SSL_VERIFYHOST => $isTestUrl ? 0 : 2,
            CURLOPT_SSL_VERIFYPEER => !$isTestUrl,
        ]);
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        \Log::info('data sent to OrangeTelesalesCrm \n Sent data : ' . print_r($data, 1) . '\n Received response: ' . print_r($response, 1) . " with status code: " . print_r($statusCode, 1));

        $this->action->setCrmResponse($statusCode, $response, 'oro-tel');

        return true;
    }

    protected function buildData(): array
    {
        return [[
            "phone" => $this->action->phone
        ]];
    }

}
