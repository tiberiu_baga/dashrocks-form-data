<?php

namespace App\Services;

class LeadMapper
{
    public static $leadMap = [
        'love-contact-forms' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'love-contact-forms',
        ],
        'leads-contact-hipto' => [
            'campaignCode' => 'Fiber',
            'leadSource' => 'leads-contact-hipto',
        ],
        'leads-facebook-eshop-online-voice' => [
            'campaignCode' => 'Mobile Voice',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Voce Facebook',
        ],
        'leads-facebook-voce' => [
            'campaignCode' => 'Mobile Voice',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Voce Facebook',
        ],
        'leads-facebook-leads-voce' => [
            'campaignCode' => 'Mobile Voice',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Voce Facebook',
        ],
        'leads-googleads-eshop-online-voice' => [
            'campaignCode' => 'Mobile Voice',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Voce GoogleAds',
        ],
        'leads-googleads-voce' => [
            'campaignCode' => 'Mobile Voice',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Voce GoogleAds',
        ],
        'leads-googleads-voice' => [
            'campaignCode' => 'Mobile Voice',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Voce GoogleAds',
        ],
        'leads-tiktok-eshop-online-voice' => [
            'campaignCode' => 'Mobile Voice',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Voce TikTok',
        ],
        'leads-facebook-love' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Love ARGO Facebook',
        ],
        'leads-facebook-leads' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Love ARGO Facebook',
        ],
        'leads-googleads-love' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Love ARGO GoogleAds',
        ],
        'leads-googleads-leads' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Love ARGO GoogleAds',
        ],
        'leads-google-leads-fix' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Love ARGO GoogleAds',
        ],
        'leads-tiktok' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Love ARGO TikTok',
        ],
        'leads-tiktok-leads-fix' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Love ARGO TikTok',
        ],
        'leads-tiktok-love' => [
            'campaignCode' => 'Fiber',
            // 'contactName' => 'prospect oro',
            'leadSource' => 'Native Love ARGO TikTok',
        ],

        'leads-facebook' => [
            'campaignCode' => 'vhbb',
            'contactName' => false,
            'leadSource' => 'Native Love VHBB Facebook'
        ],

        'leads-googleads' => [
            'campaignCode' => 'vhbb',
            'contactName' => false,
            'leadSource' => 'Native Love VHBB GoogleAds'
        ],

        'leads-googleads' => [
            'campaignCode' => 'vhbb',
            'contactName' => false,
            'leadSource' => 'Native Love VHBB GoogleAds'
        ],

        'leads-googleads-opera' => [
            'campaignCode' => 'Fiber',
            'contactName' => false,
            'leadSource' => 'Native Opera Facebook'
        ],

        'leads-facebook-opera' => [
            'campaignCode' => 'Fiber',
            'contactName' => false,
            'leadSource' => 'Native Opera GoogleAds'
        ],

        'leads-tiktok-opera' => [
            'campaignCode' => 'Fiber',
            'contactName' => false,
            'leadSource' => 'Native Opera TikTok'
        ],

        'verifica-acoperirea' => [
            'campaignCode' => 'Fiber',
            'contactName' => "no contact",
            'leadSource' => "verifica-acoperirea"
        ],

        'leads-leads-googleads-soho-love' => [
            'campaignCode' => 'Fiber',
            'contactName' => false,
            'leadSource' => 'googleads-soho'
        ],

        'leads-googleads-soho-love' => [
            'campaignCode' => 'Fiber',
            'contactName' => false,
            'leadSource' => 'googleads-soho'
        ],

        'leads-leads-facebook-soho-love' => [
            'campaignCode' => 'Fiber',
            'contactName' => false,
            'leadSource' => 'facebook-soho'
        ],
        'leads-facebook-soho-love' => [
            'campaignCode' => 'Fiber',
            'contactName' => false,
            'leadSource' => 'facebook-soho'
        ],
        'leads-facebook-leads-soho' => [
            'campaignCode' => 'Fiber',
            'contactName' => false,
            'leadSource' => 'facebook-soho'
        ],
    ];
}
