<?php

namespace App\Services;

use App\Services\OrangeCrm;
use App\Services\OrangeTkrCrm;
use App\Services\DefaultCrm;
use App\Models\Action;
use Carbon\Carbon;


class CrmManager
{
    private $action;

    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    public function make(): CrmInterface
    {
        switch ($this->getNextCrmType()) {
            case 'oro-sfa':
                $service = new OrangeCrm($this->action);
                break;
            case 'oro-tkr':
                $service = new OrangeTkrCrm($this->action);
                break;
            case 'oro-tel':
                $service = new OrangeTelesalesCrm($this->action);
                break;
            case 'oro-repo':
                $service = new OrangeRepoCrm($this->action);
                break;
            default:
                $service = new DefaultCrm($this->action);
                break;
        }

        return $service;
    }

    protected function getBasicForm()
    {
        if (strpos($this->action->page, 'orange.ro/reinnoire-abonament') !== false) {
            return 'do-not-send';
        }

        if (
            strpos($this->action->page, '/internet/abonament-home-net-fibra') !== false
            || strpos($this->action->page, '/tv/abonament-tv-satelit') !== false
            || strpos($this->action->page, '/tv/abonament-tv-cablu') !== false
            || strpos($this->action->page, '/abonamente/orange-net-4G') !== false
            || strpos($this->action->page, '/abonamente/orange-net-4g') !== false
            || getHostAndStripSlash($this->action->page) == "www.orange.ro/tv"
            || strpos($this->action->page, '/abonamente/net') !== false
            || getHostAndStripSlash($this->action->page) == 'www.orange.ro/internet'
            || strpos($this->action->page, '/abonamente/voce-fixa') !== false
            || strpos($this->action->page, 'internet-mobil.ro') !== false
            || strpos($this->action->page, '/modificare-abonament') !== false
            || strpos($this->action->page, '/promotii-voce') !== false
        ) {
            return 'oro-sfa';
        }


        return $this->action->form;
    }

    protected function getNextCrmType()
    {
        switch ($this->getBasicForm()) {
            case 'leads-googleads':
            case 'leads-facebook':
            case 'verifica-acoperirea':
            case 'leads-leads-googleads-soho-love':
            case 'leads-googleads-soho-love':
            case 'leads-leads-facebook-soho-love':
            case 'leads-facebook-soho-love':
            case 'leads-facebook-leads-soho'://New endpoint for the tow above
            case 'leads-contact-hipto':
            case 'leads-googleads-internet-acasa-ro':
            case 'leads-google-internet-acasa':
            case 'leads-facebook-internet-acasa-ro':
            case 'leads-facebook-internet-acasa':
            case 'myaccount-reshape':
            case 'myaccount-reshape-invoice-cronos':
            case 'myaccount-reshape-packages-and-options':
            case 'oro-sfa':
            //20241106
            case 'leads-googleads-love':
            case 'leads-googleads-leads':
            case 'leads-facebook-love':
            case 'leads-facebook-leads':
            case 'leads-google-internet-mobil':
            case 'leads-facebook-internet-mobil':
            case 'leads-google-leads-fix':
            case 'leads-facebook-eshop-online-voice':
            case 'leads-facebook-voce':
            case 'leads-facebook-leads-voce': //New endpoint for leads-facebook-eshop-online-voice
            case 'leads-googleads-eshop-online-voice':
            case 'leads-googleads-voice':
            case 'leads-googleads-opera':
            case 'leads-facebook-opera':
            case 'leads-tiktok-opera':
            case 'leads-yoxo-app':
            case 'leads-googleads-portare':
            case 'leads-google-leads-portare':
            case 'leads-facebook-portare':
            case 'leads-tiktok-portare':
            case 'leads-tiktok':
            case 'leads-tiktok-leads-fix':
            case 'leads-googleads-voce':
            case 'leads-facebook-voice':
                $crmType = 'oro-sfa';
                break;
            case 'magazin-online-adaugat-la-cos-scanare':
            case 'love-contact-forms':
                $crmType = 'oro-sfa';
                break;
            case 'credit-card-call-back':
                $crmType = 'oro-tel';
                break;
            case 'leads-googleads-newsletter':
            case 'leads-google-eshop-newsletter':
            case 'leads-facebook-newsletter':
                $crmType = 'oro-repo';
                break;
            default:
                $crmType = 'do-not-send';
                break;
        }


        if (
            strpos($this->action->page, 'love/local') !== false ||
            strpos($this->action->page, 'love/entry') !== false ||
            strpos($this->action->page, 'love/extra') !== false ||
            strpos($this->action->page, 'love/premiere') !== false ||
            strpos($this->action->page, 'love/satelit') !== false ||
            strpos($this->action->page, 'love/supreme') !== false ||
            strpos($this->action->page, 'love/oferta-portare') !== false ||
            strpos($this->action->page, 'orange.ro/love/client-') !== false ||
            strpos($this->action->page, 'orange.ro/oferta-internet-exclusiv-online') !== false ||
            strpos($this->action->page, 'orange.ro/love/') !== false ||
            getHostAndStripSlash($this->action->page) == "www.orange.ro/love"  ||
            (getHostAndStripSlash($this->action->page) == "www.orange.ro" && $crmType !== 'do-not-send')  // route homepage leads to oro-sfa
        ) {
            $crmType = 'oro-sfa';
        }

        // $prevType = $this->getPreviousLeadDestination();
        // if (!in_array($crmType, ['do-not-send', 'oro-tel', 'oro-repo']) && $prevType) {
        //     $crmType = $prevType;
        // }

        // Always route hungarian pages to oro-sfa
        if (strpos($this->action->page, 'orange.ro/love/produse-recomandate-hu') !== false
            || strpos($this->action->page, 'orange.ro/love/oferta-locala-hu') !== false
            // || $this->action->form == 'verifica-acoperirea'
            // || in_array($this->action->form, ['myaccount-reshape', 'myaccount-reshape-invoice-cronos', 'myaccount-reshape-packages-and-options'])
        ) {
            $crmType = 'oro-sfa';
        }

        return $crmType;
    }

    protected function getPreviousLeadDestination()
    {
        if($this->action->phone == '0740000000') {
            // do not route test number
            return false;
        }
        if (strpos($this->action->page, 'orange.ro/apeluri-inbound') !== false ||
            strpos($this->action->page, 'orange.ro/apeluri-liveshop') !== false ||
            $this->action->form === 'leads-contact-hipto') {
            // do not re-route /apeluri-inbound and HipTo leads
            return false;
        }
        $prev = Action::where('created_at', '>=', Carbon::now()->startOfDay()->subDays(30))
            ->where(function ($query) {
                $query->where(function ($q) {
                    $q->where('crm_send', 1)
                        ->whereIn('crm_type', ['oro-tkr', 'oro-sfa', 'verifica-acoperirea']);
                })
                    ->orWhere(function ($q) {
                        $q->where('crm_send', 0)
                            ->whereIn('form', ['verifica-acoperirea-servicii']);
                    });
            })
            ->where('phone', $this->action->phone)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($prev && !$prev->crm_type && $prev->form == 'verifica-acoperirea-servicii') {
            $prev->crm_type = 'oro-sfa';
        }

        return $prev ? $prev->crm_type : false;
    }
}
