<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Action;
use Carbon\Carbon;
use Illuminate\Support\Str;

trait PhonePerFormDayLimiter
{
    protected $unrestrictedUrls = [
        '/apeluri-inbound',
        '/apeluri-liveshop'
    ];

    protected function leadAlreadyRegisteredToday(Action $action): bool
    {
        if (null !== $action->phone && $this->shouldApplyRestriction($action)) {
            $phoneFormCount = Action::where('form', $action->form)
                ->where('page', $action->page)
                ->where('crm_send', 1)
                ->where('created_at', '>=', Carbon::now()->startOfDay()->toDateTimeString())
                ->where('created_at', '<', Carbon::now()->endOfDay()->toDateTimeString())
                ->where('phone', $action->phone)
                ->count();
            return $phoneFormCount > 0;
        }

        return false;
    }

    protected function leadWithInvalidPhone(Action $action): bool
    {
        if(is_null($action->phone)) {
            return true; // Invalid
        }
        $phoneNumber = str_replace([" ", "-"], "", $action->phone);
        // Regular expression to match phone number format
        $pattern = "/^\\+?[0-9][0-9]{7,14}$/";
        // Check if phone number matches the pattern
        if (preg_match($pattern, $phoneNumber)) {
            return false; // Not invalid
        }
        return true; // Invalid
    }

    protected function shouldApplyRestriction(Action $action): bool
    {
        return !Str::contains($action->page, $this->unrestrictedUrls);
    }

}
