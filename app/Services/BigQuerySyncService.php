<?php

namespace App\Services;

use App\Services\BigQuery\LoadOperation;
use App\Services\BigQuery\SaveOperationInterface;
use App\Services\BigQuery\SyncConfig;
use App\Services\BigQuery\UpdateOperation;
use Carbon\Carbon;
use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Core\Exception\NotFoundException;
use Google\Cloud\Core\ExponentialBackoff;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class BigQuerySyncService
{

    private $operations = [];

    public function __construct()
    {
    }

    public function syncTableToBigQuery(SyncConfig $syncConfig): void
    {
        $operation = new LoadOperation($syncConfig);

        $operationResult = [
            'inserted' => 0,
            'updated' => 0,
        ];

        if ($syncConfig->incrementalSync) {
            $operation->setAppendMode();
            [$lastSyncedId,  $lastSyncedUpdatedAt] = $this->getLastSyncedInfoFromBigQuery($syncConfig);

            $operationResult['updated'] = $this->saveExistingUpdatedRows($syncConfig, $operation, $lastSyncedId, $lastSyncedUpdatedAt);
            $operationResult['inserted'] = $this->saveNewRows($syncConfig, $operation, $lastSyncedId);
        } else {
            $operationResult['inserted'] = $this->saveAllRows($syncConfig, $operation);
        }

        $this->operations[$syncConfig->toTable] = $operation;

        $this->commit();

        $this->saveOperationResult($syncConfig, $operationResult);

    }

    protected function getLastSyncedInfoFromBigQuery(SyncConfig $syncConfig): array
    {
        $fullTableName = $syncConfig->projectId . '.' . $syncConfig->datasetId . '.' . $syncConfig->toTable;

        $bigQuery = new BigQueryClient([
            'projectId' => $syncConfig->projectId,
            'keyFilePath' => $syncConfig->bigQueryKeyFilePath
        ]);

        $queryJobConfig = $bigQuery->query("
            SELECT id, updated_at
            FROM `{$fullTableName}`
            ORDER BY id DESC
            LIMIT 1
        ");

        try {
            $queryResults = $bigQuery->runQuery($queryJobConfig);
        } catch (NotFoundException $exc) {
            $queryResults = [];
        }

        $lastSyncedId = 0;
        $lastSyncedUpdatedAt = new \DateTime('1970-01-01 00:00:00');
        foreach ($queryResults as $row) {
            $lastSyncedId = $row['id'];
            $lastSyncedUpdatedAt = $row['updated_at'];
            break;
        }

        return [$lastSyncedId, $lastSyncedUpdatedAt];
    }

    protected function saveExistingUpdatedRows(SyncConfig $syncConfig, LoadOperation $operation, $lastSyncedId, $lastSyncedUpdatedAt): int
    {
        $updatedIds = [];
        $this->fullSyncSelect($syncConfig)
            ->where('id', '<', $lastSyncedId)
            ->where('updated_at', '>', $lastSyncedUpdatedAt)
            ->chunk(5000, function ($rows) use ($operation, &$updatedIds) {
                foreach ($rows as $row) {
                    $operation->save(['payload' => $row->payload]);
                    $updatedIds[] = $row->id;
                }
            });
        if (count($updatedIds) > 0) {
            $this->deleteUpdatedRowsFromBigQuery($syncConfig, $updatedIds);
        }
        return count($updatedIds);
    }

    protected function saveNewRows(SyncConfig $syncConfig, LoadOperation $operation, $lastSyncedId): int
    {
        $inserted = 0;
        $this->fullSyncSelect($syncConfig)
            ->where('id', '>', $lastSyncedId)
            ->chunk(5000, function ($rows) use ($operation, &$inserted) {
                foreach ($rows as $row) {
                    $operation->save(['payload' => $row->payload]);
                    $inserted++;
                }
            });
        return $inserted;
    }


    protected function saveAllRows(SyncConfig $syncConfig, LoadOperation $operation): int
    {
        $inserted = 0;
        $this->fullSyncSelect($syncConfig)
            ->chunk(5000, function ($rows) use ($operation, &$inserted) {
                foreach ($rows as $row) {
                    $operation->save(['payload' => $row->payload]);
                    $inserted++;
                }
            });
        return $inserted;
    }

    protected function fullSyncSelect(SyncConfig $syncConfig): Builder
    {
        $jsonStructure = implode(', ', array_map(static function ($columnName) {
            return "'$columnName', $columnName";
        }, array_keys($syncConfig->getColumns())));

        $query = DB::connection($syncConfig->fromConnection)->table($syncConfig->fromTable)
            ->select(DB::raw("
            id,
            JSON_OBJECT(
                " . $jsonStructure . "
                ) AS payload
            "))
            ->orderBy('id', 'asc');

        if ($syncConfig->fromSelectClosure instanceof \Closure) {
            $query = ($syncConfig->fromSelectClosure)($query);
        }

        return $query;
    }

    public function saveModelToBigQuery(Model $model, $table = null): void
    {
        // TODO: test and finish this method
        return;
        $table = $table ?? $model->getTable();

        if ($model->wasRecentlyCreated) {
            $operationName = $table . '_load';
            $operationClass = LoadOperation::class;
        } else {
            $operationName = $table . '_update';
            $operationClass = UpdateOperation::class;
        }

        if (!isset($this->operations[$operationName])) {
            $tableColumns = $this->getColumns($table);
            $this->operations[$operationName] = $this->newOperation($operationClass, $table, $tableColumns);
        }

        $operation = $this->operations[$operationName];

        $operation->save($this->getModelData($model));
    }

    protected function getModelData(Model $model): array
    {
        return $model->toArray();
    }

    protected function newOperation($operationClass, $table, $tableColumns): SaveOperationInterface
    {
        return new $operationClass($table, $tableColumns);
    }

    public function commit(): void
    {
        foreach ($this->operations as $operation) {
            $operation->commit();
        }
    }

    private function saveOperationResult(SyncConfig $syncConfig, array $operationResult): void
    {
        $columns = [
            'table' => [
                'name' => 'table',
                'type' => 'STRING',
            ],
            'inserted' => [
                'name' => 'inserted',
                'type' => 'INTEGER',
            ],
            'updated' => [
                'name' => 'updated',
                'type' => 'INTEGER',
            ],
            'incremental_sync' => [
                'name' => 'incremental_sync',
                'type' => 'BOOL',
            ],
            'synced_at' => [
                'name' => 'synced_at',
                'type' => 'DATETIME',
            ]
        ];

        $syncConfig->setColumns($columns);
        $syncedTable = $syncConfig->toTable;
        $syncConfig->toTable = 'sync_activity';

        $operation = new LoadOperation($syncConfig, true);
        $row = [
            'table' => $syncedTable,
            'inserted' => $operationResult['inserted'],
            'updated' => $operationResult['updated'],
            'incremental_sync' => $syncConfig->incrementalSync,
            'synced_at' => Carbon::now()->format('Y-m-d H:i:s')
        ];
        $operation->save($row);
        $operation->commit();
    }

    private function deleteUpdatedRowsFromBigQuery(SyncConfig $syncConfig, array $updatedIds)
    {
        $fullTableName = $syncConfig->projectId . '.' . $syncConfig->datasetId . '.' . $syncConfig->toTable;

        $bigQuery = new BigQueryClient([
            'projectId' => $syncConfig->projectId,
            'keyFilePath' => $syncConfig->bigQueryKeyFilePath
        ]);

        $queryJobConfig = $bigQuery->query("
            DELETE FROM `{$fullTableName}`
            WHERE id IN (" . implode(',', $updatedIds) . ")
        ");

        try {
            $bigQuery->runQuery($queryJobConfig);
        } catch (NotFoundException $exc) {
            // do nothing
        }
    }

}
