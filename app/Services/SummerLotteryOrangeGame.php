<?php

namespace App\Services;

use App\Models\OrangeGame;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class SummerLotteryOrangeGame
{
    private $type = 'summer-lotery';
    private $rejectedOrderStatuses = ['canceled', 'rejected', 'rejected_by_vantive'];
    private $orderDateStart = '2022-07-01';
    /**
     * @var OrangeGame
     */
    public $game;
    public $orderId;
    public $code;


    function __construct($orderId = null, $code = null)
    {
        $this->orderId = $orderId;
        $this->code = $code;

        if ($orderId) {
            $this->game = OrangeGame::where('order_id', $orderId)->first();
        }

        if ($code) {
            $this->game = OrangeGame::where('code', $code)->first();
            if (null === $this->game) {
                $this->game = OrangeGame::whereHas('prize', function (Builder $query) use ($code) {
                    $query->where('voucher_code', '=', $code);
                })->first();
            }
        }
    }

    public function alreadyRegistered()
    {
        return ($this->game && $this->game->code);
    }

    public function register()
    {
        if ($this->alreadyRegistered()) {
            return [
                "error" => true,
                "message" => "Order already registered"
            ];
        }

        $this->game = OrangeGame::create([
            'type' => $this->type,
            'order_id' => $this->orderId,
            'code' => $this->generateCode(),
            'status' => 'code'
        ]);

        $transaction = $this->getOrderDetails();
        if (!$transaction) {
            $this->game->setAsNotWinner();
            return $this->errorResponse("No transaction found");
        }

        // save transaction details
        $this->game->order_date = $transaction->date;
        $this->game->order_status = $transaction->status;
        $this->game->order_items = $transaction->items;
        $this->game->save();

        // check order statuses
        if (in_array($this->game->order_status, $this->rejectedOrderStatuses)) {
            $this->game->setAsNotWinner();
            return $this->errorResponse("Rejected order status : '{$this->game->order_status}'");
        }

        // check orderDate
        if (Carbon::parse($this->game->order_date)->lt(Carbon::parse($this->orderDateStart))) {
            $this->game->setAsNotWinner();
            return $this->errorResponse("OrderDate {$this->game->order_date} is earlier than campaign start {$this->orderDateStart}");
        }

        // check eligible prize level
        $prizeLevels = [];
        foreach ($this->game->order_items as $item) {
            $prizeLevels[] = $item['type'];
        }

        if (!$this->hasEligiblePrizeLevel($prizeLevels)) {
            $this->game->setAsNotWinner();
            return $this->errorResponse("Order does not contain fidelity or loyalty or acquisition items.");
        }

        return [
            "success" => true,
            "error" => false,
            "message" => "Registered",
            "code" => $this->getCode()
        ];
    }

    public function getCode()
    {
        if ($this->game && $this->game->code) {
            return $this->game->code;
        }

        return $this->code;
    }

    protected function generateCode()
    {
        $code = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2)
            . substr(str_shuffle("0123456789" . $this->orderId), 0, 4)
            . substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ" . $this->orderId), 0, 2)
            . substr(str_shuffle("0123456789" . $this->orderId), 0, 2);

        if ($this->codeExists($code)) {
            $code = $this->generateCode();
        }

        return $code;
    }

    public function alreadyPlayed()
    {
        return ($this->alreadyRegistered() &&
            in_array($this->game->status, ['winner', 'not-winner'])
        );
    }

    protected function codeExists($code)
    {
        return OrangeGame::where('code', $code)->count();
    }

    protected function getOrderDetails()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://www.orange.ro/webshopportlets/api/order/details/' . $this->game->order_id,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Basic d2Vic2hvcDpEYzFQZDBUcnNv',
            'Cookie: BIGipServer~DEVELOPMENT_IT~webshop_wl12_predeploy_pool=rd51o00000000000000000000ffffac107e43o9080'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $order = null;
        if ($response) {
            $order = json_decode($response);
        }

        return $order;
    }

    public function play($phone)
    {

        // save phone
        $this->game->phone = $phone;
        $this->game->save();

        // spin the wheel
        if (!$this->game->availablePrizes->count()) {
            return $this->successResponse(false, "No prizes have remained");
        }

        $prize = $this->game->availablePrizes->random();
        return $this->successResponse(true, "Winner", $prize);

    }


    public function notifyCode($email)
    {
        $actito = new ActitoService('', '');
        $fields = '[{"key":"code", "values":["' . $this->game->code . '"]}]';

        $response = $actito->createProfileIfNotExists($email);
        // Send To Actito
        $response = $actito->triggerEmail(
            config('services.actito.summerLotteryCodeCampaignId'),
            $email,
            $fields);

        return $this->saveGameNotifyToExtras($email, $response, 'code');
    }

    public function notifyPrize($email)
    {
        $actito = new ActitoService('', '');

        $prize = $this->game->prize;
        if (null === $prize) {
            return $this->errorResponse("No prize was found.");
        }

        $fields = '[{"key":"code", "values":["' . $this->game->code . '"]},'
            . '{"key":"voucher_code", "values":["' . $prize->voucher_code . '"]},'
            . '{"key":"voucher_pin", "values":["' . $prize->voucher_pin . '"]},'
            . '{"key":"voucher_value", "values":["' . $prize->voucher_value . '"]}'
            . ']';

        $response = $actito->createProfileIfNotExists($email);

        switch ($prize->voucher_type) {
            case 'Carrefour':
                $campaignId = 'summerLotteryPrizeCampaignCarrefourId';
                break;
            case 'Decathlon':
                $campaignId = 'summerLotteryPrizeCampaignDecathlonId';
                break;
            default:
                $campaignId = 'summerLotteryPrizeCampaignShopId';
        }

        // Send To Actito
        $response = $actito->triggerEmail(
            config('services.actito.' . $campaignId),
            $email,
            $fields);

        return $this->saveGameNotifyToExtras($email, $response, 'prize');
    }

    protected function saveGameError($error)
    {
        $errors = $this->game->errors;
        $errors[Carbon::now()->toDateTimeString()] = $error;
        $this->game->errors = $errors;
        $this->game->save();
    }

    public function saveGameNotifyToExtras($email, $response, $type)
    {
        if (!$response || $response['status'] != 200) {
            $details = isset($response['response']->message)
                ? substr($response['response']->message, 0, strpos($response['response']->message, ' in path'))
                : $response['response'];
            $message = "Failed to send {$type} : $details";
            $this->saveGameError($message);

            return $this->errorResponse($message);
        }

        $extra = [
            'action' => "Sent $type email",
            'value' => $email,
            'response' => $response['response'],
        ];

        $extras = $this->game->extras;
        $extras[Carbon::now()->toDateTimeString()] = $extra;
        $this->game->extras = $extras;
        $this->game->save();

        return [
            "success" => true,
            "error" => false,
            "status" => $this->game->status,
            "message" => "Message with $type sent",
        ];
    }

    protected function errorResponse($message)
    {
        $this->saveGameError($message);
        return [
            "error" => true,
            "message" => $message
        ];
    }

    protected function successResponse($won = false, $message, $prize = false)
    {
        if ($won) {
            $prize->setAsUnavailable();
            $this->game->setAsWinner();
            $this->game->prize_id = $prize->id;
            $this->game->save();
        } else {
            $this->game->setAsNotWinner();
            $this->saveGameError($message);
        }
        return [
            "success" => true,
            "error" => false,
            "status" => $this->game->status,
            "prize" => $this->responseFormattedPrize($prize),
            "message" => $message
        ];
    }

    public function responseFormattedPrize($prize)
    {
        $prizeInfo = $prize ? $prize->toArray() : null;

        if (isset($prizeInfo['voucher_type']) && strpos($prizeInfo['voucher_type'], 'ORO') !== false) {
            $prizeInfo['voucher_type'] = 'magazinul online Orange';
        }

        return $prizeInfo;
    }

    /**
     * @param array $prizeLevels
     * @return bool
     */
    private function hasEligiblePrizeLevel(array $prizeLevels): bool
    {
        $eligibleLevels = [
            'loyalty6',
            'loyalty12',
            'loyalty24',
            'acquisition6',
            'acquisition12',
            'acquisition24',
            'fidelity6m',
            'fidelity12m',
            'fidelity24m',
        ];

        return count(array_intersect($eligibleLevels, $prizeLevels)) > 0;
    }
}
