<?php

namespace App\Services;
use App\Models\Action;

interface CrmInterface
{
     public function __construct(Action $action);

     public function upload();
}
