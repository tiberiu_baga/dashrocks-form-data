<?php

namespace App\Services;

use App\Models\Action;
use Carbon\Carbon;

class OrangeCrm implements CrmInterface
{
    use PhonePerFormDayLimiter;

    private $action;
    private $columns = ['sso_id', 'phone', 'email', 'token', 'ip', 'medium', 'agent', 'name', 'state', 'city', 'address', 'gdpr'];
    // private $url = "https://www.orange.ro/argo/eligibility/request-contact";
    private $url;

    private $urlStates = "https://www.orange.ro/argo/data/counties.json";
    private $urlAddress = "https://www.orange.ro/argo/eligibility/";

    private $urlStreets = "https://www.orange.ro/argo/eligibility/get-streets?cityId=";

    private $urlStreetNrs = " https://www.orange.ro/argo/eligibility/get-streetNumbers?countyCode=403&cityId=179141&streetId=3U850HK";
    private $urlStaircase = "https://www.orange.ro/argo/eligibility/get-staircases?countyCode=403&cityId=179141&streetId=3U850HK&streetNr=24-26&";
    private $urlBuilding = "https://www.orange.ro/argo/eligibility/get-staircases?countyCode=403&cityId=179141&streetId=3U850HK&streetNr=47-53&building=EUROPA%20HOUSE";

    private $leadMap;

    private $campaignCode = 'Fiber';
    private $leadSource;
    private $contactName;
    private $retried = false;

    public function __construct(Action $action)
    {
        $this->leadMap = LeadMapper::$leadMap;
        $this->action = $action;
        $this->url = config('app.crm_url');
        $this->setCampaignCode();
        $this->setLeadSource();
        $this->setContactName();
    }

    protected function setCampaignCode()
    {
        if (isset($this->leadMap[$this->action->form])) {
            $this->campaignCode = $this->leadMap[$this->action->form]['campaignCode'];
        }

        if ( strpos($this->action->page, 'internet-mobil.ro') !== false
            || strpos($this->action->page, '/modificare-abonament') !== false
            || strpos($this->action->page, '/abonament') !== false
            || strpos($this->action->page, '/oferte-speciale') !== false
            || strpos($this->action->page, '/promotii-voce') !== false
            || strpos($this->action->page, '/oferta-voce') !== false
            || strpos($this->action->page, '/magazin-online/adaugat-la-cos/scanare') !== false
            || strpos($this->action->page, '/apeluri-inbound') !== false
            || strpos($this->action->page, '/cs-chat') !== false
            || strpos($this->action->page, '/inregistrareformular') !== false
            || getHostAndStripSlash($this->action->page) == "www.orange.ro"
        ) {
            $this->campaignCode = 'Mobile Voice';
        }

        // if ( strpos($this->action->page, 'orange.ro/internet') !== false
        // ) {
        //     $this->campaignCode = 'Fiber';
        // }

        if( strpos($this->action->page, 'orange.ro/love/produse-recomandate-hu') !== false
            || strpos($this->action->page, 'orange.ro/love/oferta-locala-hu') !== false
        ) {
            $this->campaignCode = 'PREPAY_TO_VHBB';
        }
    }

    protected function setLeadSource()
    {
        if (isset($this->leadMap[$this->action->form])) {
            $source = $this->leadMap[$this->action->form]['leadSource'];
            if($source === 'url-noparams') {
                $this->leadSource = $this->getUrlWithNoParams($this->action->page);
            } else if($source === 'utms') {
                $this->leadSource = stripos($this->action->form, 'facebook') !== false ?
                    'm:[social]s:[facebook]' :
                    'm:[paid]s:[adwords]';
            } else {
                $this->leadSource = $source;
            }

            if ($this->action->form == 'love-contact-forms') {
                if (getHostAndStripSlash($this->action->page) == "www.orange.ro") {
                    $this->leadSource = 'love-contact-forms-homepage';
                } else if (getHostAndStripSlash($this->action->page) == "www.orange.ro/love") {
                    $this->leadSource = 'love-contact-forms-love';
                } else if (getHostAndStripSlash($this->action->page) == "www.orange.ro/love/local") {
                    $this->leadSource = 'love-contact-forms-lovelocal';
                } else if (strpos($this->action->page, 'orange.ro/love/client-') !== false) {
                    $this->leadSource = 'detalii-pachet-love';
                } else if (strpos($this->action->page, 'orange.ro/love/local/client-') !== false) {
                    $this->leadSource = 'detalii-pachet-lovelocal';
                } else if (strpos($this->action->page, 'orange.ro/love/exclusiv-online') !== false) {
                    $this->leadSource = 'love-exclusiv-online';
                } else if (strpos($this->action->page, 'orange.ro/love/oferta-vara') !== false) {
                    $this->leadSource = 'love-summer-offer';
                } else if (strpos($this->action->page, 'orange.ro/oferta-internet-exclusiv-online') !== false) {
                    $this->leadSource = 'oferta-internet-exclusiv-online';
                } else if( strpos($this->action->page, 'orange.ro/love/produse-recomandate-hu') !== false
                    || strpos($this->action->page, 'orange.ro/love/oferta-locala-hu') !== false ) {
                    $this->leadSource = 'love-contact-forms-maghiara';
                } else if (strpos($this->action->page, '/internet/abonament-home-net-fibra') !== false) {
                    $this->leadSource = 'abonament-home-net-fibra';
                } else if (strpos($this->action->page, '/tv/abonament-tv-satelit') !== false ) {
                    $this->leadSource = 'abonament-tv-satelit';
                } else if (strpos($this->action->page, '/tv/abonament-tv-cablu') !== false) {
                    $this->leadSource = 'abonament-tv-cablu';
                } else if (strpos($this->action->page, '/abonamente/orange-net-4G') !== false
                    || strpos($this->action->page, '/abonamente/orange-net-4g') !== false) {
                    $this->leadSource = 'abonamente-orange-net-4G';
                } else if (getHostAndStripSlash($this->action->page) == "www.orange.ro/tv") {
                    $this->leadSource = 'tv-tv';
                } else if (strpos($this->action->page, '/abonamente/net') !== false) {
                    $this->leadSource = 'abonamente-net';
                } else if (getHostAndStripSlash($this->action->page) == 'www.orange.ro/internet') {
                    $this->leadSource = 'internet-internet';
                } else if (strpos($this->action->page, '/abonamente/voce-fixa') !== false) {
                    $this->leadSource = 'abonamente-voce-fixa';
                } else {
                    $this->leadSource = str_replace(["www.orange.ro/", "www.orange.ro", "/"], ["", "", "-"], getHostAndStripSlash($this->action->page));
                }
            }
        } else {
            $this->leadSource = $this->action->form;
        }
    }

    protected function setContactName()
    {
        if (
            !isset($this->leadMap[$this->action->form])
            || !isset($this->leadMap[$this->action->form]['contactName'])
            || $this->leadMap[$this->action->form]['contactName'] === false
        ) {
            $this->contactName = $this->action->name ?? "prospect oro";
            if(!$this->contactName) $this->contactName = "prospect oro";
        } else {
            $this->contactName = $this->leadMap[$this->action->form]['contactName'];
        }
    }

    public function upload()
    {
        $data = $this->buildData();

        if ($this->leadWithInvalidPhone($this->action)) {
            $this->action->setCrmResponse(400, "Not sent. Phone number invalid." , 'oro-tkr');

            \Log::info('data NOT sent to OrangeTkrCRM \n Reason : Phone number invalid. Data:' . print_r($data, 1));
            return true;
        }

        if ($this->leadAlreadyRegisteredToday($this->action)) {
            $this->action->setCrmResponse(400, "Not sent. Phone number already registered for the form today." , 'oro-sfa');

            \Log::info('data NOT sent to OrangeCRM \n Reason : Phone number already registered for the form today. Data:' . print_r($data, 1));
            return true;
        }

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERPWD => config('app.crm_user') . ':' . config('app.crm_password'),
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
            ],
        ]);
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        // $response = json_encode([
        //     ['defaultMessage' => 'blaaa']
        // ]);
        // $statusCode = 200;

        \Log::info('data sent to OrangeCRM \n Sent data : ' . print_r($data, 1) . '\n Received response: ' . print_r($response, 1) . " with status code: " . print_r($statusCode, 1));

        $this->action->setCrmResponse($statusCode, $response, 'oro-sfa');

        // retry upload if status 424 failed dependency
        if (in_array($statusCode, [424]) && !$this->retried) {
            $this->retried = true;
            return $this->upload();
        }

        return true;
    }

    protected function buildData()
    {
        // Concatenat URL to leadSource if from web
        $this->leadSource .= (stripos($this->action->page, "lead - id") !== false) ?
            "" :
            ' | ' . $this->getUrlWithNoParams($this->action->page);

        $data = [
            'createdDate' => Carbon::parse($this->action->created_at)->format('Y-m-d\TH:i:s\Z'),
            'source' => "ONLINE",
            'campaignCode' => $this->campaignCode,
            'phoneNumber' => $this->action->phone,
            'contactName' => $this->contactName,
            'geographicalAssignation' => "",
            'organizationalAssignation' => "",
            'externalId' => $this->action->id,
            'leadSource' => $this->leadSource,
            // 'orangeAddressId' => "5IU4R",
            "packetItems" => []
        ];
        // $details = array_merge($this->buildDataOld(), (array) json_decode($this->action->post_data));

        $data['details'] = $this->buildDetails($this->buildDataOld());

        return $data;
    }

    protected function buildDataOld()
    {
        $data = [
            'email' => $this->action->email,
            'state' => $this->action->state,
            'city' => $this->action->city,
            'subject' => $this->action->subject,
            'message' => $this->action->message,
            'gdpr' => $this->action->gdpr,
            'address' => $this->getAddress(),
        ];

        // For HipTo add the following fields
        if ($this->action->form === 'leads-contact-hipto') {
            $postData = (array) json_decode($this->action->post_data);
            $data['currentCommitment'] = $postData['currentCommitment'] ?? null;
            $data['currentProvider'] = $postData['currentProvider'] ?? null;
            $data['currentPricePaid'] = $postData['currentPricePaid'] ?? null;
            $data['postCode'] = $postData['postCode'] ?? null;
            $data['eligibleFiber'] = $postData['eligibleFiber'] ?? null;
        }

        return $data;
    }

    public function getAddress()
    {
        $address = [
            'county' => [],
            'city' => [],
            'street' => [],
            'streetNo' => '',
            'blockNo' => '',
            'staircase' => '',
            'apNo' => '',
            'orangeAddressId' => null,
        ];

        if (!$this->action->state) {
            return $address;
        }
        $address['county'] = $this->getCounty();

        if (isset($address['county']['id']) && $this->action->city) {
            $address['city'] = $this->getCity($address['county']['id']);
        }

        if (isset($address['city']['id']) && $this->action->address) {
            $address['street'] = $this->getStreet($address['city']['id']);
        }

        return $address;
    }

    public function getCounty()
    {
        $state = trim(strtolower($this->action->state));
        $content = file_get_contents($this->urlStates);
        $states = json_decode($content);

        foreach ($states as $_state) {
            if (strtolower($_state->name) == $state) {
                return [
                    'id' => $_state->siruta,
                    'name' => $_state->name,
                ];
            }
        }

        return [];
    }

    public function getCity($countryId)
    {
        $city = trim(strtolower($this->action->city));
        $content = file_get_contents($this->urlAddress . 'get-cities?countyCode=' . $countryId);
        $cities = json_decode($content);
        foreach ($cities as $_city) {
            if (strtolower($_city->name) == $city) {
                return [
                    'id' => $_city->siruta,
                    'name' => $_city->name,
                ];
            }
        }

        return [];
    }

    public function getStreet($cityId)
    {
        $street = trim(strtolower($this->action->address));
        $content = file_get_contents($this->urlAddress . 'get-streets?cityId=' . $cityId);
        $streets = json_decode($content);

        foreach ($streets as $_street) {
            if (stripos(strtolower($_street->name), $street) !== false) {
                return [
                    'id' => $_street->streetId,
                    'name' => $_street->name,
                ];
            }
        }

        return [];
    }

    public function buildDetails($data)
    {
        $address = $this->parseAddress($data['address']);
        unset($data['address']);
        $dataToImplode = array_merge($data, $address);

        $result = $this->createArrayToImplode($dataToImplode);

        return implode("|", $result);
    }

    public function createArrayToImplode($array)
    {
        $result = [];

        foreach ($array as $key => $value) {
            if (is_object($value)) {
                continue;
            }
            $val = is_array($value) ? json_encode($value) : strval($value);

            if ($key === "") {
                $result[] = strval($val);
            } else {
                $result[] = strval($key) . ":" . $val;
            }
        }

        return $result;
    }

    public function parseAddress($address)
    {
        $result = [];
        $result[""] = "Adresa client";
        foreach ($address as $key => $value) {
            if (isset($value['name'])) {
                $result[$key] = $value['name'];
            } else {
                $result[$key] = $value;
            }
        }
        $result['Status Adresa'] = "neeligibila";
        return $result;
    }

    public function getUrlWithNoParams($url)
    {
        if(strpos($url, '?') !== false) {
            $url = substr($url, 0, strpos($url, '?'));
        }

        $url = rtrim($url, '/');

        if( strpos($url, 'www.hellofix.ro/help') !== false ) {
            $url = 'https://www.hellofix.ro/help';
        }

        return $url;
    }
}
