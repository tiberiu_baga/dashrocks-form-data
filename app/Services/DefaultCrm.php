<?php

namespace App\Services;

use App\Models\Action;

class DefaultCrm implements CrmInterface
{
    private $action;

    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    public function upload()
    {
        return true;
    }
}
