<?php

namespace App\Services;

use App\Models\Action;

class OrangeRepoCrm implements CrmInterface
{

    private $action;

    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    public function upload()
    {
        $data = $this->buildData();

        if ($this->leadWithInvalidEmail($this->action)) {
            $this->action->setCrmResponse(400, "Not sent. Email invalid.", 'oro-repo');

            \Log::info('data NOT sent to Orange Email Repo \n Reason : Email invalid. Data:' . print_r($data, 1));
            return true;
        }

        $repoUrl = config('app.repo.url');

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $repoUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
            ]
        ]);
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        \Log::info('data sent to Orange Email Repo \n Sent data : ' . print_r($data, 1) . '\n Received response: ' . print_r($response, 1) . " with status code: " . print_r($statusCode, 1));

        $this->action->setCrmResponse($statusCode, $response, 'oro-repo');

        return true;
    }

    protected function buildData(): array
    {
        // {"address":"laszlo@microanalytics.ro","msisdn":null,"name":"colectaredec2022","newsFlag":"da","sendEmailFlag":"da","consent":"yes","extraParams":{"sourceDescription":"colectaredec2022"}}
        return [
            "address" => $this->action->email,
            "msisdn" => null,
            "name" => "colectaredec2022",
            "newsFlag" => "da",
            "sendEmailFlag" => "da",
            "consent" => "yes",
            "extraParams" => [
                "sourceDescription" => "colectaredec2022"
            ]
        ];
    }

    private function leadWithInvalidEmail(Action $action)
    {
        return !filter_var($action->email, FILTER_VALIDATE_EMAIL);
    }

}
