<?php

namespace App\Services;

use App\Models\Action;
use Carbon\Carbon;

class OrangeTkrCrm implements CrmInterface
{
    use PhonePerFormDayLimiter;

    private $action;
    private $leadMap;

    public function __construct(Action $action)
    {
        $this->leadMap = LeadMapper::$leadMap;
        $this->action = $action;
    }

    public function upload()
    {
        $data = $this->buildData();

        if ($this->leadWithInvalidPhone($this->action)) {
            $this->action->setCrmResponse(400, "Not sent. Phone number invalid." , 'oro-tkr');

            \Log::info('data NOT sent to OrangeTkrCRM \n Reason : Phone number invalid. Data:' . print_r($data, 1));
            return true;
        }

        if ($this->leadAlreadyRegisteredToday($this->action)) {
            $this->action->setCrmResponse(400, "Not sent. Phone number already registered for the form today." , 'oro-tkr');

            \Log::info('data NOT sent to OrangeTkrCRM \n Reason : Phone number already registered for the form today. Data:' . print_r($data, 1));
            return true;
        }

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => config('app.tkr.url'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
            ],
        ]);
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if(!$response){
            \Log::info('Curl error at : ' . print_r(curl_error($curl), 1) );
        }

        curl_close($curl);

        // $response = json_encode(
        //     ['message' => 'all good']
        // );
        // $statusCode = 200;

        \Log::info('\n data sent to OrangeTkrCRM to endpoint: '. config('app.tkr.url') .' \n Sent data : ' . print_r($data, 1)
            . '\n Received response: ' . print_r($response, 1) . " with status code: " . print_r($statusCode, 1));

        $this->action->setCrmResponse($statusCode, $response, 'oro-tkr');

        // START - send to new Endpoint as well
        if(config('app.tkr.url') == 'https://leads.fix.orange.ro/leadin.php') {
            return true;
        }
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://leads.fix.orange.ro/leadin.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
            ],
        ]);
        $response = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if(!$response){
            \Log::info('Curl error new endpoint at : ' . print_r(curl_error($curl), 1) );
        } else {
            \Log::info('data sent to OrangeTkrCRM NewEndpoint \n Received response: ' . print_r($response, 1) . " with status code: " . print_r($statusCode, 1));
        }
        curl_close($curl);
        // END - send to new Endpoint as well

        return true;
    }

    protected function buildData()
    {
        $data = array_merge( $this->action->toArray(), [
            "key" => config('app.tkr.key'),
            "date" => $this->action->created_at->tz('Europe/Bucharest')->toDateTimeString(),
            "logged_in" => $this->action->logged_in ? 'true': 'false',
            "page" => $this->getPageAttribute(),
            "gdpr" => true
        ] );

        $remove = ['created_at', 'updated_at', 'ip', 'crm_type', 'crm_send', 'crm_send_response', 'status', 'source'];
        $data = array_diff_key($data, array_flip($remove));

        return $data;
    }

    protected function getPageAttribute()
    {
        if(strpos($this->action->form, 'leads-') !== false) {
            if(isset($this->leadMap[$this->action->form])) {
                return $this->leadMap[$this->action->form]['leadSource'];
            }
        }

        return $this->getUrlWithNoParams($this->action->page);
    }

    public function getUrlWithNoParams($url)
    {
        if(strpos($url, '?') !== false) {
            $url = substr($url, 0, strpos($url, '?'));
        }

        $url = rtrim($url, '/');

        if( strpos($url, 'www.hellofix.ro/help') !== false ) {
            $url = 'https://www.hellofix.ro/help';
        }

        return $url;
    }

}
