<?php

namespace App\Services;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ActitoService
{
    private $apiPath = 'https://api3.actito.com';
    private $apiVersion = 'v5';
    public $entityId;
    public $start;
    public $end;
    private $accessToken;
    private $accessTokenExpires = 0;
    /**
     * @var HttpClient
     */
    private $httpClient;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($start, $end)
    {
        $this->entityId = config('services.actito.entity');
        $this->start = $start;
        $this->end = $end;
        $this->httpClient = HttpClient::create([
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36',
            ],
        ]);
    }

    public function getCampaignsReport($name = null, $status = null)
    {
        $endpoint = '/' . $this->apiVersion . '/entities/' . $this->entityId . '/email-campaigns-results?'
            . 'createdAtFrom=' . $this->start
            . '&createdAtTo=' . $this->end;

        if ($name) {
            $endpoint .= '&nameLike=' . $name;
        }
        if ($status) { //"DRAFT" "SCHEDULED" "IN_PROGRESS" "FINISHED" "IN_ERROR"
            $endpoint .= '&status=' . $status;
        }
        // $endpoint = '/entities/'. $this->entityId.'/email-campaign-groups';
        // $endpoint .= '&fields=name,type,status,displayOptions,id';
        $result = [];
        do {
            $response = $this->curlRequest($endpoint);
            if (!isset($response->_embedded->emailCampaignsResults)) {
                \Log::error("No result for $endpoint");
                break;
            }
            $result = array_merge($result, $this->mapper($response->_embedded->emailCampaignsResults));
            $endpoint = isset($response->_links->next->href) ? str_replace(' ', '+', $response->_links->next->href) : null;
        } while (!is_null($endpoint));

        return $result;
    }

    public function getCampaignSubject($campaignId)
    {
        $endpoint = '/v4/entity/' . $this->entityId . '/mail/' . $campaignId . '/content/subject';
        $response = $this->curlRequest($endpoint);

        return isset($response->subject) ? $response->subject : null;
    }

    public function getCampaignBody($campaignId)
    {
        $endpoint = '/v4/entity/' . $this->entityId . '/mail/' . $campaignId . '/content/body';
        $response = $this->curlRequest($endpoint);

        return $response;
    }

    public function getCampaignTarget($campaignId)
    {
        $endpoint = '/v4/entity/' . $this->entityId . '/mail/' . $campaignId . '/content/target';
        $response = $this->curlRequest($endpoint);

        return isset($response) ? print_r($response, 1) : null;
    }

    private function getAccessToken()
    {
        if ($this->accessToken && $this->accessTokenExpires > time()) {
            return $this->accessToken;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->apiPath . '/auth/token',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => [
                'Authorization: ' . config('services.actito.key'),
                'Accept: application/json'
            ],
        ));

        $response = curl_exec($curl);
        if (curl_error($curl)) {
            \Log::error(curl_error($curl));
            return false;
        }
        curl_close($curl);

        $response = json_decode($response);

        $this->accessToken = $response->accessToken;
        $this->accessTokenExpires = time() + 880; //900s

        return $this->accessToken;
    }

    private function curlRequest($endpoint, $method = 'GET', $returnStatus = false, $postfields = [])
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->apiPath . $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $this->getAccessToken(),
                'Accept: application/json'
            ],
            ]
        );

        $response = curl_exec($curl);
        if (curl_error($curl)) {
            \Log::error(curl_error($curl));
            return false;
        }
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($returnStatus) {
            return [
                'status' => $statusCode,
                'response' => json_decode($response) ?? $response,
            ];
        }

        return json_decode($response) ?? $response;
    }

    protected function mapper($results)
    {
        $mappedResult = [];
        foreach ($results as $result) {
            $body = $this->getCampaignBody($result->campaignId);
            $links = $this->getLinksFromBody($body);

            $idx = 0;
            do {
                $mainLink = $this->finalUrlOf((string)$links[$idx]);
                $utmData = $this->extractUtmData($mainLink);
                $idx++;
            } while ($utmData['utm_medium'] !== 'email' && $idx < count($links));

            $mappedResult[] = [
                'platform' => 'actito',
                'campaign_id' => $result->campaignId,
                'campaign_name' => $result->campaignName,
                'subject_line' => $this->getCampaignSubject($result->campaignId),
                'body' => $body,
                'action_link' => $mainLink,
                'action_links' => json_encode($links),
                'utm_campaign' => $utmData['utm_campaign'],
                'utm_medium' => $utmData['utm_medium'],
                'utm_source' => $utmData['utm_source'],
                // 'campaign_title' => null,
                // 'campaign_group' => null,
                // 'campaign_label' => null,
                'campaign_display_name' => $result->campaignDisplayName,
                'started_at' => $result->startedAt,
                'ended_at' => $result->endedAt,
                'status' => $result->status,
                // 'base_name' => null,

                'targeted' => $result->report->targeted,
                'sent' => $result->report->sent,
                'delivered' => $result->report->delivered,
                'opens' => $result->report->opened,
                // 'unique_opens' => 0,
                'clicks' => $result->report->clicked,
                // 'unique_clicks' => 0,
                'unsubscribed' => $result->report->unsubscribed,
                'filtered' => $result->report->filtered,
                'bounces' => $result->report->bounces,
                'complaints' => $result->report->complaints,
                'errors' => $result->report->errors,
                // 'extras'  => null,
            ];
        }

        return $mappedResult;
    }

    protected function getLinksFromBody($body): array
    {
        $crawler = new Crawler($body, 'https://www.orange.ro');
        $mainLink = null;
        $links = [];
        foreach ($crawler->filter('a')->links() as $link) {
            $actitoLinkId = $link->getNode()->getAttribute('data-actito-link-id');
            if ($actitoLinkId === 'link_0') {
                $mainLink = preg_replace('/\s+/', '', $link->getUri());
            } else {
                $links[] = preg_replace('/\s+/', '', $link->getUri());
            }
        }

        array_unshift($links, $mainLink);

        return $links;
    }

    protected function extractUtmData($url): array
    {
        $query = parse_url($url, PHP_URL_QUERY);
        parse_str($query, $params);
        $utmParameters['utm_campaign'] = $params['utm_campaign'] ?? '';
        $utmParameters['utm_medium'] = $params['utm_medium'] ?? '';
        $utmParameters['utm_source'] = $params['utm_source'] ?? '';

        return $utmParameters;
    }

    private function finalUrlOf(string $link): string
    {
        $finalUrl = $link;
        try {
            $response = $this->httpClient->request('HEAD', $link);
            $statusCode = $response->getStatusCode();
            if (200 === $statusCode) {
                $finalUrl = $response->getInfo('url');
            }
        } catch (TransportExceptionInterface $e) {

        }
        return $finalUrl;
    }

    public function triggerScenario($email)
    {
        $endpoint = '/v4/entity/' . $this->entityId . '/table/' . $this->entityId . '/scenario/'
            . config('services.actito.abandonScenarioId') . '/profile/EMAIL=' . $email;

        return $this->curlRequest($endpoint, 'POST', 1);
    }

    public function triggerEmail($campaignId, $email, $fields) {

        $endpoint = '/v4/entity/' . $this->entityId . '/mail/'
            . $campaignId
            . '/profile/EMAIL=' . $email;

        return $this->curlRequest($endpoint, 'POST', 1, $fields);
    }

    public function createProfileIfNotExists($email) {

        $endpoint = '/v4/entity/' . $this->entityId . '/table/'. $this->entityId. '/profile';
        $fields = '{"attributes": [{"name": "EMAIL","value": "'. $email .'"}]}';

        return $this->curlRequest($endpoint, 'POST', 1, $fields);
    }
}
