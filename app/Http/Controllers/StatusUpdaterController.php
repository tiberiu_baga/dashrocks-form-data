<?php

namespace App\Http\Controllers;

use App\Models\Action;
use Illuminate\Http\Request;

class StatusUpdaterController extends Controller
{
    public function statusReceiver(Request $request)
    {
        $postData = $request->all();
        $postData['headers'] = $request->header();

        \Log::info('updatesReceiver received : ' . print_r($postData, 1));

        if (!$this->checkAuth($postData)) {
            return response(["message" => "Unauthorized"], 401);
        }

        if (!isset($postData['leads'])) {
            return response(["message" => "Missing data"], 422);
        }
        if(!array_key_exists(0, $postData['leads'])) {
            // Only one array sent
            $postData['leads'] = [0 => $postData['leads']];
        }
        foreach ($postData['leads'] as $lead) {
            if (!isset($lead['status'])) {
                continue;
            }
            if (isset($lead['externalId'])) {
                $actions = Action::whereId($lead['externalId'])->get();
            }

            if (!isset($action) && isset($lead['phoneNumber']) && isset($lead['createdDate'])) {
                $date = date('Y-m-d', strtotime($lead['createdDate']));
                $actions = Action::where('phone', $lead['phoneNumber'])
                    ->whereRaw('DATE(created_at)', $date)
                    ->get();
            }

            if ($actions->count() > 0) {
                // Update action stat
                foreach ($actions as $action) {
                    $action->setStatus($lead['status']);
                }
            }
        }
        return '{}';
    }

    protected function checkAuth($postData)
    {
        return isset($postData['cbox_key']) && $postData['cbox_key'] == config('app.cbox_key');
    }
}
