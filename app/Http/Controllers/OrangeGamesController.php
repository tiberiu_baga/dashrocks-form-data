<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\XmasLotteryOrangeGame;

class OrangeGamesController extends Controller
{
    public function save(Request $request)
    {
        $postData = $request->all();
        $postData['headers'] = $request->header();

        return response([
            "error" => true,
            "message" => "The lottery has ended."
        ]);

        // check auth
        if (!$this->checkAuth($postData)) {
            return response([
                "error" => true,
                "message" => "Unauthorized"
            ], 401);
        }

        // check if valid request
         if (!isset($postData['id'])) {
            return response([
                "error" => true,
                "message" => "Missing data"
            ], 422);
        }

        $game = new XmasLotteryOrangeGame($postData['id']);
        // check if not already registered
        if ($game->alreadyRegistered()) {
            return response([
                "error" => true,
                "message" => "Order already registered"
            ], 200);
        }

        // generate code
        $response = $game->register($postData['email'] ?? null);

        // respond
        return response($response, $response['error'] === true ? 200 : 201);

        // \Log::info('updatesReceiver received : ' . print_r($postData, 1));
    }

    public function play(Request $request)
    {
        $postData = $request->all();
        $postData['headers'] = $request->header();

        return response([
            "error" => true,
            "message" => "The lottery has ended."
        ]);

        // check auth
        if (!$this->checkAuth($postData)) {
            return response([
                "error" => true,
                "message" => "Unauthorized"
            ], 401);
        }

        // check if valid request
         if (!isset($postData['code']) || !isset($postData['phone'])) {
            return response([
                "error" => true,
                "message" => "Missing data"
            ], 422);
        }

        // check if Code exists
        $game = new XmasLotteryOrangeGame(null, $postData['code']);
        if (!$game->alreadyRegistered()) {
            return response([
                "error" => true,
                "message" => "Invalid code"
            ], 200);
        }

        // check if not already played
        if ($game->alreadyPlayed()) {
            return response([
                "error" => true,
                "message" => "Already played",
                "status" => $game->game->status,
                "prize" => $game->responseFormattedPrize($game->game->prize)
            ], 200);
        }

        // get transaction data from Orange
        return response($game->play($postData['phone']), 200);

    }

    public function notify(Request $request)
    {
        $postData = $request->all();
        $postData['headers'] = $request->header();

        return response([
            "error" => true,
            "message" => "The lottery has ended."
        ]);

        // check auth
        if (!$this->checkAuth($postData)) {
            return response([
                "error" => true,
                "message" => "Unauthorized"
            ], 401);
        }

        // check if valid request
         if (!isset($postData['code']) || !isset($postData['email'])) {
            return response([
                "error" => true,
                "message" => "Missing data"
            ], 422);
        }

        // // check if Code exists
        $game = new XmasLotteryOrangeGame(null, $postData['code']);
        if (!$game->alreadyRegistered()) {
            return response([
                "error" => true,
                "message" => "Invalid code"
            ], 200);
        }

        // // check if not already played
        if ($game->alreadyPlayed()) {
            return response($game->notifyPrize($postData['email'], 200));
        }

        return response($game->notifyCode($postData['email'], 200));
    }

    protected function checkAuth($postData)
    {
        return isset($postData['key']) && $postData['key'] == config('app.oro_games_key');
    }
}
