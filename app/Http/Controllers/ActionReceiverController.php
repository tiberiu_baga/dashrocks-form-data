<?php

namespace App\Http\Controllers;

use App\Jobs\SendToCrmJob;
use App\Models\Action;
use App\Models\Customer;
use App\Services\CustomerService;
use App\Services\CrmManager;
use Illuminate\Http\Request;

class ActionReceiverController extends Controller
{
    private $authKey = '2afbd389089a5176e12ede2422e3540a';
    private $columns = ['name', 'state', 'city', 'address', 'email', 'phone', 'subject', 'message',
        // 'source', 'referrer', 'utm_campaign', 'utm_medium', 'utm_source'
    ];

    public function create(Request $request)
    {
        if (!$this->checkAuth($request)) {
            return response()->json(['error' => 'missing data'], 400);
        }

        $postData = $request->post_data;
        foreach ($this->columns as $col) {
            if(isset($postData[$col])) {
                unset($postData[$col]);
            }
        }

        $utms = $this->getUtmParams($request);

        $action = Action::create([
            'logged_in' => $request->logged_id == 'true' ? 1 : 0,
            'form' => $request->form ?? '',
            'page' => substr(($request->page ?? ''), 0, 254),
            'medium' => $this->isMobile($request->agent ?? null) ? 'mobile' : 'web',
            'agent' => isset($request->agent) ? substr($request->agent, 0, 254) : '',
            'name' => $request->name ?? null,
            'state' => $request->state ?? null,
            'city' => $request->city ?? null,
            'address' => $request->address ?? null,
            'email' => $request->email ?? null,
            'phone' => isset($request->phone) ? $this->formatPhone($request->phone) : null,
            'token' => $request->token ?? null,
            'sso_id' => $request->sso_id ?? null,
            'subject' => $request->subject ?? null,
            'message' => $request->message ?? null,
            'source' => $request->source ?? null,
            'referrer' => $request->referrer ?? null,
            'utm_campaign' => $utms['utm_campaign'],
            'utm_medium' => $utms['utm_medium'],
            'utm_source' => $utms['utm_source'],
            'gdpr' => $this->getGdpr($request->gdpr),
            'ip' => $request->ip(),
            'post_data' => $postData ? json_encode($postData) : '[]',
        ]);

        // $customerData = [
        //     'type' => $request->type ?? null,
        //     'loyalty_eur' => $request->loyalty_eur ?? null,
        //     'offer_eligibility' => $request->offer_eligibility ?? null,
        //     'last_loyalty_date' => $request->last_loyalty_date ?? null,
        //     'contract_expiration_date' => $request->contract_expiration_date ?? null,
        //     'package_code' => $request->package_code ?? null,
        // ];

        $customerService = new CustomerService($action);
        $customerService->store();

        // save to CRM
        if ($action->form != 'verifica-acoperirea') {
            dispatch(new SendToCrmJob($action));
        }


        return response()->json(['status' => 'saved'], 201);
    }

    public function customerDataReceiver(Request $request)
    {
        $postData = $request->all();
        foreach (['sso_id', 'msisdn', 'email', 'token', 'state', 'city', 'address',
            'customer_type','loyaltyEur','offerEligibility','lastLoyaltyDate','contractExpirationDate','packageCode',
            'phone', 'gdpr-directmarketing', 'gdpr-marketresearch', 'gdpr-orangemoney', 'gdpr-thirdparty' ] as $col) {
            unset($postData[$col]);
        }

        $data = [
            'sso_id' => $request->sso_id ?? null,
            'phone' => $this->formatPhone($request->msisdn),

            'email' => $request->email ?? null,
            'token' => $request->token ?? null,
            'state' => $request->state ?? null,
            'city' => $request->city ?? null,
            'address' => $request->address ?? null,
            // Contract details
            'type' => $request->customer_type ?? null,
            'loyalty_eur' => $request->loyaltyEur ?? null,
            'offer_eligibility' => $request->offerEligibility ?? null,
            'last_loyalty_date' => $request->lastLoyaltyDate ?? null,
            'contract_expiration_date' => $request->contractExpirationDate ?? null,
            'package_code' => $request->packageCode ?? null,

            'phone_type' => $request->phone ?? null,
            'gdpr_directmarketing' =>  $this->getGdpr($request->{'gdpr-directmarketing'}),
            'gdpr_marketresearch' =>  $this->getGdpr($request->{'gdpr-marketresearch'}),
            'gdpr_orangemoney' =>  $this->getGdpr($request->{'gdpr-orangemoney'}),
            'gdpr_thirdparty' =>  $this->getGdpr($request->{'gdpr-thirdparty'}),
            'extras' => json_encode($postData),
        ];
        // \Log::info('Data received : '. print_r($data, 1));

        foreach (array_keys($data) as $col) {
            if (is_null($data[$col]) || $data[$col] == "") {
                unset($data[$col]);
            }
        }

        if (count($data) == 1 || (!isset($data['phone']) && !isset($data['sso_id']))) {
            \Log::error('invalid-customer-data received : '. print_r($data, 1));
            return ['status' => 'invalid-data'];
        }

        $customer = false;
        if (isset($data['phone']) && isset($data['sso_id'])) {
            $customer = Customer::where('phone', $data['phone'])
                ->where('sso_id', $data['sso_id'])
                ->first();
            ;
        }

        if (!$customer && isset($data['phone'])) {
            $customer = Customer::where('phone', $data['phone'])
                ->first();
            ;
        }

        if (!$customer && isset($data['sso_id'])) {
            $customer = Customer::where('sso_id', $data['sso_id'])
                ->first();
            ;
        }

        if ($customer) {
            $customer = Customer::updateOrCreate(
                ['id' => $customer->id],
                $data
            );

            return ['status' => 'updated'];
        } else {
            $customer = Customer::create($data);
            return ['status' => 'saved'];
        }

        return ['status' => 'not-saved'];
    }

    public function isMobile($userAgent = null)
    {
        if (empty($userAgent)) {
            return false;
        }

        if (strpos($userAgent, 'Mobile') !== false// many mobile devices (all iPhone, iPad, etc.)
            || strpos($userAgent, 'Android') !== false
            || strpos($userAgent, 'Silk/') !== false
            || strpos($userAgent, 'Kindle') !== false
            || strpos($userAgent, 'BlackBerry') !== false
            || strpos($userAgent, 'Opera Mini') !== false
            || strpos($userAgent, 'Opera Mobi') !== false) {
            return true;
        }

        return false;
    }



    protected function checkAuth(Request $request)
    {
        return $request->form
        && $request->robit
        && $request->form . $this->authKey == $request->robit;
    }

    protected function getGdpr($gdpr)
    {
        $lowGdpr = strtolower($gdpr);
        return (
            !$gdpr || $gdpr === 0
            || $lowGdpr === 'false' || $gdpr === false
            || $lowGdpr === 'off'
            || $lowGdpr === "null"
            || $lowGdpr === "nu"
            || $lowGdpr === "no"
        ) ? 0 : 1;
    }

    protected function formatPhone($phone = null)
    {
        if (is_null($phone) || strlen($phone) == 0) {
            return null;
        }
        $phone = str_replace(
            ["+40", " ", "(", ")", "-", "+"],
            ["0", "", "", "", "", ""],
            $phone
        );

        if (strpos($phone, "0040") === 0) {
            $phone = substr_replace($phone, "0", 0, 4);
        }

        if (strpos($phone, "4007") === 0) {
            $phone = substr_replace($phone, "07", 0, 4);
        }

        if (strpos($phone, "407") === 0) {
            $phone = substr_replace($phone, "07", 0, 3);
        }

        if (strpos($phone, "007") === 0) {
            $phone = substr_replace($phone, "07", 0, 3);
        }

        if (strpos($phone, "00") === 0) {
            $phone = substr_replace($phone, "00", 0, 2);
        }

        if (isset($phone[0]) && $phone[0] !== "0") {
            $phone = "0" . $phone;
        }

        return $phone;
    }

    protected function getUtmParams($request)
    {
        if(is_array($request)) {
            $request = json_decode(json_encode($request));
        }

        $result = [
            'utm_campaign' => $request->utm_campaign ?? null,
            'utm_medium' => $request->utm_medium ?? null,
            'utm_source' => $request->utm_source ?? null
        ];
        if(!isset($request->page) || ( isset($request->overwriteUtms) && $request->overwriteUtms) ) {
            return $result;
        }

        $parsedUrl = parse_url($request->page);
        if(!isset($parsedUrl['query'])) {
            return $result;
        }
        parse_str($parsedUrl['query'], $params);

        if(isset($params['campaign'])) {
            $result['utm_campaign'] = $params['campaign'];
        }
        if(isset($params['utm_campaign'])) {
            $result['utm_campaign'] = $params['utm_campaign'];
        }
        if(isset($params['t_campaign'])) {
            $result['utm_campaign'] = $params['t_campaign'];
        }
        if(isset($params['medium'])) {
            $result['utm_medium'] = $params['medium'];
        }
        if(isset($params['utm_medium'])) {
            $result['utm_medium'] = $params['utm_medium'];
        }
        if(isset($params['source'])) {
            $result['utm_source'] = $params['source'];
        }
        if(isset($params['utm_source'])) {
            $result['utm_source'] = $params['utm_source'];
        }

        return $result;
    }
}
