<?php

namespace App\Http\Controllers;

use App\Jobs\SendToCrmJob;
use App\Models\Action;
use App\Services\CustomerService;
use App\Services\CrmManager;
use App\Services\GoogleAdsLeadCampaignMatcher;
use Illuminate\Http\Request;

class LeadsReceiverController extends Controller
{
    protected $columns = [
        'full_name', 'nume_complet', 'nume', 'name',
        'state', 'address', 'subject', 'message',
        'e-mail', 'email',
        'phone_number', 'număr_de_telefon', 'numar_de_telefon', 'num\u0103r_de_telefon', 'phone',
        'ora\u015f', 'oraș', 'oras', 'city',
        'utm_campaign', 'utm_medium', 'utm_source', 'gdpr'
    ];

    public function leadsReceiver(Request $request, $form)
    {
        $postData = $request->all();
        $postData['headers'] = $request->header();
        $postData['form'] = $form;

        \Log::info('leadsReceiver received : ' . print_r($postData, 1));

        if (!$this->checkAuth($postData, $form)) {
            return response(["message" => "Unauthorized"], 401);
        }

        $data = strpos($form, 'googleads') !== false ?
            $this->getGoogleAction($postData, $request->ip(), $form) :
            $this->getFacebookAction($postData, $request->ip(), $form);

        if($form == 'yoxo-app') {
            $data['page'] = 'Yoxo App';
        }
        $action = Action::create($data);
        // save customer data
        $customerService = new CustomerService($action);
        $customerService->store();

        // save to CRM
        dispatch(new SendToCrmJob($action));

        // successful call
        return '{}';
    }

    protected function checkAuth($postData, $form)
    {
        if (strpos($form, 'googleads') !== false || strpos($form, 'google-') !== false) {
            return isset($postData['google_key']) && $postData['google_key'] == 'Zz7WmcpTN1P-duEjxeGFvLl-1ZGvYXQZTkt_x5YY7kxttQx';
        }

        if (strpos($form, 'facebook') !== false || strpos($form, 'tiktok') !== false || $form == 'yoxo-app') {
            return isset($postData['headers']['authorization'][0]) && trim($postData['headers']['authorization'][0]) == 'Bearer 0jTFoNZhur5-E4utiW8X9Rl-zltKsubbhvl_YvaDww88X8G';
        }

        return false;
    }

    protected function getFacebookAction($postData, $ip, $form)
    {
        // Get google lead form data
        $agent = isset($postData['headers']['user-agent'][0]) ? substr($postData['headers']['user-agent'][0], 0, 254) : '';
        $data = [
            'form' => 'leads-' . $form,
            'agent' => $agent,
            'medium' => $this->isMobile($agent) ? 'mobile' : 'web',
            'page' => 'lead - id: ' . ($postData['id'] ?? 'not-set'),
            // 'ip' => $ip,
        ];
        foreach ($this->columns as $col) {
            if (isset($postData[$col])) {
                if ($col == 'phone' || $col == 'phone_number' || $col == 'număr_de_telefon' || $col == 'numar_de_telefon' || $col == 'num\u0103r_de_telefon') {
                    $data['phone'] = $this->formatPhone($postData[$col]);
                    unset($postData[$col]);
                } elseif ($col == 'name' || $col == 'full_name' || $col == 'nume_complet' || $col == 'nume') {
                    $data['name'] = $postData[$col];
                    unset($postData[$col]);
                } elseif ($col == 'e-mail') {
                    $data['email'] = $postData[$col];
                    unset($postData[$col]);
                } elseif ($col == 'ora\u015f' || $col == 'oraș' || $col == 'oras') {
                    $data['city'] = $postData[$col];
                    unset($postData[$col]);
                } else {
                    $data[$col] = $postData[$col];
                    unset($postData[$col]);
                }
            }
        }
        foreach (['headers', 'form', 'google_key', 'user_column_data'] as $col) {
            unset($postData[$col]);
        }
        $data['post_data'] = json_encode($postData);

        return $data;
    }

    protected function getGoogleAction($postData, $ip, $form)
    {
        // Get google lead form data
        $agent = isset($postData['headers']['user-agent'][0]) ? substr($postData['headers']['user-agent'][0], 0, 254) : '';
        $data = [
            'form' => 'leads-' . $form,
            'agent' => $agent,
            'medium' => $this->isMobile($agent) ? 'mobile' : 'web',
            'page' => 'lead - id: ' . $postData['lead_id'],
            // 'ip' => $ip,
        ];
        foreach ($postData['user_column_data'] as $lead) {
            switch ($lead['column_id']) {
                case 'FULL_NAME':
                case 'FIRST_NAME':
                case 'LAST_NAME':
                    $data['name'] = isset($data['name']) ?
                    $data['name'] . " " . $lead['string_value'] :
                    $lead['string_value'];
                    break;
                case 'CITY':
                case 'EMAIL':
                    $data[strtolower($lead['column_id'])] = $lead['string_value'];
                    break;
                case 'PHONE_NUMBER':
                    $data['phone'] = $this->formatPhone($lead['string_value']);
                    break;
                case 'STREET_ADDRESS':
                    $data['address'] = $lead['string_value'];
                    break;

                case 'FULL_NAME':
                    $data['name'] = $lead['string_value'];
                    break;
                case 'CITY':
                    $data['city'] = $lead['string_value'];
                    break;
                case 'REGION':
                    $data['state'] = $lead['string_value'];
                    break;
                default:
                    $postData[strtolower($lead['column_id'])] = $lead['string_value'];
                    break;
            }
        }
        foreach (['headers', 'form', 'google_key', 'user_column_data'] as $col) {
            unset($postData[$col]);
        }

        $matchedData = (new GoogleAdsLeadCampaignMatcher())->matchCampaign($postData, $data);
        if ($matchedData) {
            $data = $matchedData;
        }

        $data['post_data'] = json_encode($postData);

        return $data;
    }

    protected function buildActionData()
    {
    }

    public function isMobile($userAgent = null)
    {
        if (empty($userAgent)) {
            return false;
        }

        if (strpos($userAgent, 'Mobile') !== false// many mobile devices (all iPhone, iPad, etc.)
            || strpos($userAgent, 'Android') !== false
            || strpos($userAgent, 'Silk/') !== false
            || strpos($userAgent, 'Kindle') !== false
            || strpos($userAgent, 'BlackBerry') !== false
            || strpos($userAgent, 'Opera Mini') !== false
            || strpos($userAgent, 'Opera Mobi') !== false) {
            return true;
        }

        return false;
    }

    protected function formatPhone($phone = null)
    {
        if (is_null($phone) || strlen($phone) == 0) {
            return null;
        }
        $phone = str_replace(
            ["+40", " ", "(", ")", "-", "+"],
            ["0", "", "", "", "", ""],
            $phone
        );

        if (strpos($phone, "0040") === 0) {
            $phone = substr_replace($phone, "0", 0, 4);
        }

        if (strpos($phone, "4007") === 0) {
            $phone = substr_replace($phone, "07", 0, 4);
        }

        if (strpos($phone, "407") === 0) {
            $phone = substr_replace($phone, "07", 0, 3);
        }

        if (strpos($phone, "007") === 0) {
            $phone = substr_replace($phone, "07", 0, 3);
        }

        if (strpos($phone, "00") === 0) {
            $phone = substr_replace($phone, "00", 0, 2);
        }

        if ($phone[0] != "0") {
            $phone = "0" . $phone;
        }

        return $phone;
    }
}
