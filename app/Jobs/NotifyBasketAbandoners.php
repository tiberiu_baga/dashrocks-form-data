<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Models\BasketAbandoner;
use App\Services\ActitoService;
use Log;

class NotifyBasketAbandoners extends Job
{
    private $source;
    private $isTesting;
    private $testEmails;

    function __construct()
    {
        $this->source = config('services.basketRecovery.source');
        $this->isTesting = config('services.basketRecovery.isTesting');
        $this->testEmails = explode(",", config('services.basketRecovery.testEmails'));
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(300);
        Log::Info("NotifyBasketAbandoners - execution started at " . date('Y-m-d H:i:s'));
        // Get Basket Abandoners from Inner
        $abandoners = $this->getInnerAbandonersWithRetries();
        $runDate = Carbon::now()->toDateTimeString();

        $actito = new ActitoService('', '');
        foreach($abandoners as $abandoner) {
            // Save Rows in Table
            $basketAbandoner = BasketAbandoner::create([
                'datetime' => $runDate,
                'sso_id' => isset($abandoner->ssoid) ? $abandoner->ssoid : null,
                'email' => $abandoner->email,
            ]);

            if (!filter_var($abandoner->email, FILTER_VALIDATE_EMAIL)) {
                $basketAbandoner->response = 'Not a valid email';
                $basketAbandoner->save();
                continue;
            }

            if($this->isTesting && !in_array($abandoner->email, $this->testEmails)) {
                $basketAbandoner->response = 'Skipped - Test Mode';
                $basketAbandoner->save();
                continue;
            }

            // Send To Actito
            $response = $actito->triggerScenario($abandoner->email);

            // Update basket
            $basketAbandoner->response = $response['response'];
            $basketAbandoner->save();

            if ($response['status'] == '202') {
                $basketAbandoner->setAsSent();
            }
        }
    }

    public function getInnerAbandoners()
    {
        $json = file_get_contents($this->source, 0, stream_context_create(["http" => ["timeout" => 200]]));
        if (!$json) {
            Log::error("NotifyBasketAbandoners - InnerFetch no json" . $json);
            return [];
        }
        $data = json_decode(trim($json));
        if (!$data) {
            Log::error("NotifyBasketAbandoners - InnerFetch invalid json" . print_r($data, 1) . print_r($json, 1));
            return [];
        }
        return $data;
    }

    protected function getInnerAbandonersWithRetries()
    {
        $tries = 0;
        do {
            $data = $this->getInnerAbandoners();
            $tries++;
        } while (count($data) == 0 && ($tries < 3));

        return $data;
    }
}
