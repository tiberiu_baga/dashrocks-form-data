<?php

namespace App\Jobs;

use App\Models\GoogleCampaign;
use App\Services\GoogleSheets;
use Google\Client;
use Google\Service\Sheets;

class FetchGoogleCampaignsJob extends Job
{

    public function __construct()
    {
    }

    public function handle()
    {
        $spreadsheetId = '1UMfv1xh0lD5ISS8dBBAGPe6Z7vi2pBT4BUdcgNWfbCk';
        $sheets = new GoogleSheets($spreadsheetId);
        $range = 'Sheet1';
        $values = $sheets->readSheetValues($range);

        $campaignData = [];
        $inCampaignData = false;
        foreach ($values as $row) {
            if ($inCampaignData) {
                $campaignId = $row[1] ?? null;
                $campaign = $row[0];
                if (!isset($campaignData[$row[0]])) {
                    $campaignData[$row[0]] = [];
                }
                $campaignData[$row[0]][] = $campaignId;

                if (is_numeric($campaignId)) {
                    GoogleCampaign::updateOrCreate([
                        'campaign_id' => $campaignId
                    ],
                    [
                        'campaign_id' => $campaignId,
                        'campaign' => $campaign
                    ]);
                }
            }
            if (count($row) >=2 && $row[0] === 'Campaign' && $row[1] === 'Campaign ID') {
                $inCampaignData = true;
            }
        }

    }
}
