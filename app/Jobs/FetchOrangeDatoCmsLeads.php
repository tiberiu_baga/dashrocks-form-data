<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Models\Action;
use App\Services\CrmManager;

class FetchOrangeDatoCmsLeads extends Job
{

    public $start;
    public $end;
    private $dateFormat = \DateTime::ISO8601;
    private $leadName = 'love-contact-forms';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($start = null, $end = null)
    {
        $this->start = $start ?
            Carbon::parse($start) :
            Carbon::now()->subMinutes(10);
        $this->end = $end ?
            Carbon::parse($end) :
            Carbon::now()->addMinutes(5);

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->getLeads() as $lead) {
            if ($this->leadExists($lead)) continue;

            $utms = $this->getUtmParams([
                'page' => $lead->page,
                'utm_campaign' => $lead->campaignCode,
                'utm_medium' => $lead->medium,
                'utm_source' => $lead->source
            ]);

            // create action
            $action = Action::create([
                'form' => $lead->form ?? $this->leadName,
                'source' => $lead->id,
                'phone' => $lead->phone,
                'email' => $lead->email ?? null,
                'name' => $lead->name ?? null,
                'city' => $lead->city ?? null,
                'state' => $lead->state ?? null,
                'page' => $lead->page,
                'gdpr' => $lead->gdpr,
                'created_at' => Carbon::parse($lead->createdAt)->setTimezone('UTC')->toDateTimeString(),
                'utm_campaign' => $utms['utm_campaign'],
                'utm_medium' => $utms['utm_medium'],
                'utm_source' => $utms['utm_source'],
                'post_data' => $lead->postData ?? json_encode(['formId' => $lead->formId]),
            ]);

            // save to CRM
            $service = new CrmManager($action);
            $crm = $service->make();
            $crm->upload();
        }
    }

    protected function getLeads()
    {
        $loveContectFormsLeads = $this->getLoveContactFormsLeads();
        $hiptoLeads = $this->getHiptoLeads();
        return array_merge($loveContectFormsLeads, $hiptoLeads);
    }

    protected function getLoveContactFormsLeads()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => config('app.datoCms.url'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{"query":"query MyQuery {\\r\\n  allLoveContactForms(\\r\\n      filter: {\\r\\n        createdAt: {gt: \\"'
                . $this->start->format($this->dateFormat) . '\\"} \\r\\n    }\\r\\n  ) {\\r\\n    id\\r\\n    phone\\r\\n    gdpr\\r\\n    campaignCode\\r\\n    medium\\r\\n    source\\r\\n    page\\r\\n    createdAt\\r\\n    updatedAt\\r\\n formId\\r\\n }\\r\\n}","variables":{}}',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . config('app.datoCms.key'),
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            curl_close($curl);
            \Log::error("cURL Error in FetchOrangeDatoCmsLeads@getLeads #:" . curl_error($curl));
            return false;
        }
        curl_close($curl);

        $data = json_decode($response);
        if (isset($data->data) && $data->data->allLoveContactForms) {
            return $data->data->allLoveContactForms;
        }

        return [];
    }

    protected function getHiptoLeads()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => config('app.datoCms.url'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{"query":"query MyQuery {\\r\\n  allLoveContactForms(\\r\\n      filter: {\\r\\n        _createdAt: {gt: \\"'
                . $this->start->format($this->dateFormat) . '\\"} \\r\\n    }\\r\\n  ) {\\r\\n    id\\r\\n    phone\\r\\n    email\\r\\n    firstName\\r\\n    lastName\\r\\n    postCode\\r\\n    city\\r\\n    state\\r\\n    eligibleFiber\\r\\n    currentCommitment\\r\\n    currentPricePaid\\r\\n    currentProvider\\r\\n    utmSource\\r\\n    _createdAt\\r\\n    _updatedAt\\r\\n }\\r\\n}","variables":{}}',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . config('app.datoCms.hipto_key'),
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            curl_close($curl);
            \Log::error("cURL Error in FetchOrangeDatoCmsLeads@getHiptoLeads #:" . curl_error($curl));
            return false;
        }
        curl_close($curl);

        $data = json_decode($response);
        if (isset($data->data) && $data->data->allLoveContactForms) {
            $leads = [];
            foreach ($data->data->allLoveContactForms as $lead) {
                $lead->page = 'hipto';
                $lead->gdpr = true;
                $lead->source = $lead->utmSource;
                $lead->medium = null;
                $lead->campaignCode = null;
                $lead->formId = null;
                $lead->createdAt = $lead->_createdAt;
                $lead->form = 'leads-contact-hipto';
                $lead->name = $lead->firstName . ' ' . $lead->lastName;
                $lead->postData = json_encode([
                    'currentCommitment' => $lead->currentCommitment,
                    'currentPricePaid' => $lead->currentPricePaid,
                    'currentProvider' => $lead->currentProvider,
                    'postCode' => $lead->postCode,
                    'eligibleFiber' => $lead->eligibleFiber,
                ]);
                $leads[] = $lead;
            }
            return $leads;
        }

        return [];
    }

    protected function leadExists($lead)
    {
        $count = Action::where('form', $lead->form ?? $this->leadName)
            ->where('source', $lead->id)
            ->where('created_at', '>=', $this->start->copy()->subDays(30))
            ->count();
        return ($count > 0);
    }

    protected function getUtmParams($request)
    {
        if (is_array($request)) {
            $request = json_decode(json_encode($request));
        }

        $result = [
            'utm_campaign' => $request->utm_campaign ?? null,
            'utm_medium' => $request->utm_medium ?? null,
            'utm_source' => $request->utm_source ?? null
        ];
        if (!isset($request->page) || (isset($request->overwriteUtms) && $request->overwriteUtms)) {
            return $result;
        }

        $parsedUrl = parse_url($request->page);
        if (!isset($parsedUrl['query'])) {
            return $result;
        }
        parse_str($parsedUrl['query'], $params);

        if (isset($params['campaign'])) {
            $result['utm_campaign'] = $params['campaign'];
        }
        if (isset($params['utm_campaign'])) {
            $result['utm_campaign'] = $params['utm_campaign'];
        }
        if (isset($params['t_campaign'])) {
            $result['utm_campaign'] = $params['t_campaign'];
        }
        if (isset($params['medium'])) {
            $result['utm_medium'] = $params['medium'];
        }
        if (isset($params['utm_medium'])) {
            $result['utm_medium'] = $params['utm_medium'];
        }
        if (isset($params['source'])) {
            $result['utm_source'] = $params['source'];
        }
        if (isset($params['utm_source'])) {
            $result['utm_source'] = $params['utm_source'];
        }

        return $result;
    }
}
