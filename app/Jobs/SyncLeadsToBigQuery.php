<?php

namespace App\Jobs;

use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Core\ExponentialBackoff;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Support\Facades\DB;

class SyncLeadsToBigQuery extends Job
{
    /**
     * Path to the NDJSON file containing the leads export
     * @var string
     */
    private $exportPath;

    public function __construct()
    {
        $this->exportPath = storage_path('app/actions_filtered.ndjson');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->exportLeadsDataIntoFile();

        $this->uploadLeadsFileToGoogleCloud();

        $this->loadLeadsFileIntoBigQuery();
    }

    /**
     * @return void
     */
    private function exportLeadsDataIntoFile()
    {
        if (!$fp = fopen($this->exportPath, 'w+')) {
            throw new \Exception("Cannot open file ($this->exportPath)");
        }

        DB::table('actions')->select(DB::raw("
        JSON_OBJECT(
            'form', form,
            'created_at', created_at,
            'phone', phone,
            'page', page,
            'crm_send', crm_send,
            'utm_campaign', utm_campaign,
            'utm_medium', utm_medium,
            'utm_source', utm_source,
            'status' , status,
            'form_id', json_extract(post_data, '$.formId')
            ) AS payload
        "))
            ->where('created_at', '>', '2022-01-01')
            ->whereNotIn('form', [
                'disponibilitate',
                'contact',
                'newsletter-page',
                'newsletter_modal',
                'newsletter',
                'usabilla_feedback',
                'shops',
                'antreprenori',
                'homepage-abonament-nou',
                'abonamente-oferta-portare-client-nou',
                'client-nou'
            ])
            ->orderBy('id')
            ->chunk(5000, function ($leads) use ($fp) {
                foreach ($leads as $lead) {
                    fwrite($fp, $lead->payload . "\n");
                    $last = $lead;
                }
            });

        fclose($fp);
    }

    /**
     * @return void
     */
    private function uploadLeadsFileToGoogleCloud()
    {

        $storage = new StorageClient([
            'keyFilePath' => base_path('orange-195015-550c7e95d80e.json'),
        ]);
        $bucketName = 'staging.orange-195015.appspot.com';
        $bucket = $storage->bucket($bucketName);
        $chunkSize = 262144; // chunk in bytes

        $options = [
            'chunkSize' => $chunkSize,
        ];

        $uploader = $bucket->getResumableUploader(
            fopen($this->exportPath, 'r'),
            $options
        );

        try {
            $object = $uploader->upload();
        } catch (GoogleException $ex) {
            $resumeUri = $uploader->getResumeUri();
            $object = $uploader->resume($resumeUri);
        }

    }

    /**
     * @return void
     * @throws \Exception
     */
    private function loadLeadsFileIntoBigQuery()
    {

        $projectId = 'orange-195015';
        $datasetId = 'cbox';
        $tableId = 'actions-filtered';

        // instantiate the bigquery table service
        $bigQuery = new BigQueryClient([
            'projectId' => $projectId,
            'keyFilePath' => base_path('orange-195015-550c7e95d80e.json')
        ]);
        $dataset = $bigQuery->dataset($datasetId);
        $table = $dataset->table($tableId);

        // create the import job
        $gcsUri = 'gs://staging.orange-195015.appspot.com/actions_filtered.ndjson';

        $schema = [
            'fields' => [
                ['name' => 'form', 'type' => 'string'],
                ['name' => 'created_at', 'type' => 'datetime'],
                ['name' => 'phone', 'type' => 'string'],
                ['name' => 'page', 'type' => 'string'],
                ['name' => 'crm_send', 'type' => 'boolean'],
                ['name' => 'utm_campaign', 'type' => 'string'],
                ['name' => 'utm_medium', 'type' => 'string'],
                ['name' => 'utm_source', 'type' => 'string'],
                ['name' => 'status', 'type' => 'string'],
                ['name' => 'form_id', 'type' => 'string']
            ]
        ];
        $loadConfig = $table->loadFromStorage($gcsUri)
            ->schema($schema)
            ->sourceFormat('NEWLINE_DELIMITED_JSON')
            ->writeDisposition('WRITE_TRUNCATE');
        $job = $table->runJob($loadConfig);
        // poll the job until it is complete
        $backoff = new ExponentialBackoff(10);
        $backoff->execute(function () use ($job) {
            $job->reload();
            if (!$job->isComplete()) {
                throw new \Exception('Job has not yet completed', 500);
            }
        });
        // check if the job has errors
        if (isset($job->info()['status']['errorResult'])) {
            $error = $job->info()['status']['errorResult']['message'];
            throw new \Exception(sprintf('Error running BigQuery load job: %s' . PHP_EOL, $error));
        }
    }

}
