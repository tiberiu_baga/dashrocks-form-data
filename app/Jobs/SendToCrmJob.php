<?php

namespace App\Jobs;

use App\Models\Action;
use App\Services\CrmManager;

class SendToCrmJob extends Job
{
    private $action;

    public function __construct(Action $action)
    {
        $this->action = $action;
    }

    public function handle()
    {
        $service = new CrmManager($this->action);
        $crm = $service->make();
        $crm->upload();
    }
}
