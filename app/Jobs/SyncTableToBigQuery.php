<?php

namespace App\Jobs;

use App\Services\BigQuery\SyncConfig;
use App\Services\BigQuerySyncService;

class SyncTableToBigQuery extends Job
{
    private string $syncKey;

    public int $timeout = 3600;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $syncKey)
    {
        $this->syncKey = $syncKey;
    }

    private function getSyncConfig(string $syncKey): SyncConfig
    {
        $syncConfig = config('bigquery_sync');
        if (!isset($syncConfig[$syncKey])) {
            throw new \Exception('Sync key not found in config');
        }
        return SyncConfig::fromSyncConfig($syncKey, $syncConfig[$syncKey]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $syncConfig = $this->getSyncConfig($this->syncKey);
            $syncService = new BigQuerySyncService();
            $syncService->syncTableToBigQuery($syncConfig);
        } catch (\Exception $e) {
            dump($e->getMessage() . ' ' . $e->getTraceAsString());
            throw $e;
        }
    }

}
