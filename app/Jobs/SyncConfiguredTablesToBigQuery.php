<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Bus;

class SyncConfiguredTablesToBigQuery extends Job
{

    public function __construct()
    {
    }

    public function handle()
    {
        $syncConfig = config('bigquery_sync');
        foreach ($syncConfig as $syncKey => $syncConfigItem) {
            Bus::dispatch(new SyncTableToBigQuery($syncKey));
        }
    }
}
