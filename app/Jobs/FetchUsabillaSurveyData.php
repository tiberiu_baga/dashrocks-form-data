<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Models\UsabillaSurvey;
use Usabilla\API\Client\UsabillaClient;

class FetchUsabillaSurveyData extends Job
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Creates a new Usabilla client.
        $client = new UsabillaClient(config('app.usabilla.key'), config('app.usabilla.secret'));
        $since = Carbon::now()->subDays(5);

        foreach(config('app.usabilla.forms') as $form ) {
            do {
                $params = [
                    'id' => $form,
                    'limit' => 100,
                    'since' => $since->timestamp*1000
                ];
                $command = $client->getCommand('GetAppFeedbackItems', $params);
                $response = $client->execute($command);

                if(!isset($response['items'])) {
                    \Log::error("Error fetching Usabilla data in FetchUsabillaSurveyData with response " . print_r($response, 1));
                }

                foreach($response['items'] as $data) {
                    $mappedData = $this->keyMapper($data);
                    UsabillaSurvey::updateOrCreate(
                        ['external_id' => $mappedData['external_id']],
                        $mappedData
                    );
                }
                $since->addHours(12);
            } while ($response['count'] == 100 && $since->lt(Carbon::now()) );
        }
    }

    protected function keyMapper($data)
    {
        $unset = ['freeStorage', 'totalStorage', 'batteryLevel'];
        $jsonify = ['geoLocation'];

        $return = [];
        foreach ($data as $key => $value) {
            if(in_array($key, $unset)) {
                continue;
            }
            if($key == 'id') {
                $return['external_id'] = $value;
                continue;
            }
            if(in_array($key, $jsonify)) {
                $return[$this->toSnakeCase($key)] = json_encode($value);
                continue;
            }
            if(in_array($key, $unset)) {
                $return[$this->toSnakeCase($key)] = json_encode($value);
                continue;
            }

            if($key == 'customData' && is_array($value)) {
                foreach($value as $customDataKey => $customDataValue) {
                    $return['custom_data_' . $customDataKey] = $customDataValue;
                }
                continue;
            }

            if($key == 'data' && is_array($value)) {
                foreach($value as $dataKey => $dataValue) {
                    if(is_array($dataValue)) {
                        if(count($dataValue) == 1) {
                            $return[strtolower($dataKey)] = $dataValue[0];
                        } else {
                            $return[strtolower($dataKey)] = json_encode($dataValue);
                        }
                    } else {
                        $return[strtolower($dataKey)] = $dataValue;
                    }
                }
                continue;
            }

            $return[$this->toSnakeCase($key)] = $value;
        }
        if($key == 'timestamp') {
            $return['feedback_date'] = date('Y-m-d H:i:s', $value);
        }
        return $return;
    }

    protected function toSnakeCase($string, $glue = '_') {
        return strtolower(preg_replace(
        '/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])/', $glue, $string));
    }

}
