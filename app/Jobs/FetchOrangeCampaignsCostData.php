<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Services\GoogleSheets;
use App\Models\CampaignCost;
use App\Models\CampaignInfo;

class FetchOrangeCampaignsCostData extends Job
{

    public $spreadSheetId = '1fpg1kRFRlaRKzIihW5sh9qXc9QStZoJ6cLGu-NQezjI';

    public $sourceSheets = [
        'regular' => 'CRM Data',

        'perfMax' => 'PerfMax Cost - Google',

        'biziday' => 'DB - Biziday',

        'tiktok' => 'DB - TikTok - HTTTPOOL',

        'phone_calls_google' => 'Phone Calls - Google',
    ];


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sheets = new GoogleSheets($this->spreadSheetId);

        if(!ini_set('default_socket_timeout', 15)) echo "<!-- unable to change socket timeout -->";
        foreach ($this->sourceSheets as $type => $sheetName) {

            $sheetData = $sheets->readSheetValues($sheetName);
            foreach ($sheetData as $data) {

                if (!isset($data[0]) || $data[0] === 'Date' || !isset($data[1]) || $data[1] === 'Campaign Name' || strtotime($data[0]) <= strtotime(0)) continue;

                $mappedData = $this->keyMapper($type, $data);

                $filterData = [
                    'date' => $mappedData['date'],
                    'campaign' => $mappedData['campaign'],
                ];
                $campaignData = [
                    'date' => $mappedData['date'],
                    'business_area' => $mappedData['business_area'],
                    'campaign' => $mappedData['campaign'],
                ];

                // We have cost data
                if ($mappedData['cost'] !== '' && !is_null($mappedData['cost'])) {
                    $campaignData['cost'] = $mappedData['cost'];
                }
                // We have source/medium data
                if ($mappedData['source_medium']) {
                    list($source, $medium) = explode("/", $mappedData['source_medium']);
                    $campaignData['source'] = trim($source);
                    $campaignData['medium'] = trim($medium);
                    if (!isset($campaignData['cost'])) {
                        CampaignCost::where('campaign', $mappedData['campaign'])
                            ->update([
                                'source' => $campaignData['source'],
                                'medium' => $campaignData['medium']
                            ]);
                        continue;
                    }
                }

                if ($type === 'phone_calls_google') {
                    $filterData['type'] = $type;
                    CampaignInfo::updateOrCreate(
                        $filterData, $mappedData
                    );
                } else {
                    CampaignCost::updateOrCreate(
                        $filterData, $campaignData
                    );
                }
            }
        }
    }

    protected function keyMapper($type, $data)
    {
        if($type === 'phone_calls_google') {
            return [
                'date' => $data[0],
                'type' => $type,
                'campaign' => $data[1],
                'count' => $data[2],
                'business_area' => '',
                'cost' => 0,
                'source_medium' => '',
            ];
        }

        if($type === 'perfMax') {
            return [
                'date' => $data[0],
                'business_area' => '',
                'campaign' => $data[1],
                'cost' => trim(str_replace('€', '', $data[3])),
                'source_medium' => '',
            ];
        }

        if($type === 'biziday' || $type === 'tiktok') {
            return [
                'date' => Carbon::parse($data[1])->toDateString(),
                'business_area' => $data[4],
                'campaign' => $data[5],
                'cost' => trim(str_replace('€', '', $data[6])),
                'source_medium' => '',
            ];
        }

        return [
            'date' => $data[0],
            'business_area' => $data[1],
            'campaign' => $data[2],
            'cost' => $data[3],
            'source_medium' => $data[4] ?? "",
        ];
    }
}
