<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Models\Action;

class FetchOrangeTkrCrmLeadsStatus extends Job
{

    public $start;
    public $end;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($start = null, $end = null)
    {
        $this->start = $start ?
            Carbon::parse($start) :
            Carbon::now()->subMinutes(10);
        $this->end = $end ?
            Carbon::parse($end) :
            Carbon::now()->addMinutes(5);

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getLeadWithNoStatus()->chunk(50, function ($leads) {
            $statuses = $this->getStatuses($leads->toArray());
            \Log::info('FetchOrangeTkrCrmLeadsStatus statuses fetched : ' . count($statuses));
            foreach ($statuses as $status) {
                if (!$status->date_updated) continue;
                $action = Action::find($status->id);
                $action->setStatus($status->rezolutie);
            }
        });
    }

    protected function getLeadWithNoStatus()
    {
        return Action::selectRaw('id, "' . config('app.datoCmsStatus.key') . '" as "key"')
            ->where('crm_type', 'oro-tkr')
            ->where('crm_send', 1)
            ->whereNull('status')
            ->where(function ($q) {
                $q->whereNull('status')
                    ->orWhere('status', '');
            })
            ->where('created_at', '>=', $this->start->copy()->subDays(120))
            ->orderBy('id', 'DESC');
    }

    protected function getStatuses($leads)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
                CURLOPT_URL => config('app.datoCmsStatus.url'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($leads),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            )
        );

        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            curl_close($curl);
            \Log::error("cURL Error in FetchOrangeTkrCrmLeadsStatus #:" . curl_error($curl));
            return false;
        }

        curl_close($curl);

        $data = json_decode($response);
        if (isset($data->records)) {
            return $data->records;
        }

        return [];
    }
}
