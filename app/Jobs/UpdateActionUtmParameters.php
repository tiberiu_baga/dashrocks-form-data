<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Models\Action;
use App\Services\CrmManager;

class UpdateActionUtmParameters extends Job
{

    public $start;
    public $end;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($start = null, $end = null)
    {
        $this->start = $start ?
            Carbon::parse($start) :
            Carbon::now()->subDays(10);
        $this->end = $end ?
            Carbon::parse($end) :
            Carbon::now()->addMinutes(5);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $updated = 0;
        $updated = $this->updateGclids();

        $updated += $this->updateUtms();

        return $updated;
    }


    protected function updateGclids()
    {
        $actions = Action::where('page', 'LIKE', "%gclid%")
            ->where('page', 'LIKE', "%campaign%")
            ->where('utm_campaign', 'NOT LIKE', "% - %")
            ->where('utm_campaign', 'NOT LIKE', "%ORO%")
            ->where('utm_campaign', 'NOT LIKE', "%OMY%")
            ->where('created_at', '>=', $this->start)
            ->where('created_at', '<', $this->end)
            ->get();

        $updated = 0;
        foreach($actions as $action) {
            $utms = $this->getUtmParams($action);
            $parsedUrl = parse_url($action->page);
            if(!isset($parsedUrl['query'])) {
                return;
            }
            parse_str($parsedUrl['query'], $params);
            if(isset($params['gclid']) && !is_null($utms['utm_campaign'])) {
                $action->utm_campaign = $utms['utm_campaign'];
                $action->save();

                $result = Action::where('utm_campaign', $params['gclid'])
                    ->orWhere(function ($query) use ($params) {
                        $query->whereNull('utm_campaign')
                            ->where('page', 'LIKE', "%". $params['gclid'] ."%");
                    })
                    ->update(['utm_campaign' => $utms['utm_campaign']]);
                $updated += $result;
                echo "\n" . $utms['utm_campaign'] . "---" . $params['gclid'] . " $result";
            }
        }

        return $updated;
    }

    protected function updateUtms()
    {
        $actions = Action::where('page', 'LIKE', "%campaign%")
            ->where('created_at', '>=', $this->start)
            ->where('created_at', '<', $this->end)
            ->get();

        $updated = 0;
        foreach($actions as $action) {
            $utms = $this->getUtmParams($action);
            if($utms['utm_campaign'] == $action->utm_campaign) {
                continue;
            }
            $action->utm_campaign = $utms['utm_campaign'];
            $action->utm_medium = $utms['utm_medium'];
            $action->utm_source = $utms['utm_source'];
            $action->save();
            $updated++;
            echo "\n" . $utms['utm_campaign'] . " $updated";
        }

        return $updated;
    }

    protected function getUtmParams($request)
    {
        if(is_array($request)) {
            $request = json_decode(json_encode($request));
        }

        $result = [
            'utm_campaign' => $request->utm_campaign ?? null,
            'utm_medium' => $request->utm_medium ?? null,
            'utm_source' => $request->utm_source ?? null
        ];
        if(!isset($request->page) || ( isset($request->overwriteUtms) && $request->overwriteUtms) ) {
            return $result;
        }

        $parsedUrl = parse_url($request->page);
        if(!isset($parsedUrl['query'])) {
            return $result;
        }
        parse_str($parsedUrl['query'], $params);

        if(isset($params['campaign'])) {
            $result['utm_campaign'] = $params['campaign'];
        }
        if(isset($params['utm_campaign'])) {
            $result['utm_campaign'] = $params['utm_campaign'];
        }
        if(isset($params['t_campaign'])) {
            $result['utm_campaign'] = $params['t_campaign'];
        }
        if(isset($params['medium'])) {
            $result['utm_medium'] = $params['medium'];
        }
        if(isset($params['utm_medium'])) {
            $result['utm_medium'] = $params['utm_medium'];
        }
        if(isset($params['source'])) {
            $result['utm_source'] = $params['source'];
        }
        if(isset($params['utm_source'])) {
            $result['utm_source'] = $params['utm_source'];
        }

        return $result;
    }
}
