<?php

namespace App\Jobs;

use App\Models\Action;
use App\Models\GoogleCampaign;
use App\Services\GoogleAdsLeadCampaignMatcher;
use Carbon\Carbon;

class MatchGoogleCampaignLeadsJob extends Job
{
    private $startDate;

    public function __construct(Carbon $startDate = null)
    {
        $this->startDate = $startDate;
    }

    public function handle(): array
    {
        $campaigns = GoogleCampaign::pluck('campaign', 'campaign_id');

        if (count($campaigns) === 0) {
            $job = new FetchGoogleCampaignsJob();
            $job->handle();
        }

        $leadsMatcher = new GoogleAdsLeadCampaignMatcher($campaigns->toArray());

        $query = Action::where('form', 'like', 'leads-google%')
            ->whereNull('utm_campaign');
        if ($this->startDate instanceof Carbon) {
            $query->where('created_at', '>=', $this->startDate->format('Y-m-d H:i:s'));
        }
        $leadsToCheck = $query->get();

        $matchedLeads = [];

        foreach ($leadsToCheck as $lead) {
            $postData = json_decode($lead->post_data, true);
            $matchedLead = $leadsMatcher->matchCampaign($postData, $lead);
            if ($matchedLead) {
                $matchedLeads[] = $matchedLead;
            }
        }

        return $matchedLeads;
    }
}
