<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Action;
use App\Services\OrangeCrm;
use Illuminate\Support\Facades\Mail;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class BuildFacebookAudience extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "leads:build-audience {date?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update Facebook Audience.";
    private $dateStart;
    private $dateEnd;
    private $leadTypes = [
        [
            'form' => "`form` LIKE '%eshop-online-voice%' OR `form` = 'client-nou'",
            'zapier_endpoint' => 'https://hooks.zapier.com/hooks/catch/8051582/br4icfc/',
        ],
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('Europe/Bucharest');

        $this->dateStart = $this->argument('date') ?
            Carbon::parse($this->argument('date')) :
            Carbon::yesterday()->startOfDay();
        $this->dateEnd = $this->dateStart->copy()->addDays(1);

        foreach($this->leadTypes as $leadConf) {
            $leads = $this->getUniqueLeads($leadConf['form']);
            if ($leads->count()) {
                $this->sendToZapier($leadConf['zapier_endpoint'], ['contacts' => $leads->toArray()]);
            }
            echo("\nDONE for ". $leadConf['title'] . " " . $this->dateStart->toDateString()." ". $leads->count() ." leads sent");
        }
    }

    protected function getUniqueLeads($form)
    {
        return Action::selectRaw(
                "IF( LENGTH(phone) = 10, CONCAT('004',phone),
                    IF(phone REGEXP '^0*', CONCAT('0',phone), CONCAT('00',phone))
                ) as phone"
            )->whereRaw( $form )
            ->where('created_at', '>=', $this->dateStart->toDateString())
            ->where('created_at', '<', $this->dateEnd->toDateString())
            ->groupBy('phone')
            ->get();
    }

    protected function sendToZapier($endpoint, $leads)
    {
        dd(json_encode($leads));
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $endpoint,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($leads),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: text/plain'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }


}
