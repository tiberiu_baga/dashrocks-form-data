<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Transaction;
use Carbon\Carbon;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class ImportTransactionsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "import:transactions";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Importing transaction data";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $directory = "/home/forge/cbox.dash.rocks/docs/";
        $directory = "C:/work/dashrocks-form-data/docs/";

        $files = [
            "Ian2021.csv",
            "Feb2021.csv",
            "Martie.csv",
            "Aprilie2021.csv",
            "Mai.csv",
        ];

        $row = 0;
        foreach ($files as $file) {
            if (($handle = fopen($directory . $file, "r")) !== false) {
                while (($data = fgetcsv($handle, 2000, ",")) !== false) {
                    $row++;
                    if ($row == 1 || !isset($data[0]) || trim($data[0]) == 'data') {
                        continue;
                    }
                    if ($row % 250 == 0) {
                        echo ".";
                    }
                    if (strlen(trim($data[0])) !== 19) {
                        dd($data);
                    }
                    $transaction = [
                        'date' => Carbon::createFromFormat('d.m.Y H:i:s', trim($data[0])),
                        'numar_comanda' => trim($data[1]),
                        'name' => trim($data[3]),
                        'email' => trim($data[5]),
                        'phone' => $this->formatPhone(trim($data[4])),
                        'sso_id' => trim($data[19]),
                        'state' => trim($data[6]),
                        // 'city' => trim($data[0]),
                        'address' => trim($data[9]),
                        'status' => trim($data[10]),
                        'produs' => trim($data[15]),
                        'produs_cod_oa' => trim($data[16]),
                        'motive_rejectare' => substr(trim($data[59]), 0, 191),
                        'valoare_cu_tva' => trim($data[60]),
                    ];
                    Transaction::create($transaction);
                }
                fclose($handle);
                echo "\n $file processed with $row rows. \n";
            }
        }

        die("\nDONE");
    }

    protected function formatPhone($phone = null)
    {
        if (is_null($phone) || strlen($phone) == 0) {
            return null;
        }
        $phone = str_replace(
            ["+40", " ", "(", ")", "-", "+"],
            ["0", "", "", "", "", ""],
            $phone
        );

        if (strpos($phone, "0040") === 0) {
            $phone = substr_replace($phone, "0", 0, 4);
        }

        if (strpos($phone, "4007") === 0) {
            $phone = substr_replace($phone, "07", 0, 4);
        }

        if (strpos($phone, "407") === 0) {
            $phone = substr_replace($phone, "07", 0, 3);
        }

        if (strpos($phone, "007") === 0) {
            $phone = substr_replace($phone, "07", 0, 3);
        }

        if (strpos($phone, "00") === 0) {
            $phone = substr_replace($phone, "00", 0, 2);
        }

        if ($phone[0] != "0") {
            $phone = "0" . $phone;
        }

        return $phone;
    }
}
