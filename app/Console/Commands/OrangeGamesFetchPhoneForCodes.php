<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\OrangeGame;
use Illuminate\Console\Command;

class OrangeGamesFetchPhoneForCodes extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "orange-games:fetch-phone";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Fetch contact phone number for orders that have status code.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unclaimedCodes = OrangeGame::where('type', '=', 'xmas-lotery')->where('status', '=', 'code')->get();

        foreach ($unclaimedCodes as $game) {
            $orderInfo = $this->getOrderInfo($game->order_id);
            if (isset($orderInfo->contactPersonPhoneNumber)) {
                $game->phone = $orderInfo->contactPersonPhoneNumber;
                $game->save();
            }
        }

        $this->info('Done');
    }

    protected function getOrderInfo($orderId)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://www.orange.ro/webshopportlets/api/order/' . $orderId,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic d2Vic2hvcDpEYzFQZDBUcnNv',
                'Cookie: BIGipServer~DEVELOPMENT_IT~webshop_wl12_predeploy_pool=rd51o00000000000000000000ffffac107e43o9080'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $order = null;
        if ($response) {
            $order = json_decode($response);
        }

        return $order;
    }

}
