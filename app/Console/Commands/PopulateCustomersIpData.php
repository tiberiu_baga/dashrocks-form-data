<?php
namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Customer;
use App\Models\IpLocation;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class PopulateCustomersIpData extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "populate:customer-ips {limit?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Populate IP data for customers";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = $this->argument('limit') ?
            $this->argument('limit') :
            1000;

       // get customers with missing IP data
        $customers = Customer::whereNotNull('ip')
            ->whereNull('ip_location_id')
            ->limit($limit)
            ->get();

        foreach ($customers as $customer) {
            // Get ip Data from DB
            $ip = $this->getIpLocationIdFromDb($customer->ip);

            // Get ip Data from Service - Populate Ip data
            if(!$ip) {
                $ip = $this->getIpLocationIdFromService($customer->ip);
                usleep(400000);
            }

            // Update customer model
            if($ip) {
                $customer->ip_location_id = $ip->id;
                $customer->save();
                echo("\n customer data saved ipID = " . $ip->id);
            } else {
                echo("\n No IP found for = " . $customer->ip);
            }
        }

        echo("\nDONE");
    }

    protected function getIpLocationIdFromDb($ip)
    {
        return IpLocation::where('ip', $ip)->first();
    }

    protected function getIpLocationIdFromService($ip)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://tools.keycdn.com/geo.json?host='. $ip,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'User-Agent: keycdn-tools:https://cbox.dash.rocks'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        if($response) {
            $response = json_decode($response, true);
            if ($response['status'] != 'success') {
                \Log::error('IP getter error : ' . print_r($response, 1));
            }

            $iplocation = IpLocation::create($response["data"]["geo"]);
            return $iplocation;
        }

        return false;
    }
}
