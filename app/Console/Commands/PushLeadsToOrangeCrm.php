<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Action;
use App\Services\CrmManager;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class PushLeadsToOrangeCrm extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "leads:push-to-orange {date?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Push leads to orange CRM - SFA.";
    private $dateStart;
    private $dateEnd;
    private $leadType = 'verifica-acoperirea';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dateStart = $this->argument('date') ?
            Carbon::parse($this->argument('date')) :
            Carbon::now()->subDays(1)->startOfDay();

        $this->dateEnd = $this->dateStart->copy()->addDays(1);

        $leads = $this->getUniqueLeads();
        foreach($leads as $action) {
            // save to CRM
            $service = new CrmManager($action);
            $crm = $service->make();
            $crm->upload();
            echo "\n ." . $action->id;
        }
        echo("\nDONE for ". $this->dateStart->toDateString()." ". $leads->count() ." leads sent");
    }

    protected function getUniqueLeads()
    {
        $leadsSent = Action::whereIn('form', ['verifica-acoperirea-servicii'])
            ->where('created_at', '>=', $this->dateStart->toDateString())
            ->whereNotNull('phone')
            ->pluck('phone')
            ->toArray();

        $leadsAlreadySent = Action::whereIn('form', ['verifica-acoperirea'])
            ->where('created_at', '>=', $this->dateStart->toDateString())
            ->where('crm_send', 1)
            ->whereNotNull('phone')
            ->pluck('phone')
            ->toArray();

        return Action::whereIn('form', ['verifica-acoperirea'])
            ->where('created_at', '>=', $this->dateStart->toDateString())
            ->where('created_at', '<', $this->dateEnd->toDateString())
            ->whereNotIn('phone', $leadsSent)
            ->whereNotIn('phone', $leadsAlreadySent)
            ->groupBy('phone')
            ->get();
    }
}
