<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Transaction;
use Carbon\Carbon;
use App\Models\Action;
use App\Services\CrmManager;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class SendLeadsToCrmCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "upload:leads {type} {dateFrom} {dateTo} {sleep?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Resending leads to CRM";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        $type = $this->argument('type');
        $sleep = (int) $this->argument('sleep');
        $dateFrom = $this->argument('dateFrom');
        $dateTo = $this->argument('dateTo');

        $leadActions = Action::where('form', $type)
            ->where('created_at', '>=', $dateFrom)
            ->where('created_at', '<=', $dateTo)
            ->where('crm_send', 0)
            ->where('crm_send_response', 'Unable to create lead. Data is incomplete.')
            ->orderBy('id', 'DESC')
            ->get();

        foreach($leadActions as $action) {
            $service = new CrmManager($action);
            $crm = $service->make();
            $crm->upload();

            echo ("\n . " . $action->phone);
            sleep($sleep);
        }
        echo ("\nDONE");
    }

}
