<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Transaction;
use Carbon\Carbon;
use App\Models\Action;
use App\Services\CustomerService;
use App\Services\CrmManager;

/**
 * Class ImportLeadsFromFileCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class ImportLeadsFromFileCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "import:leads {filename} {type?} {sleep?} {offset?} {limit?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Importing leads from file";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('filename');
        $type = $this->argument('type');
        $sleep = (int) $this->argument('sleep');
        $offset = $this->argument('offset') ?? 0;
        $limit = $this->argument('limit') ?? 10000;

        $delimiter = ",";
        $file = "/home/forge/cbox.dash.rocks/docs/" . $filename;
        // $file = "C:/work/dashrocks-form-data/docs/" . $filename;

        foreach (file($file) as $key => $line) {
            $csv[] = str_getcsv($line, $delimiter);
            // $csv[] = array_map("utf8_encode", $columns);
        }

        array_walk($csv, function (&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header
        foreach (array_slice($csv, $offset, $limit) as $key => $value) {
            $alreadyImported = \App\Models\Action::where('form', $type)
                ->where('phone', $this->formatPhone($value["Phone"]))
                ->count();
            if($alreadyImported) {
                continue;
            }

            $action = \App\Models\Action::create([
                'form' => $type,
                'page' => "file-import",
                'medium' => 'web',
                'agent' => isset($value["Agent"]) ? $value["Agent"] : 'import',
                'name' => isset($value["Name"]) ? $value["Name"] : null,
                'email' => strpos($value["Email"], '@') ? $value["Email"] : null,
                'phone' => $this->formatPhone($value["Phone"]),
                'sso_id' => isset($value["Sso_is"]) ? $value["Sso_is"] : null,
                'city' => isset($value["City"]) ? $value["City"] : null,
                'post_data' => json_encode(['age' => $value["Age"], 'vechimeClient' => $value["VechimeClient"]]),
                // 'created_at' => date("Y-m-d H:i:s", strtotime($value["Created"]))
                'created_at' => date("Y-m-d H:i:s")
            ]);

            $customerService = new CustomerService($action);
            $customerService->store();
            // // save to CRM
            // $service = new CrmManager($action);
            // $crm = $service->make();
            // $crm->upload();

            // Delay execution by $sleep seconds
            // sleep($sleep);
            // echo ("\n . " . $action->email . " ". $action->name);
        }
        echo ("\nDONE");
    }

    protected function formatPhone($phone = null)
    {
        if (is_null($phone) || strlen($phone) == 0) {
            return null;
        }
        $phone = str_replace(
            ["+40", " ", "(", ")", "-", "+"],
            ["0", "", "", "", "", ""],
            $phone
        );

        if (strpos($phone, "0040") === 0) {
            $phone = substr_replace($phone, "0", 0, 4);
        }

        if (strpos($phone, "4007") === 0) {
            $phone = substr_replace($phone, "07", 0, 4);
        }

        if (strpos($phone, "407") === 0) {
            $phone = substr_replace($phone, "07", 0, 3);
        }

        if (strpos($phone, "007") === 0) {
            $phone = substr_replace($phone, "07", 0, 3);
        }

        if (strpos($phone, "00") === 0) {
            $phone = substr_replace($phone, "00", 0, 2);
        }

        if (isset($phone[0]) && $phone[0] !== "0") {
            $phone = "0" . $phone;
        }

        return $phone;
    }
}
