<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\OrangeGamePrize;
use Illuminate\Console\Command;
use App\Models\Transaction;
use Carbon\Carbon;
use App\Models\Action;
use App\Services\CustomerService;
use App\Services\OrangeCrm;

/**
 * Class ImportLeadsFromFileCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class ImportPrizesFromFileCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "import:prizes {filename?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Importing prizes from file";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        $filename = $this->argument('filename');

        if (null === $filename) {
            // Generate random codes for testing
            $numberOfCodes = 50;
            $csv = [];
            $storeDef = [
                'IKEA' => [
                    'pin' => true,
                    'length' => 19,
                    'value' => 100,
                    'description' => 'Voucher de 100 RON la Ikea.',
                ],
                'Decathlon' => [
                    'pin' => false,
                    'length' => 19,
                    'value' => 100,
                    'description' => 'Voucher de 100 RON la Decathlon.',
                ],
                'Noriel' => [
                    'pin' => false,
                    'length' => 19,
                    'value' => 100,
                    'description' => 'Voucher de 100 RON la Noriel.',
                ],
                'Carrefour' => [
                    'pin' => true,
                    'length' => 19,
                    'value' => 150,
                    'description' => 'Voucher de 150 RON la Carrefour.',
                ],
                'ORO 20' => [
                    'pin' => false,
                    'length' => 15,
                    'value' => 20,
                    'description' => 'Voucher de 20 EUR  în magazinul online Orange.',
                ]
            ];

            foreach ($storeDef as $store => $def) {
                for ($i = 0; $i < $numberOfCodes; $i++) {
                    OrangeGamePrize::create([
                        'voucher_code' => $this->randomNumber($def['length']),
                        'voucher_pin' => $def['pin'] ? $this->randomNumber(4) : null,
                        'voucher_value' => $def['value'],
                        'voucher_type' => $store,
                        'description' => $def['description'],
                        'type' => 'xmas-lotery'
                    ]);
                }
            }
        } else {
            $delimiter = ",";
            $file = base_path() . '/' . $filename;
            // $file = "C:/work/dashrocks-form-data/docs/" . $filename;

            foreach (file($file) as $key => $line) {
                $csv[] = str_getcsv($line, $delimiter);
                // $csv[] = array_map("utf8_encode", $columns);
            }

            array_shift($csv);
        }

        foreach ($csv as $row) {
            if ($row[1] === "") continue;
            switch ($row[0]) {
                case "IKEA":
                    $value = 100;
                    $description = 'Voucher de 100 RON la Ikea.';
                    break;
                case "Carrefour":
                    $value = 150;
                    $description = 'Voucher de 150 RON la Carrefour.';
                    break;
                case "Decathlon":
                    $value = 100;
                    $description = 'Voucher de 100 RON la Decathlon.';
                    break;
                case "Noriel":
                    $value = 100;
                    $description = 'Voucher de 100 RON la Noriel.';
                    break;
                case "ORO 20":
                    $value = 20;
                    $description = 'Voucher de 20 EUR în magazinul online Orange.';
                    break;
            }
            $pin = $row[2];
            if ($pin !== '') {
                $pin = str_pad((string) $row[2], 4, '0');
            } else {
                $pin = null;
            }
            OrangeGamePrize::create([
                'voucher_code' => $row[1],
                'voucher_pin' => $pin,
                'voucher_value' => $value,
                'voucher_type' => $row[0],
                'description' => $description,
                'type' => 'xmas-lotery'
            ]);
        }

        echo("\nDONE");
    }

    protected function randomNumber($n)
    {
        $characters = '0123456789';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = random_int(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

}
