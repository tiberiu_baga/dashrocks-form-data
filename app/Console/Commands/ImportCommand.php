<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class ImportCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "import:customers";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Importing customer data";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $directory = "/home/forge/cbox.dash.rocks/docs/";
        // $directory = "C://work/dashrocks-form-data/docs/";
        $files = [
            "pushids_decrypted_1.csv",
            "pushids_decrypted_2.csv",
            "pushids_decrypted_3.csv",
            "pushids_decrypted_4.csv",
        ];

        $row = 1;
        foreach ($files as $file) {
            if (($handle = fopen($directory . $file, "r")) !== false) {
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    $row++;
                    if ($row % 250 == 0) {
                        echo ".";
                    }
                    $customerData = [
                        'sso_id' => $data[0],
                        'token'  => $data[1],
                    ];

                    $customer = Customer::where('sso_id', $data[0])
                        ->where('token', $data[1])
                        ->first();

                    if ($customer) {
                        continue;
                    }

                    $customer = Customer::where('sso_id', $data[0])->first();
                    if (!$customer) {
                        $customer = Customer::where('token', $data[1])->first();
                    }

                    if ($customer) {
                        $customer = Customer::updateOrCreate(
                            ['id' => $customer->id],
                            $customerData
                        );
                    } else {
                        $customer = Customer::create($customerData);
                    }
                }
                fclose($handle);
                echo "\n $file processed with $row rows.";
            }
        }

        die("\nDONE");
    }
}
