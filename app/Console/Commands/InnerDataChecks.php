<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use DB;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class InnerDataChecks extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "check:inner-data {date?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Checking inner data consistency for date - compare to previous 30 day average.";

    private $date;
    private $errors = [];
    /**
     * Data check tables
     *
     * @var array
     */
    private $checkables = [
        'yoxo' => ['features', 'goals', 'onboarded', 'signupintent', 'visitors'],
        'yoxo_channel' => ['goals', 'onboarded', 'signupintent', 'visitors'],
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->date = $this->argument('date') ?
            Carbon::parse($this->argument('date')) :
            Carbon::now()->subDays(3)->startOfDay();
        $this->dateStart = $this->date->copy()->subDays(30);
        $this->dateEnd = $this->date->copy()->subDays(1);

        $this->checkIfDataPresent();
        $this->checkVisitors();
        $this->checkOnboarding();
        $this->checkGoals();
        $this->checkFeatures();

        if (!empty($this->errors)) {
            echo("\nSending mail");

            $data = [
                'content' => $this->errors,
                'date' => $this->date,
            ];
            Mail::send('emails.yoxoalert', $data, function ($message) {
                $message->to(config('alerts_mail.yoxo_to'))
                        ->subject(config('alerts_mail.yoxo_subject') . $this->date->toDateString());
            });
            echo("\n Alert Email Sent. Check inbox.");
        }
        echo("\nDONE");
    }

    protected function checkIfDataPresent()
    {
        $hasError = false;
        foreach ($this->checkables as $db => $tables) {
            foreach ($tables as $table) {
                $count = DB::connection($db)
                    ->table($table)
                    ->where('date', '=', $this->date)
                    ->count();
                if ($count == 0) {
                    $this->errors[] = 'No data for day '. $this->date->toDateString() . ' on '. $db .' DB, table `'. $table .'`.';
                    $hasError = true;
                }
            }
        }

        return $hasError;
    }

    protected function checkVisitors()
    {
        // oscilatie +/- cu mai mult de 30% fata de media ultimelor 30 zile a numarului de Visitors
        // get average for last 30 days
        $average = (float) DB::connection('yoxo')
            ->table('visitors')
            ->where('date', '>=', $this->dateStart)
            ->where('date', '<=', $this->dateEnd)
            ->average('visitors');
        // get current value
        $current = (float) DB::connection('yoxo')
            ->table('visitors')
            ->where('date', '=', $this->date)
            ->average('visitors');

        $this->checkDifference($average, $current, 30, 'visitors');


        // oscilatie +/- 20% pe principalele canale de mkt: Paid/ DIrect/ Organic/ Referrals fata de media ultimelor 30 zile a numarului de Visitors
        $averages = DB::connection('yoxo_channel')
            ->table('visitors')
            ->select(DB::raw('AVG(visitors) as average, COUNT(*) as days, channel'))
            ->where('date', '>=', $this->dateStart)
            ->where('date', '<=', $this->dateEnd)
            ->whereIn('channel', ['direct', 'organic', 'paid', 'referrer'])
            ->groupBy('channel')
            ->get();

        foreach ($averages as $channel) {
            $current = (float) DB::connection('yoxo_channel')
                ->table('visitors')
                ->where('date', '=', $this->date)
                ->where('channel', $channel->channel)
                ->average('visitors');

            $this->checkDifference($channel->average, $current, 20, $channel->channel .' channel visitors');
        }
    }

    protected function checkOnboarding()
    {
        // oscilatie +/- cu mai mult de 30% fata de media ultimelor 30 zile a numarului de Created Accounts
        // get average for last 30 days
        $averages = DB::connection('yoxo')
            ->table('onboarded')
            ->select(DB::raw('COUNT(*) / COUNT(DISTINCT date) as average, COUNT(DISTINCT date) as days, onboarding_step'))
            ->where('date', '>=', $this->dateStart)
            ->where('date', '<=', $this->dateEnd)
            ->whereIn('onboarding_step', ['Created account', 'Became active'])
            ->groupBy('onboarding_step')
            ->get();

        // Continue HERE
        foreach ($averages as $onboarding) {
            $current = (float) DB::connection('yoxo_channel')
                ->table('onboarded')
                ->where('date', '=', $this->date)
                ->where('onboarding_step', $onboarding->onboarding_step)
                ->count();
            $this->checkDifference($onboarding->average, $current, 30, "Customers from step '" .$onboarding->onboarding_step ."'");
        }

        // -  oscilatie +/- cu mai mult de 30% fata de media ultimelor 30 zile a timpului mediu de onboarding -> endDate - startDate
        $average = DB::connection('yoxo_channel')->select(
            "SELECT AVG( DATEDIFF(o1.date, o2.date) ) as average_onboarding_days
            FROM onboarded o1
            LEFT JOIN onboarded o2 ON o1.userid = o2.userid
            WHERE 1
            AND o1.onboarding_step = 'Became active'
            AND o2.onboarding_step = 'Created account'
            AND o1.date >= ?
            AND o1.date <= ?",
            [$this->dateStart, $this->dateEnd]
        );
        $average = isset($average[0]->average_onboarding_days) ? $average[0]->average_onboarding_days : 0;
        $current = DB::connection('yoxo_channel')->select(
            "SELECT AVG( DATEDIFF(o1.date, o2.date) ) as average_onboarding_days
            FROM onboarded o1
            LEFT JOIN onboarded o2 ON o1.userid = o2.userid
            WHERE 1
            AND o1.onboarding_step = 'Became active'
            AND o2.onboarding_step = 'Created account'
            AND o1.date >= ?",
            [$this->date]
        );
        $current = isset($current[0]->average_onboarding_days) ? $current[0]->average_onboarding_days : 0;

        $this->checkDifference($average, $current, 30, "Average onboarding time (days)");

        return;

        // oscilatie +/- 20% pe principalele canale de mkt: Paid/ DIrect/ Organic/ Referrals fata de media ultimelor 30 zile a numarului de onboarded
        $averages = DB::connection('yoxo_channel')
            ->table('onboarded')
            ->select(DB::raw('AVG(onboarded) as average, COUNT(*) as days, channel'))
            ->where('date', '>=', $this->dateStart)
            ->where('date', '<=', $this->dateEnd)
            ->whereIn('channel', ['direct', 'organic', 'paid', 'referrer'])
            ->groupBy('channel')
            ->get();

        foreach ($averages as $channel) {
            $current = (float) DB::connection('yoxo_channel')
                ->table('onboarded')
                ->where('date', '=', $this->date)
                ->where('channel', $channel->channel)
                ->average('onboarded');

            $this->checkDifference($channel->average, $current, 20, $channel->channel .' channel onboarded');
        }
    }

    protected function checkGoals()
    {
        // oscilatie +/- cu mai mult de 30% fata de media ultimelor 30 zile a numarului de conturi care a atins fiecare din cele 4 goals urmarite
        $averages = DB::connection('yoxo')
            ->table('goals')
            ->select(DB::raw('COUNT(*) / COUNT(DISTINCT date) as average, COUNT(DISTINCT date) as days, goal'))
            ->where('date', '>=', $this->dateStart)
            ->where('date', '<=', $this->dateEnd)
            ->groupBy('goal')
            ->get();

        foreach ($averages as $goal) {
            $current = (float) DB::connection('yoxo')
                ->table('goals')
                ->where('date', '=', $this->date)
                ->where('goal', $goal->goal)
                ->count();

            $this->checkDifference($goal->average, $current, 30, 'people reaching `'. $goal->goal .'` goal');
        }
    }

    protected function checkFeatures()
    {
        // oscilatie +/- cu mai mult de 30% fata de media ultimelor 30 zile a numarului de conturi care a atins fiecare din cele 4 goals urmarite
        $averages = DB::connection('yoxo')
            ->table('features')
            ->select(DB::raw('AVG(accounts) as average,  COUNT(*) as days, feature_name as feature'))
            ->where('date', '>=', $this->dateStart)
            ->where('date', '<=', $this->dateEnd)
            ->whereIn('feature_name', ['Uninstall', 'Errors'])
            ->groupBy('feature_name')
            ->get();

        foreach ($averages as $feature) {
            $current = (float) DB::connection('yoxo')
                ->table('features')
                ->where('date', '=', $this->date)
                ->where('feature_name', $feature->feature)
                ->count();

            $this->checkDifference($feature->average, $current, 30, $feature->feature .' feature accounts');
        }
    }

    protected function checkDifference($average, $current, $allowedMargin, $dataType)
    {
        $variation = $average <= $current ? 'increased' : 'decreased';
        $diff = $average <= $current ?
            100 - $average / $current * 100 :
            100 - $current / $average * 100;
        if ($allowedMargin < $diff) {
            $this->errors[] = ucfirst($dataType) ." for day {$this->date->toDateString()} {$variation} with more than {$allowedMargin}% compared to the previous 30 day average. (" . number_format($current, 2) ." vs " .
                number_format($average, 2) . " [" . number_format($diff, 2) . "%])";
        }
    }
}
