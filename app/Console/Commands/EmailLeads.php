<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Action;
use Illuminate\Support\Facades\Mail;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class EmailLeads extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "leads:email-to-orange {date?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Email leads to orange CRM.";
    private $dateStart;
    private $dateEnd;
    private $leadTypes = [
        [
            'form' => 'abonamente-oferta-portare-client-nou',
            'email_to' => ['orange@dashrocks.com', 'directsales@orange.com'],
            'title' => 'portare-abonamente-client-nou',
            'page' => 'www.orange.ro/abonamente/oferta-portare/client-nou'
        ],
        [
            'form' => 'wholesale-page',
            'email_to' => ['wholesale-romania@orange.com'],
            'title' => 'pagina Wholesale',
            'page' => 'www.orange.ro/wholesale'
        ]
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('Europe/Bucharest');

        $this->dateStart = $this->argument('date') ?
            Carbon::parse($this->argument('date')) :
            Carbon::now()->startOfDay();
        $this->dateEnd = $this->dateStart->copy()->addDays(1);

        foreach($this->leadTypes as $leadConf) {
            $leads = $this->getUniqueLeads($leadConf['form']);
            if ($leads->count()) {
                Mail::send('emails.leads-client-nou',
                    [
                        'content' => $leads,
                        'page' => $leadConf['page']
                    ],
                    function ($message) use ($leadConf) {
                        $message->to($leadConf['email_to'])
                            ->subject('New Leads from '. $leadConf['title'] .' - ' . Carbon::now()->toDateTimeString());
                    });
            }

            foreach($leads as $action) {
                $action->crm_send = 1;
                $action->crm_send_response = 'email';
                $action->save();
                echo "\n ." . $action->id;
            }
            echo("\nDONE for ". $leadConf['title'] . " " . $this->dateStart->toDateString()." ". $leads->count() ." leads sent");
        }
    }

    protected function getUniqueLeads($form)
    {
        return Action::where('form', $form)
            ->where('created_at', '>=', $this->dateStart->toDateString())
            ->where('created_at', '<', $this->dateEnd->toDateString())
            ->where('crm_send', 0)
            ->groupBy('phone')
            ->get();
    }

}
