<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Transaction;
use Carbon\Carbon;
use App\Models\Action;
use App\Services\CustomerService;
use App\Services\OrangeCrm;

/**
 * Class ImportLeadsFromFileCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class ImportVaLeadsFromFileCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "import:leads-va {filename} {type?} {sleep?} {offset?} {limit?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Importing leads from file";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        $filename = $this->argument('filename');
        $type = $this->argument('type');
        $sleep = (int) $this->argument('sleep');
        $offset = $this->argument('offset') ?? 0;
        $limit = $this->argument('limit') ?? 10000;

        $delimiter = ",";
        $file = "/home/forge/cbox.dash.rocks/docs/" . $filename;
        // $file = "C:/work/dashrocks-form-data/docs/" . $filename;

        foreach (file($file) as $key => $line) {
            $csv[] = str_getcsv($line, $delimiter);
            // $csv[] = array_map("utf8_encode", $columns);
        }

        array_walk($csv, function (&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header

        foreach (array_slice($csv, $offset, $limit) as $key => $value) {
            $action = \App\Models\Action::create([
                'form' => "leads-va-vhbb",
                'page' => "lead - id: file-import",
                'medium' => 'web',
                'agent' => 'Import',
                // 'name' => 'prospect ' . $type,
                'name' => 'no contact',
                'email' => '',
                'phone' => formatPhone($value["Phone"]),
                // 'created_at' => date("Y-m-d H:i:s", strtotime($value["Created"]))
                'created_at' => date("Y-m-d H:i:s")
            ]);
            $customerService = new CustomerService($action);
            $customerService->store();
            // save to CRM
            $service = new CrmManager($action);
            $crm = $service->make();
            $crm->upload();
            // Delay execution by $sleep seconds
            sleep($sleep);
            echo ("\n . " . $action->phone);
        }
        echo ("\nDONE");
    }

}
