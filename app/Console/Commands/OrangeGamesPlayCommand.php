<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Services\SummerLotteryOrangeGame;
use App\Services\XmasLotteryOrangeGame;
use Illuminate\Console\Command;

class OrangeGamesPlayCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "orange-games:play {code}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Manually play for a code.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $code = $this->argument('code');

        $game = new XmasLotteryOrangeGame(null, $code);
        if (!$game->alreadyRegistered()) {
            $this->error('Invalid code');
            return 0;
        }

        // check if not already played
        if ($game->alreadyPlayed()) {
            $this->info(var_export([
                "error" => true,
                "message" => "Already played",
                "status" => $game->game->status,
                "prize" => $game->responseFormattedPrize($game->game->prize)
            ], true));

            return 0;
        }

        // get transaction data from Orange
        $this->info('Lottery draw for code ' . $code . ' with result:' . PHP_EOL . var_export($game->play($game->game->phone), true));

        $this->info('Done');
    }

}
