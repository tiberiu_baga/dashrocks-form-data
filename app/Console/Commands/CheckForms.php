<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\Action;
use App\Models\Form;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class CheckForms extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "check:inactive-forms";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Checking inactive forms";

    private $startDate;
    private $endDate;

    public function __construct()
    {
        parent::__construct();
        $this->startDate = Carbon::now()->yesterday()->startOfDay();
        $this->endDate = $this->startDate->copy()->endOfDay();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $inactiveForms = [];
            $formsToCheck = Form::select('name', 'page')
                ->where('active', '1')
                ->get();

            foreach ($formsToCheck as $form) {
                $query = Action::where('form', $form->name)
                    ->whereBetween('created_at', [$this->startDate, $this->endDate]);

                if ($form->page) {
                    $query->whereRaw("TRIM(TRAILING '/' FROM SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(page, '#', 1), '?', 1), '://', -1)) = '" . $form->page . "'" );
                }

                if (!$query->count()) {
                    $inactiveForms[] = [
                        'name' => $form->name,
                        'page' => $form->page,
                    ];
                }
            }

            if (!empty($inactiveForms)) {
                echo("\nSending mail");

                $data = ['content' => $inactiveForms];
                Mail::send('emails.alert', $data, function ($message) {
                    $message->to(config('alerts_mail.to_mail'))
                        ->subject(config('alerts_mail.subject') . $this->startDate->toDateString());
                });
                echo("\n Alert Email Sent. Check inbox.");
            }
            echo("\nDONE");
        } catch (\Exception $e) {
            Log::info("Something went wrong in CHECKFORMS command. {$e->getMessage()}");
        }
    }
}
