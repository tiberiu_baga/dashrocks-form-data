<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Action;
use App\Services\CrmManager;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class PushFailedLeadsToCrm extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "leads:push-failed-leads {dateStart?} {dateEnd?} {sleep?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Push failed leads to CRM Orange - SFA TKR or Omy.";
    private $dateStart;
    private $dateEnd;
    private $failedResponses = [
        [
            'condition' => 'LIKE',
            'value' => 'Contact name is mandatory%',
        ]
    ];


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dateStart = $this->argument('dateStart') ?
            Carbon::parse($this->argument('dateStart')) :
            Carbon::now()->startOfDay();

        $this->dateEnd = $this->argument('dateEnd') ?
            Carbon::parse($this->argument('dateEnd')) :
            Carbon::now();

        $sleep = $this->argument('sleep') ? (int) $this->argument('sleep') : 1;


        $leads = $this->getUniqueLeads();

        foreach($leads as $action) {
            $service = new CrmManager($action);
            $crm = $service->make();
            $crm->upload();
            echo "\n ." . $action->id;

            sleep($sleep);
        }
        echo("\nDONE for ". $this->dateStart->toDateTimeString()." ". $leads->count() ." leads sent");
    }

    protected function getUniqueLeads()
    {
        $qb = Action::where('crm_send', 0)
            ->where('created_at', '>=', $this->dateStart->toDateTimeString())
            ->where('created_at', '<', $this->dateEnd->toDateTimeString())
            ->where('crm_send', 0);
        foreach ($this->failedResponses as $response) {
            $qb->where('crm_send_response', $response['condition'], $response['value']);
        }
        return $qb->get();
    }

}
