<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\YoxoUserStep;
use Carbon\Carbon;
use DB;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class FetchYoxoStepsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "import:yoxo-steps {refDate?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Importing yoxo steps";

    protected $steps = [
       'Created account'                => 'step1_date',
       'Insert phone number'            => 'step2_date',
       'Verify PIN Screen'              => 'step3_date',
       'Number Eligibility confirmed'   => 'step4_date',
       'Scan ID'                        => 'step5_date',
       'Scan ID Summary'                => 'step6_date',
       'GDPR Screen (eligible owner)'   => 'step7_date',
       'Purchased subscription'         => 'step8_date',
       'Became active'                  => 'step9_date',
    ];

    protected $goals = [
        'Eligibility denial'        => 'goal1_date',
        'Reconfigure Subscription'  => 'goal2_date',
        'Canceled subscription'     => 'goal3_date',
        'Purchased option'          => 'goal4_date',
        'Purchased Subscription'    => 'goal5_date',
        'Portin Success'            => 'goal6_date',
    ];

    private $refDate;

    public function __construct()
    {
        parent::__construct();
        $this->refDate = Carbon::now()->subDays(2)->startOfDay()->toDateString();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('refDate')) {
            $this->refDate = Carbon::parse($this->argument('refDate'))->toDateString();
        }
        $this->fetchOnboardedData();
        $this->fetchGoalsData();
        $this->fetchGenericTableData('features', '\App\Models\YoxoFeature');
        $this->fetchGenericTableData('signupintent', \App\Models\YoxoSignupintent::class);
        $this->fetchGenericTableData('visitors', \App\Models\YoxoVisitor::class);
    }

    protected function fetchOnboardedData()
    {
        // Get onboarded data by date
        $date = DB::connection('yoxo')
            ->table('onboarded')
            ->where('date', '>=', $this->refDate)
            ->get();
        foreach ($date as $userStep) {
            if (!isset($this->steps[$userStep->onboarding_step])) {
                \Log::error("Undefined step in FetchYoxoStepsCommand : " . $userStep->onboarding_step);
            }
            // Save to yoxo_user_steps
            YoxoUserStep::updateOrCreate(
                ['sso_id' => $userStep->userid],
                [
                    'sso_id' => $userStep->userid,
                    $this->steps[$userStep->onboarding_step] => $userStep->date,
                ]
            );
        }
    }

    protected function fetchGoalsData()
    {
        // Get goals data by date
        $userSteps = DB::connection('yoxo')
            ->table('goals')
            ->where('date', '>=', $this->refDate)
            ->get();

        foreach ($userSteps as $userStep) {
            if (!isset($this->goals[$userStep->goal])) {
                \Log::error("Undefined goal in FetchYoxoStepsCommand : " . $userStep->goal);
            }
            // Save to yoxo_user_steps
            YoxoUserStep::updateOrCreate(
                ['sso_id' => $userStep->userid],
                [
                    'sso_id' => $userStep->userid,
                    $this->goals[$userStep->goal] => $userStep->date,
                ]
            );
        }
    }

    protected function fetchGenericTableData($table, $sourceClass)
    {
        // Get goals data by date
        $data = DB::connection('yoxo')
            ->table($table)
            ->where('date', '>=', $this->refDate)
            ->get()->toArray();

        foreach ($data as $_data) {
            $sourceClass::insert((array) $_data);
        }
    }
}
