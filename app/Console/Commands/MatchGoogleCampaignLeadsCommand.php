<?php

namespace App\Console\Commands;

use App\Jobs\MatchGoogleCampaignLeadsJob;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MatchGoogleCampaignLeadsCommand extends Command
{
    protected $signature = 'leads:match-google-campaigns {startDate?}';

    protected $description = 'Command description';

    public function handle()
    {
        $startDateArg = $this->argument('startDate');
        $startDate = $startDateArg ?
            Carbon::createFromFormat('Y-m-d H:i:s', $startDateArg)
            :
            Carbon::now()->startOfYear();
        $start = microtime(true);
        $this->info(sprintf('Matching leads with google campaign data starting with %s ...', $startDate->format('Y-m-d H:i:s')));
        $job = new MatchGoogleCampaignLeadsJob($startDate);
        $matchedLeads = $job->handle();
        foreach ($matchedLeads as $lead) {
            $this->info(sprintf('Matched lead id: %s with campaign: %s', $lead->id, $lead->utm_campaign));
        }

        $this->info('Done in ' . number_format((microtime(true) - $start), 2) . ' seconds.');
    }
}
