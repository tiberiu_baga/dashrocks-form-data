<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Services\SummerLotteryOrangeGame;
use Illuminate\Console\Command;

class OrangeGamesNotifyCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "orange-games:notify {code} {email}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Manually notify the prize for a code.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $code = $this->argument('code');
        $email = $this->argument('email');

        // // check if Code exists
        $game = new SummerLotteryOrangeGame(null, $code);
        if (!$game->alreadyRegistered()) {
            $this->error('Invalid code.');
            return;
        }

        // // check if not already played
        if ($game->alreadyPlayed()) {
            $response = $game->notifyPrize($email);
            $this->info('Notify prize, with result:' . PHP_EOL . var_export($response, true));
        } else {
            $this->error('Code does not played yet.');
        }

        $this->info('Done');
    }

}
