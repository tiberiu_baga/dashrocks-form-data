<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\OrangeGame;
use App\Services\SummerLotteryOrangeGame;
use App\Services\XmasLotteryOrangeGame;
use Illuminate\Console\Command;

class OrangeGamesNotifyCodeReminderCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "orange-games:send-code-reminder";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send code reminder email to users who has not used their code.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $gamesWithUnusedCode = OrangeGame::where('type', 'xmas-lotery')->where('status', 'code')->get();

        foreach ($gamesWithUnusedCode as $game) {
            $email = $game->email;
            $extras = $game->extras;
            if (is_array($extras)) {
                foreach ($extras as $date => $info) {
                    if (isset($info['action']) && $info['action'] === 'Sent code email' && str_contains($info['value'], '@')){
                        $email = $info['value'];
                    }
                }
            }
            if (null !== $email && str_contains($email, '@')) {
                $xmasGame = new XmasLotteryOrangeGame($game->order_id);
                $this->info('Sending code reminder for '. $game->order_id . ' with email: ' . $email);
                $xmasGame->notifyCodeReminder($email);
            } else {
                $this->error('Sending code reminder failed for  ' . $game->order_id . ' Missing email');
            }
        }

        $this->info('Done');
    }

}
