<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Services\ActitoService;
use App\Services\SummerLotteryOrangeGame;
use Illuminate\Console\Command;

class OrangeGamesNotifyCodeUpdateCommand extends Command
{
    const ACTITO_CAMPAIGN_ID=505527;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "orange-games:notify-new-code {newVouchersFile}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Manually notify the prize update for a code.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $newVouchersFile = $this->argument('newVouchersFile');

        $delimiter = ",";
        $file = base_path() . '/' . $newVouchersFile;

        foreach (file($file) as $key => $line) {
            $csv[] = str_getcsv($line, $delimiter);
        }

        array_shift($csv);

        $actito = new ActitoService('', '');

        foreach ($csv as $row) {
            $code = $row[3];
            $oldVoucherCode = $row[5];
            $newVoucherCode = str_replace('"', '', $row[6]);
            $newVoucherPin = $row[7];
            $email = $row[8];
            $game = new SummerLotteryOrangeGame(null, $code);
            if (!$game->alreadyRegistered()) {
                $this->error('Invalid code: ' . $code);
                continue;
            }

            if ($game->alreadyPlayed()) {
                $game->game->prize->voucher_code = $newVoucherCode;
                $game->game->prize->voucher_pin = $newVoucherPin;
                $game->game->prize->save();

                $fields = '[{"key":"code", "values":["' . $code . '"]},'
                    . '{"key":"voucher_code", "values":["' . $newVoucherCode . '"]},'
                    . '{"key":"voucher_pin", "values":["' . $newVoucherPin . '"]},'
                    . '{"key":"voucher_value", "values":["' . $game->game->prize->voucher_value . '"]}'
                    . ']';

                $response = $actito->createProfileIfNotExists($email);

                // Send To Actito
                $response = $actito->triggerEmail(
                    self::ACTITO_CAMPAIGN_ID,
                    $email,
                    $fields);

                $res = $game->saveGameNotifyToExtras($email, $response, 'prize_update');

                $this->info('Notify code update with result:' . PHP_EOL . var_export($res, true));

            } else {
                $this->error('Code does not played yet: ' . $code);
            }

        }

        $this->info('Done');
    }

}
