<?php

namespace App\Console\Commands;

use App\Jobs\FetchGoogleCampaignsJob;
use Illuminate\Console\Command;

class FetchGoogleCampaignsCommand extends Command
{
    protected $signature = 'fetch:google-campaigns';

    protected $description = 'Command description';

    public function handle()
    {
        $this->info('Fetching Google Adwords campaign data from sheet...');
        $job = new FetchGoogleCampaignsJob();
        $job->handle();
        $this->info('Done');
    }
}
