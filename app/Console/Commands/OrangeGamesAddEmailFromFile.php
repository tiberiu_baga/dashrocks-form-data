<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\OrangeGame;
use Illuminate\Console\Command;

class OrangeGamesAddEmailFromFile extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "orange-games:add-email {filename}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Add email address to specific orders from csv file.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('filename');

        $delimiter = ",";
        $file = base_path() . '/' . $filename;

        foreach (file($file) as $key => $line) {
            $csv[] = str_getcsv($line, $delimiter);
        }

        array_shift($csv);

        foreach ($csv as $row) {
            $game = OrangeGame::where('order_id', $row[0])->first();
            if ($game) {
                if (null === $game->email || $game->email === '') {
                    $game->email = $row[1];
                }
                $game->save();
                $this->info("Added email: " . $row[1] . ' to game with order id: ' . $row[0]);
            } else {
                $this->info("Game not found for order id: " . $row[0]);
            }
        }


        $this->info('Done');
    }

}
