<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Action;
use App\Services\CrmManager;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class PushLeadsToCrm extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "leads:push-leads {dateStart} {dateEnd} {form} {sleep?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Push leads to CRM Orange - SFA TKR or Omy.";
    private $dateStart;
    private $dateEnd;
    private $form;


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dateStart = $this->argument('dateStart') ?
            Carbon::parse($this->argument('dateStart')) :
            Carbon::now()->subMinutes(10);

        $this->dateEnd = $this->argument('dateEnd') ?
            Carbon::parse($this->argument('dateEnd')) :
            Carbon::now()->subMinutes(10);

        $this->form = $this->argument('form');
        $sleep = (int) $this->argument('sleep');


        $leads = $this->getUniqueLeads();

        foreach($leads as $action) {
            $service = new CrmManager($action);
            $crm = $service->make();
            $crm->upload();
            echo "\n ." . $action->id;

            sleep($sleep);
        }
        echo("\nDONE for ". $this->dateStart->toDateTimeString()." ". $leads->count() ." leads sent");
    }

    protected function getUniqueLeads()
    {
        return Action::where('crm_send', 0)
            ->where('created_at', '>=', $this->dateStart->toDateString())
            ->where('created_at', '<', $this->dateEnd->toDateString())
            ->where('crm_send', 0)
            ->whereNull('crm_send_response')
            ->where('form', $this->form)
            ->get();
    }

}
