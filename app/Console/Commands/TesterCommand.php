<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Action;
use App\Services\OrangeTkrCrm;
use App\Services\CrmManager;
use App\Services\OrangeCrm;
use App\Jobs\FetchOrangeTkrCrmLeadsStatus;
use App\Jobs\FetchOrangeCampaignsCostData;
use App\Jobs\FetchUsabillaSurveyData;
use App\Jobs\UpdateActionUtmParameters;
use App\Jobs\SyncLeadsToBigQuery;

class TesterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tester:cmd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('started command please wait... ');

        // $service = new CrmManager(App\Models\Action::find('1037884') );
        // $job->make();

        // $job = new OrangeTkrCrm(\App\Models\Action::find('1037884') );
        // $job = new OrangeCrm(\App\Models\Action::find('1058433') );
        // $job->upload();

        // $job = new UpdateActionUtmParameters(\App\Models\Action::find('1124675') );
        // $service = $job->make();
        // $service->upload();
        // $job = new FetchUsabillaSurveyData();
        $job = new FetchOrangeTkrCrmLeadsStatus();
        $updated = $job->handle();

        $this->info('Finished '. $updated);
        return true;

    }
 }
