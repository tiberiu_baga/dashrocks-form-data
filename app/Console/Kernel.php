<?php

namespace App\Console;

use App\Jobs\FetchGoogleCampaignsJob;
use App\Jobs\MatchGoogleCampaignLeadsJob;
use App\Jobs\SyncConfiguredTablesToBigQuery;
use App\Jobs\SyncLeadsToBigQuery;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Jobs\FetchOrangeDatoCmsLeads;
use App\Jobs\FetchOrangeTkrCrmLeadsStatus;
use App\Jobs\FetchOrangeCampaignsCostData;
use App\Jobs\FetchUsabillaSurveyData;
use App\Jobs\UpdateActionUtmParameters;
use App\Jobs\NotifyBasketAbandoners;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ImportCommand::class,
        Commands\ImportTransactionsCommand::class,
        Commands\FetchYoxoStepsCommand::class,
        Commands\SendLeadsToCrmCommand::class,
        Commands\ImportLeadsFromFileCommand::class,
        Commands\CheckForms::class,
        Commands\InnerDataChecks::class,
        Commands\ImportVaLeadsFromFileCommand::class,
        Commands\PushLeadsToOrangeCrm::class,
        Commands\EmailLeads::class,
        Commands\PopulateCustomersIpData::class,
        Commands\PushLeadsToCrm::class,
        Commands\PushFailedLeadsToCrm::class,
        Commands\TesterCommand::class,
        Commands\OrangeGamesPlayCommand::class,
        Commands\OrangeGamesNotifyCommand::class,
        Commands\OrangeGamesNotifyCodeUpdateCommand::class,
        Commands\FetchGoogleCampaignsCommand::class,
        Commands\MatchGoogleCampaignLeadsCommand::class,
        Commands\ImportPrizesFromFileCommand::class,
        Commands\OrangeGamesFetchPhoneForCodes::class,
        Commands\OrangeGamesAddEmailFromFile::class,
        Commands\OrangeGamesNotifyCodeReminderCommand::class,
        Commands\PushFailedLeadsToCrm::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->job(new FetchOrangeTkrCrmLeadsStatus())->hourlyAt(49);

        $schedule->command('leads:push-to-orange')->daily()->at('4:00');
        $schedule->command('leads:push-to-orange')->daily()->at('5:10');
        $schedule->command('leads:push-failed-leads')->everyTwoHours();
        // $schedule->command('check:inner-data')->daily()->at('6:00');
        $schedule->command('check:inactive-forms')->daily()->at('8:00');
        // $schedule->command('import:yoxo-steps')->daily()->at('12:30');

        $schedule->command('populate:customer-ip')->twiceDaily(3, 15);
        $schedule->command('leads:email-to-orange')->everyFiveMinutes();
        // $schedule->command('leads:push-leads')->everyFiveMinutes();
        $schedule->job(new FetchOrangeDatoCmsLeads())->cron('*/2 * * * *'); //every two minutes

        $schedule->job(new FetchOrangeCampaignsCostData())->twiceDaily(2, 5);

        $schedule->job(new FetchUsabillaSurveyData())->daily()->at('3:30');

        $schedule->job(new SyncConfiguredTablesToBigQuery())->daily()->at('4:00'); // Datalake dataset

        $schedule->job(new SyncLeadsToBigQuery())->daily()->at('5:10'); // Orange dataset used for reporting

        $schedule->job(new UpdateActionUtmParameters())->daily()->at('2:00');

        $schedule->job(new FetchGoogleCampaignsJob())->daily()->at('6:00');

        $schedule->job(new MatchGoogleCampaignLeadsJob(Carbon::now()->startOfYear()))->cron('5 */6 * * *'); // Every 6 hours at 5 minutes past the hour

        // $schedule->job(new NotifyBasketAbandoners())->hourlyAt(57);
    }
}
