<?php
if (! function_exists('getHostAndStripSlash')) {
    /**
     * @param  string  $url
     */
    function getHostAndStripSlash($url) {
        $parsed_url = parse_url($url);
        if(!isset($parsed_url['path']) || !isset($parsed_url['host'])) {
            return $url;
        }

        $path = rtrim($parsed_url['path'], '/');
        return $parsed_url['host'] . $path;
    }
}
?>
