<?php
$input = "C://temp/Transactions_&_Source_Oct_-_Jan.csv";
$input = "C://temp/Transactions_by_Channels2021.csv";
$input = "C://temp/Transactions_by_Channels_2020.csv";
$output = "C://temp/output". date('Y-m-d') ."v2.sql";

$row = 1;
$content = '';
if (($handle = fopen($input, "r")) !== false) {
    while (($data = fgetcsv($handle, 1000, ",")) !== false) {
        $row++;
        if ($row == 2) {
            continue; // header row
        }
        $num = count($data);
        // echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        if ($num < 4) {
            print_r("\n Line $row error.");
            print_r($data);
            continue;
        }

        // Transactions_&_Source_Oct_-_Jan.csv
        // $content .= "UPDATE transactions SET source = '" . $data[2] . "', extras = '". $data[3] ."' WHERE numar_comanda = '". str_replace('SS-', "", $data[0]) ."';\n";

        // Transactions_by_Channels2021.csv
        $content .= "UPDATE transactions SET source = '"
            . str_replace(["(Other)", "(", ")"], ["other", "", ""], $data[0])
            . "', extras = '". $data[10]
            . "' WHERE numar_comanda = '". $data[1] ."';\n";
    }
    fclose($handle);
}

file_put_contents($output, $content);
